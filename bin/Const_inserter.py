#!/usr/bin/python

import sys
import os
import optparse

from Lexer import *

class Command:

    def __init__(self):
        self.value = None # "const"
                          # "insert"
        self.array = ""   # varname

class Inserter:

    def __init__(self, ltab="lextab"):
        self.lextab = ltab
        # delete previous lexer regual expression file file
        if os.path.isfile(self.lextab+".py"):
            os.remove(self.lextab+".py")
        #
        # build the lexer
        #
        self.lexer = Lexer()
        self.lexer.build(optimize=1,lextab=self.lextab)
        # input C file
        self.icset = False
        self.icf = sys.stdin
        # output C file
        self.ocset = False
        self.ocf = sys.stdout

    def cleanup(self):
        # delete previous lexer regual expression file file
        if os.path.isfile(self.lextab+".py"):
            os.remove(self.lextab+".py")
        # close open files
        if self.icset:
            self.icf.close()
        if self.ocset:
            self.ocf.close()

    def input_file(self, ifname):
        if self.icset:
            self.icf.close()
        self.icset = True
        if not os.path.isfile(ifname):
            sys.stderr.write("\""+ifname+"\": no such file or directory\n")
            sys.exit(1)
        self.icf = open(ifname, "r")


    def output_file(self, ofname):
        if self.ocset:
            self.ocset.close()
        self.ocset = True
        self.ocf = open(ofname, "w")

    def run(self):
        data = self.icf.read()
        self.lexer.input(data)

        while True:
            tok = self.lexer.token()
            if not tok: break

            if tok.type == "COMMENT":
                command = self.__parse_comment(tok.value)
                if command.value == "const":
                    self.ocf.write("const")
                    pass
                elif command.value == "insert":
                    self.ocf.write("= ")
                    self.__insert_array_definition(command.array)
                else:
                    self.ocf.write(tok.value)
            else:
                self.ocf.write(tok.value)


    def __strip_comment(self, comment):
        # returns comment content, without leading '$'
        if comment[1] == '*':
            return comment[3:-2]
        else:
            return comment[3:]

    def __parse_comment(self, comment):
        command = Command()

        if comment[2] == '$':
            comment = self.__strip_comment(comment)
            comment = comment.strip()
            if comment == "const":
                command.value = "const"
            else:
                command.value = "insert"
                command.array = comment

        return command

    def __insert_array_definition(self, arrnamestr):
        dimension = arrdim[arrnamestr]
        if dimension == 1:
            self.__insert_1D_array(eval(arrnamestr))
        elif dimension == 2:
            self.__insert_2D_array(eval(arrnamestr))

    def __insert_1D_array(self, arr):
        self.ocf.write("{"+str(arr[0]))
        for i in range(1, len(arr)):
            self.ocf.write(", "+str(arr[i]))
        self.ocf.write("}")

    def __insert_2D_array(self, arr):
        self.ocf.write("{")
        self.__insert_1D_array(arr[0])
        for i in range(1, len(arr)):
            self.ocf.write(", ")
            self.__insert_1D_array(arr[i])
        self.ocf.write("}")


""" parse input parameters """
oparser = optparse.OptionParser()
oparser.add_option("-i", "--input", action="store",
                   dest="input", metavar="FILE",
                   help="set output file")
oparser.add_option("-o", "--output", action="store",
                   dest="output", metavar="FILE",
                   help="set output file")
oparser.add_option("-c", "--constfile", action="store",
                   dest="constfile", metavar="FILE",
                   help="name of the file containing python definition of inserted constants")
oparser.add_option("-l", "--lextmp", action="store",
                   dest="lextmp", metavar="FILE",
                   help="set lexer temporary file")
(options, args) = oparser.parse_args()

""" load constants file """
constfile = "gpu_constants"
if options.constfile != None:
    # trip the .py suffix
    splitted = options.constfile.split(".")
    if splitted[-1] != "py":
        sys.stderr.write("constant file must have \".py\" suffix\n")
        sys.exit(1)
    constfile = ".".join(splitted[0:-1])
sys.path.append(os.getcwd())
exec("from "+constfile+" import *")

""" set lexer lemporary file """
if options.lextmp != None:
    lextmp = options.lextmp
else:
    lextmp = "lextab"

inserter = Inserter(ltab=lextmp)

""" pass parameters """
if options.input != None:
    inserter.input_file(options.input)
if options.output != None:
    inderter.output_file(options.output)

inserter.run()

""" close and lelete files """
inserter.cleanup()
