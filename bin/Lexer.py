#!/usr/bin/python

from ply import lex
from ply.lex import TOKEN

class Lexer:

    def __init__(self):
        pass

    def build(self, **kwargs):
        self.lexer = lex.lex(object=self, **kwargs)

    def input(self, text):
        self.lexer.input(text)

    def token(self):
        return self.lexer.token()

    reserved = {
        "auto"     : "AUTO",
        "break"    : "BREAK",
        "case"     : "CASE",
        "char"     : "CHAR",
        "const"    : "CONST",
        "continue" : "CONTINUE",
        "default"  : "DEFAULT",
        "do"       : "DO",
        "double"   : "DOUBLE",
        "else"     : "ELSE",
        "enum"     : "ENUM",
        "extern"   : "EXTERN",
        "float"    : "FLOAT",
        "for"      : "FOR",
        "goto"     : "GOTO",
        "if"       : "IF",
        "int"      : "INT",
        "long"     : "LONG",
        "register" : "REGISTER",
        "return"   : "RETURN",
        "short"    : "SHORT",
        "signed"   : "SIGNED",
        "sizeof"   : "SIZEOF",
        "static"   : "STATIC",
        "struct"   : "STRUCT",
        "switch"   : "SWITCH",
        "typedef"  : "TYPEDEF",
        "union"    : "UNION",
        "unsigned" : "UNSIGNED",
        "void"     : "VOID",
        "volatile" : "VOLATILE",
        "while"    : "WHILE"
    }

    tokens = [
        "ELLIPSIS",
        "RIGHT_ASSIGN",
        "LEFT_ASSIGN",
        "ADD_ASSIGN",
        "SUB_ASSIGN",
        "MUL_ASSIGN",
        "DIV_ASSIGN",
        "MOD_ASSIGN",
        "AND_ASSIGN",
        "XOR_ASSIGN",
        "OR_ASSIGN",
        "RIGHT_OP",
        "LEFT_OP",
        "INC_OP",
        "DEC_OP",
        "PTR_OP",
        "AND_OP",
        "OR_OP",
        "LE_OP",
        "GE_OP",
        "EQ_OP",
        "NE_OP",
        "CONSTANT",
        "STRING_LITERAL",
        "ID",
        "BLANK",
        "COMMENT",
        "PREPHASH"
    ] + list(reserved.values())

    literals = [ ';', '{', '}', ',', ':', '=', '(', ')', '[', ']',
                 '.', '&', '!', '~', '-', '+', '*', '/', '%', '<',
                 '>', '^', '|', '?' ]

    #t_ignore  = " \t"

    # Define a rule so we can track line numbers
    #def t_newline(self, t):
    #    r" \n+ "
    #    t.lexer.lineno += len(t.value)

    # Compute column. 
    #     input is the input text string
    #     token is a token instance
    def _find_column(input,token):
        last_cr = input.rfind('\n',0,token.lexpos)
        if (last_cr < 0):
        	last_cr = 0
        column = (token.lexpos - last_cr) + 1
        return column

    constant       = r" ( [0-9]+[eE][+-]?[0-9]+[fFlL]? )|( [0-9]*\.[0-9]+([eE][+-]?[0-9]+)?[fFlL]? )|( [0-9]+\.[0-9]*([eE][+-]?[0-9]+)?[fFlL]? )| \
                        ( 0[xX][a-fA-F0-9]+[uUlL]* )|( 0[0-9]+[uUlL]* )|( [0-9]+[uUlL]* )|( L?'((\\.)|([^\\']))+' ) "
    string_literal = r' L?"(\\.|[^\\"])*" '
    id             = r" [a-zA-Z_]([a-zA-Z_]|[0-9])* "
    blank          = r" [ ]|\t|\n "
    comment        = r" ( /\*([^\*]|(\*[^/]))*\*/ )|( //[^\n]* ) "
    prephash       = r" [ \t]* \# ([^\\\n]|(\\[^\n])|(\\\n))* "

    @TOKEN(constant)
    def t_CONSTANT(self, t):
        return t

    @TOKEN(string_literal)
    def t_STRING_LITERAL(self, t):
        return t

    @TOKEN(id)
    def t_ID(self, t):
        t.type = self.reserved.get(t.value,'ID') # Check for reserved words
        return t

    t_ELLIPSIS     = r"\.\.\."
    t_RIGHT_ASSIGN = r">>="
    t_LEFT_ASSIGN  = r"<<="
    t_ADD_ASSIGN   = r"\+="
    t_SUB_ASSIGN   = r"-="
    t_MUL_ASSIGN   = r"\*="
    t_DIV_ASSIGN   = r"/="
    t_MOD_ASSIGN   = r"%="
    t_AND_ASSIGN   = r"&="
    t_XOR_ASSIGN   = r"^="
    t_OR_ASSIGN    = r"\|="
    t_RIGHT_OP     = r">>"
    t_LEFT_OP      = r"<<"
    t_INC_OP       = r"\+\+"
    r_DEC_OP       = r"--"
    r_PTR_OP       = r"->"
    t_AND_OP       = r"&&"
    t_OR_OP        = r"\|\|"
    t_LE_OP        = r"<="
    t_GE_OP        = r">="
    t_EQ_OP        = r"=="
    t_NE_OP        = r"!="

    @TOKEN(blank)
    def t_BLANK(self, t):
        return t

    @TOKEN(comment)
    def t_COMMENT(self, t):
        return t

    @TOKEN(prephash)
    def t_PREPHASH(self, t):
        return t

    # Error handling rule
    def t_error(self, t):
        print("Illegal character '%s'" % t.value[0])
        t.lexer.skip(1)
        t.value = t.value[0]
        return t

"""
"<%"		{ count(); return('{'); }
"%>"		{ count(); return('}'); }
"<:"		{ count(); return('['); }
":>"		{ count(); return(']'); }


%{
#include <stdio.h>
#include "y.tab.h"

void count();
%}

%%
"/*"			{ comment(); }

[ \t\v\n\f]		{ count(); }
.			{ /* ignore bad characters */ }

%%
"""
