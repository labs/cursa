#!/usr/bin/python

import sys
import os
import optparse

from Lexer import *

class Commands:

    def __init__(self):
        self.expand = None # True or False
        self.itervar = ""  # "varname"
        self.indexes = []  # list of possible indexes
        self.replace = []  # constants to be replaced

class Expander:

    def __init__(self, ltab="lextab"):
        self.lextab = ltab
        # delete previous lexer regual expression file file
        if os.path.isfile(self.lextab+".py"):
            os.remove(self.lextab+".py")
        #
        # build the lexer
        #
        self.lexer = Lexer()
        self.lexer.build(optimize=1,lextab=self.lextab)
        # input C file
        self.icset = False
        self.icf = sys.stdin
        # output C file
        self.ocset = False
        self.ocf = sys.stdout
        
    def cleanup(self):
        # delete previous lexer regual expression file file
        if os.path.isfile(self.lextab+".py"):
            os.remove(self.lextab+".py")
        # close open files
        if self.icset:
            self.icf.close()
        if self.ocset:
            self.ocf.close()

    def input_file(self, ifname):
        if self.icset:
            self.icf.close()
        self.icset = True
        if not os.path.isfile(ifname):
            sys.stderr.write("\""+ifname+"\": no such file or directory\n")
            sys.exit(1)
        self.icf = open(ifname, "r")


    def output_file(self, ofname):
        if self.ocset:
            self.ocset.close()
        self.ocset = True
        self.ocf = open(ofname, "w")


    def run(self):
        data = self.icf.read()
        self.lexer.input(data)

        last_comment = ""

        while True:
            tok = self.lexer.token()
            if not tok: break
            if (tok.type == "FOR") and (last_comment != ""):
                # found unroll comment
                command = self.__parse_comment(last_comment)
                if command.expand:
                    # call loop unrolling function
                    strbuff = self.__expand_for(command)
                    for item in strbuff:
                        self.ocf.write(item)
                else:
                    self.ocf.write(tok.value)
            else:
                if tok.type == "COMMENT":
                    last_comment = tok.value
                elif tok.type != "BLANK":
                    last_comment = ""
                self.ocf.write(tok.value)


    def __strip_comment(self, comment):
        # returns comment content, without leading '@'
        if comment[1] == '*':
            return comment[3:-2]
        else:
            return comment[3:]


    def __parse_comment(self, comment):
        command = Commands()
        # set expand
        command.expand = (comment[2] == '@')
        if command.expand:
            comment = self.__strip_comment(comment)

            # parse comment for variables
            comment = comment.split(";")
            # parse iteration variable
            r = list(map(lambda x : x.strip(), comment[0].split(",")))
            # convert to integers
            command.itervar = r[0]
            command.indexes = list(range(eval(r[1]),eval(r[2]),eval(r[3])))
            if (len(comment) > 1):
                # get list of constants to be replaced
                command.replace = list(map(lambda x : x.strip(), comment[1].split(",")))

        return command


    def __skip_parentheses(self):
        while True:
            tok = self.lexer.token()
            if not tok: 
                sys.stderr.write("unexpected end of file\n")
                sys.exit(1)
            if tok.type == '(': break
        openpar = 1
        while openpar > 0:
            tok = self.lexer.token()
            if not tok:
                sys.stderr.write("unexpected end of file\n")
                sys.exit(1)
            if tok.type == ')':
                openpar -= 1
            elif tok.type == '(':
                openpar += 1


    #
    # Expand for loop
    #
    def __expand_for(self, command):
        self.__skip_parentheses()
        # find first nonblank token
        tok = self.lexer.token()
        strbuff = []
        strbuff.append(tok.value)
        while tok and (tok.type != '{'):
            tok = self.lexer.token()
            strbuff.append(tok.value)
        if not tok:
            sys.stderr.write("unexpected end of file\n")
            sys.exit(1)

        last_comment = ""
        openbrace = 1

        while openbrace > 0:
            tok = self.lexer.token()
            if not tok:
                sys.stderr.write("unexpected end of file\n")
                sys.exit(1)
            if (tok.type == "FOR") and (last_comment != ""):
                # found unroll comment
                new_command = self.__parse_comment(last_comment)
                if new_command.expand:
                    # call loop unrolling function
                    strbuff += self.__expand_for(new_command)
                    #self.ocf.write(buffer)
                else:
                    strbuff.append(tok.value)
            else:
                if tok.type == "COMMENT":
                    last_comment = tok.value
                elif tok.type != "BLANK":
                    last_comment = ""
                    if tok.type == '{':
                        openbrace += 1
                    elif tok.type == '}':
                        openbrace -= 1
                    
                strbuff.append(tok.value)

        # iterate code and replace iteration indexes
        strbuff = self.__iterate_code(strbuff, command)
        # replace acees of constant memory with its value
        strbuff = self.__replace_constants(strbuff, command)
        # check for register stements in asm
        strbuff = self.__asm_check_register(strbuff)
        
        return strbuff

    #
    # Iterate code, rep
    #
    def __iterate_code(self, strbuff, command):
        auxbuff = []
        for idx in command.indexes:
            for word in strbuff:
                if (word != command.itervar):
                    auxbuff.append(word)
                else:
                    auxbuff.append(str(idx))
        return auxbuff

    #
    # Replace constants listed in command
    #
    def __replace_constants(self, strbuff, command):
        auxbuff = []
        idx = 0
        while idx < len(strbuff):
            if strbuff[idx] not in command.replace:
                # do not replace
                auxbuff.append(strbuff[idx])
            else:
                # replace
                dim = arrdim[strbuff[idx]]
                var = strbuff[idx]
                bracketnum = 0
                bracketopened = 0
                while (bracketopened != 0) or (bracketnum < dim):
                    idx += 1
                    if strbuff[idx] == "[":
                        bracketopened += 1
                    elif strbuff[idx] == "]":
                        bracketopened -= 1
                        if bracketopened == 0:
                            bracketnum += 1
                    var += strbuff[idx]
                auxbuff.append(str(eval(var)))
                
            idx += 1
        return auxbuff

    #
    # Replace "r"(number) with "n"(number) in asm statements
    #
    def __asm_check_register(self, strbuff):
        auxbuff = []
        regbuff = []
        asmparentheses = 0
        regparentheses = 0
        s_outside_asm    = 0
        s_inside_asm     = 1
        s_register       = 2
        s_regpar         = 3
        state = s_outside_asm
        for word in strbuff:
            if state == s_outside_asm:
                if word == "asm":
                    state = s_inside_asm
                auxbuff.append(word)

            else:
                if word == "(":
                    asmparentheses += 1
                elif word == ")":
                    asmparentheses -= 1
                    
                if state == s_inside_asm:
                    if word == "\"r\"":
                        state = s_register
                    else :
                        if word == ")":
                            if asmparentheses == 0:
                                state = s_outside_asm
                        if regbuff != []:
                            # process register buffer
                            if len(regbuff) == 1:
                                # could be a number
                                is_num = True
                                try:
                                    int(regbuff[0])
                                except:
                                    is_num = False
                                if is_num:
                                    auxbuff += ["\"n\"", "("]
                                else:
                                    auxbuff += ["\"r\"", "("]
                                auxbuff += regbuff
                                auxbuff.append(")")
                            else:
                                # expression
                                auxbuff += ["\"r\"", "("]
                                auxbuff += regbuff
                                auxbuff.append(")")
                            regbuff = []
                        auxbuff.append(word)

                elif state == s_register:
                    if word == "(":
                        state = s_regpar
                        regparentheses += 1

                else:
                    if state == s_regpar:
                        if word == ")":
                            regparentheses -= 1
                        if regparentheses == 0:
                            state = s_inside_asm
                        else:
                            regbuff.append(word)

        return auxbuff


""" parse input parameters """
oparser = optparse.OptionParser()
oparser.add_option("-i", "--input", action="store",
                   dest="input", metavar="FILE",
                   help="set output file")
oparser.add_option("-o", "--output", action="store",
                   dest="output", metavar="FILE",
                   help="set output file")
oparser.add_option("-c", "--constfile", action="store",
                   dest="constfile", metavar="FILE",
                   help="name of the file containing python definition of replaced constants")
oparser.add_option("-l", "--lextmp", action="store",
                   dest="lextmp", metavar="FILE",
                   help="set lexer temporary file")
(options, args) = oparser.parse_args()


""" load constants file """
constfile = "gpu_constants"
if options.constfile != None:
    # trip the .py suffix
    splitted = options.constfile.split(".")
    if splitted[-1] != "py":
        sys.stderr.write("constant file must have \".py\" suffix\n")
        sys.exit(1)
    constfile = ".".join(splitted[0:-1])
sys.path.append(os.getcwd())
exec("from "+constfile+" import *")

""" set lexer lemporary file """
if options.lextmp != None:
    lextmp = options.lextmp
else:
    lextmp = "lextab"

expander = Expander(ltab=lextmp)

""" pass parameters """
if options.input != None:
    expander.input_file(options.input)
if options.output != None:
    expander.output_file(options.output)

expander.run()

""" close and delete files """
expander.cleanup()
