/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef _GPU_RSA_H_
#define _GPU_RSA_H_


#include "base.h"
#include "cpu_rsa.h"


#ifdef __cplusplus
extern "C" {
#endif


/* The following constants define the number of messages to be signed at once.
   Currently the values are hard-wired into the code and the changing of them
   requires editing the source code and re-compilation. */


/**
 * \brief grid size
 *
 * Number of blocks to be computed at once.
 *
 * \note
 * For best performance use a multiple of the number of multiprocessors
 * available on your device. (e.g. 15 or 30 for the GTX480)
 */
#define BLOCKS   15


/**
 * \brief block size
 *
 * Number of threads to be executed in a single block.
 *
 * \note
 * To achieve best performance the block size should be as high as possible.
 * Can be a multiple of the warp size (32 for the GTX480).
 */
#define THREADS  896


/**
 * \brief number of CUDA streams
 *
 * Number of independent streams which can be executed simultaneously
 * on a single device. Allows memory transfers to be performed simultaneously
 * with GPU code execution.
 *
 * \note
 * In fact does not a have a significant effect - when using sufficiently large
 * blocks. However many threads can cause execution errors due to high demands
 * for limited resources on the chip.
 */
#define STREAMS  1


/**
 * \brief number of messages in a stream
 *
 * Number of messages to be signed by a single stream.
 */
#define STREAMSIZE ((BLOCKS) * (THREADS))
/**
 * \brief package size
 *
 * Total number of messages to be computed in a package. A package is the
 * smallest bundle of messages to be signed at once by a single CUDA kernel
 * invocation.
 */
#define CIPHERS  ((STREAMSIZE) * (STREAMS))


/**
 * \brief words in a message
 *
 * Number of words (= integers = whole lumbers) in a message.
 */
#define MESS_GPU_WORDS (MESSBYTES/sizeof(moduli_t))


/* The following defines serve as renaming for the variables storing various
   values computed in the initiation stage. */
#define CONST_BASE_A_MUSTER rns_base_a
#define CONST_BASE_B_MUSTER rns_base_b

#define CONST_BASE_A_COMP_MUSTER rns_base_a_comp
#define CONST_BASE_B_COMP_MUSTER rns_base_b_comp

#define CONST_WW_RADTORNSA_MUSTER gpu_ww_radtornsa
#define CONST_WW_RADTORNSB_MUSTER gpu_ww_radtornsb

#define CONST_A_1RM_MUSTER A_1rm
#define CONST_B_1RM_MUSTER B_1rm

#define CONST_AI_1AI_MUSTER Ai_1ai
#define CONST_BI_1BI_MUSTER Bi_1bi

#define CONST_AI_RNSB Ai_rnsb
#define CONST_BI_RNSA Bi_rnsa

#define CONST_A_RM_MUSTER A_rm
#define CONST_B_RM_MUSTER B_rm

#define CONST_AI_RM_MUSTER Ai_rm
#define CONST_BI_RM_MUSTER Bi_rm

#define CONST_MINUSA_RNSB_MUSTER minusA_rnsb
#define CONST_MINUSB_RNSA_MUSTER minusB_rnsa

#define CONST_N_NEG1B_RNSB_MUSTER n_neg1B_rnsb

#define CONST_N_RNSAB_MUSTER n_rnsab

#define CONST_B_1A_RNSA_MUSTER B_1A_rnsa

#define CONST_B2_N_RNSAB_MUSTER B2_n_rnsab

#define CONST_ONES_RNSAB_MUSTER ones_rnsab

#define CONST_MRS_PAJ_1AI_MUSTER mrs_Paj_1ai
#define CONST_MRS_PBJ_1BI_MUSTER mrs_Pbj_1bi

#define CONST_MRS_PAJ_AI_MUSTER mrs_Paj_ai
#define CONST_MRS_PBJ_BI_MUSTER mrs_Pbj_bi

#define CONST_MRS_PAJI_BI_MUSTER mrs_Paji_bi
#define CONST_MRS_PBJI_AI_MUSTER mrs_Pbji_ai

#define CONST_MRS_PAJI_MUSTER mrs_Paji
#define CONST_MRS_PBJI_MUSTER mrs_Pbji


/**
 * \brief initializes the GPU constant memory
 *
 * Copies all necessary values to the GPU constant and global memory.
 * These values are used for computation in RNS. Also sets the exponents.
 *
 * \param[in] key
 *   RSA key
 *
 * \note
 * Must be called after the values have been initialized and before the first
 * messages signing request.
 */
void gpu_init_constants(const rsa_key *key);


/**
 * \brief initialize GPU structures
 *
 * Creates and initializes internal GPU control structures. The function
 * also allocates GPU memory.
 *
 * \note
 * Must be called before first message signing request.
 */
void gpu_structs_create(void);


/**
 * \brief destroy GPU structures
 *
 * De-initializes and destroys internal GPU control structures.
 */
void gpu_structs_destroy(void);


/**
 * \brief return number os messages to be signed at once
 *
 * Returns the number of messages which can be signed at a single RSA kernel
 * invocation.
 *
 * \retrun
 *   number of messages to be signed at once
 */
unsigned long gpu_messages_at_once(void);


/**
 * \brief launches RSA signature
 *
 * Launches the GPU kernel and signs a package of messages with the private
 * exponent.
 *
 * \param[out] host_out_rad
 *   pointer to signed messages package
 * \param[in]  host_in_rad
 *   pointer to a package containing messages to be signed
 *
 * \note
 * The memory passed by the parameters must be allocated as page-locked,
 * otherwise the function will fail to execute correctly because asynchronous
 * device-to-host and host-to-device memory transfers are used.
 */
void gpu_launch_rsa_sign(unsigned char host_out_rad[MESSBYTES*CIPHERS],
                         const unsigned char host_in_rad[MESSBYTES*CIPHERS]);


/**
 * \brief launches RSA signature
 *
 * Launches the GPU kernel and signs a package of messages with the public
 * exponent.
 *
 * \param[out] host_out_rad
 *   pointer to verified message package
 * \param[in]  host_in_rad
 *   pointer to package containing messages to be verified
 *
 * \note
 * The memory containing the packages must be allocated as page-locked,
 * otherwise the function will fail to execute correctly because asynchronous
 * device-to-host and host-to-device memory transfers are used.
 */
void gpu_launch_rsa_verify(unsigned char host_out_rad[MESSBYTES*CIPHERS],
                           const unsigned char host_in_rad[MESSBYTES*CIPHERS]);


/**
 * \brief launches RSA
 *
 * Launches the GPU kernel and signs a package of messages with the private
 * exponent. The signed messages are decrypted with the public exponent
 * to verify whether the original message can be reconstructed (no error
 * occurred). Messages which could not be reconstructed are marked with 
 * non-zero value in the corresponding position in the array host_corrupt.
 * The function returns 0 if all messages are computed correctly, otherwise
 * it returns the number of messages which could not be signed correctly.
 *
 * \param[out] host_out_rad
 *   pointer to host memory which the signed messages are copied
 * \param[out] host_corrupt
 *   pointer to array where messages which could not be signed correctly
 *   are marked
 * \param[in]  host_in_rad
 *   pointer to a package of messages to be signed
 * \return
 *   the number of incorrectly signed messages
 *
 * \note
 * The memory passed via the arguments must be allocated as page-locked,
 * otherwise the function will fail to execute correctly.
 * A NULL pointer may be passed via the host_corrupt argument in cases when you
 * don't want to know which signatures are incorrect.
 */
signed int gpu_launch_rsa_sign_verify(unsigned char host_out_rad[MESSBYTES*CIPHERS],
                                      signed int host_corrupt[CIPHERS],
                                      const unsigned char host_in_rad[MESSBYTES*CIPHERS]);


/**
 * \brief tests GPU devices for presence
 *
 * Tests devices for presence. Returns number of CUDA capapble devices present.
 */
int gpu_seach_devs(void);


/**
 * \brief prints info about the GPU device
 *
 * Prints some basic information about the device GPU. If no GPU is found the
 * function will exit the program execution.
 */
void gpu_print_dev_info(void);


/**
 * \brief allocates page-locked memory
 *
 * Allocates page-locked host memory. The memory serves as the communication
 * interface for GPU-host communication. The page-locked memory is needed for
 * asynchronous memory transfers.
 *
 * \param[out] ptr
 *   address of the pointer pointing to the beginning of the allocated
 *   memory block
 * \param[in]  size
 *   number of bytes requested to be allocated
 *
 * \note
 * Asynchronous memory transfers will generate invalid argument error
 * if accessing other than page-locked memory.
 */
void malloc_pagelocked(void **ptr, size_t size);


/**
 * \brief frees page-locked memory
 *
 * Frees a page-locked memory block.
 *
 * \param[in] ptr
 *   pointer pointing to the beginning of the memory block to be freed
 */
void free_pagelocked(void *ptr);


/* \brief dumps RSA modulus independent GPU constant into a file
 * containing python array definitions
 *
 * Dumps all RSA modulus independent precomputed values into a file which
 * can be used in python constants definition scripts.
 *
 * \param[out] fout
 *   destination text file
 *
 * \note
 * This function must be run after the initialization phase. However the
 * modulus needs not to be set correctly.
 *
 * \note
 * The python definition can be used to explicitly modify the GPU source code
 * with the knowledge of the precomputed values which are otherwise set
 * during the initialization phase. This enables the replacing of several
 * variables with a constant definition and/or code replacement. The code
 * replacement consists mainly of loop unrolling and replacing reads
 * of variables with constant values.
 */
void dump_modulus_independent_to_python(FILE *fout);


#ifdef __cplusplus
}
#endif


#endif /* _GPU_RSA_H_ */
