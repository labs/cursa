/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef _BIGNUM_H_
#define _BIGNUM_H_

#include <string.h>
#include <gmp.h>


#ifdef __cplusplus
extern "C" {
#endif

/*
typedef struct
{
  unsigned long *raw;
  unsigned long allocated_words;
  unsigned long used_words;
  unsigned int  negative;
} bignum_t;
*/

/**
 * \brief used bignum type
 */
typedef mpz_t bn_t;

/**
 * \brief order type
 *
 * LSF = -1 - least significant word first
 * MSF = 1  - most significant word first
 */
typedef enum {LSF = -1, MSF = 1} order_t;


/**
 * \brief endian type
 *
 * LITTLE = -1 - least significant byte first
 * NATIVE = 0  - host CPU native endianness 
 * BIG    = 1  = most significant byte first
 */
typedef enum {LITTLE = -1, NATIVE = 0, BIG = 1} endian_t;


/**
 * \brief initialize x
 *
 * Initializes x and sets its value to 0.
 *
 * \param[out] x
 *   initialized bignum
 */
void bn_init(bn_t x);


/**
 * \brief frees space
 *
 * Frees the memory allocated for x.
 *
 * \param[in] x
 *   de-initialized bignum
 */
void bn_clear(bn_t x);


/**
 * \brief assigns a bignum to a bignum
 *
 * Sets a bignum to the value of the given bignum.
 *
 * \param[out] rx
 *   bignum to be set
 * \param[in]  x
 *   given bignum
 */
void bn_set(bn_t rx, bn_t x);


/**
 * \brief assigns integer to a bignum
 *
 * Sets a bignum to the value of the given integer.
 *
 * \param[out] rx
 *   bignum to be set
 * \param[in]  x
 *   given value
 */
void bn_set_ui(bn_t rx, unsigned long x);


/**
 * \brief conversion to unsigned integer
 *
 * Converts to unsigned integer. The sign is ignored, returns
 * the absolute value.
 *
 * \param[in] x
 *   converted bignum
 * \return
 *   value of the bignum which fits into returned data type
 *
 * \note
 * If the converted bignum exceeds the range of unsigned integer, then
 * it returns only the least significant bits which fit.
 */
unsigned long bn_get_ui(bn_t x);


/**
 * \brief imports bignum
 *
 * Imports a bignum from an array of words.
 *
 * \param[out] rn
 *   bignum to be set
 * \param[in]  words
 *   number of words of the bignum
 * \param[in]  order
 *   ordering of the words
 * \param[in]  size
 *   number of bytes in a word
 * \param[in]  endian
 *   endian type of the stored number
 * \param[in]  nails
 *   skip nails most significant bits of the word; 0 = use full words
 * \param[in]  n
 *   array of word to be read
 */
void bn_import(bn_t rn, size_t words, order_t order, size_t size,
               endian_t endian, size_t nails, const void *n);


/**
 * \brief exports bignum
 *
 * Exports a bignum into an array of words.
 *
 * \param[out] rn
 *   array of words to be filled
 * \param[out] words
 *   number of words which is produced
 * \param[in]  order
 *   ordering of the words
 * \param[in]  size
 *   number of bytes in a word
 * \param[in]  endian
 *   endian type of the number to be stored
 * \param[in]  nails
 *   skip nails most significant bits of the word; 0 = use full word
 * \param[in]  n
 *   bignum to be exported
 */
void bn_export(void *rn, size_t *words, order_t order, size_t size,
               endian_t endian, size_t nails, bn_t n);


/**
 * \brief bignum addition
 *
 * Adds two bignums and stores to a bignum.
 * res = op1 + op2
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   addend
 * \param[in]  op2
 *   addend
 */
void bn_add(bn_t res, bn_t op1, bn_t op2);


/**
 * \brief bignum subtraction
 *
 * Subtracts two bignums and stores to a bignum.
 * res = op1 - op2
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   minuend
 * \param[in]  op2
 *   subtrahend
 */
void bn_sub(bn_t res, bn_t op1, bn_t op2);


/**
 * \brief bignum subtraction
 *
 * Subtracts unsigned integer from a bignum and stores to a bignum.
 * res = op1 - op2
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   minuend
 * \param[in]  op2
 *   subtrahend
 */
void bn_sub_ui(bn_t res, bn_t op1, unsigned long op2);


/**
 * \brief bignum multiplication
 *
 * Multiplies two bignums and stores result to another a bignum.
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   first multiplicand
 * \param[in]  op2
 *   second multiplicand
 */
void bn_mul(bn_t res, bn_t op1, bn_t op2);


/**
 * \brief bignum multiplication
 *
 * Multiplies a bignum with an unsigned integer and stores result
 * to another a bignum.
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   first multiplicand
 * \param[in]  op2
 *   second multiplicand
 */
void bn_mul_ui(bn_t res, bn_t op1, unsigned long op2);


/**
 * \brief bignum division
 *
 * A bignum division, the result is rounded
 * towards zero. The function returns only the quotient.
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   dividend
 * \param[in]  op2
 *   divider
 */
void bn_tdiv_q(bn_t res, bn_t op1, bn_t op2);


/**
 * \brief bignum division
 *
 * A bignum is divided by an unsigned integer, the result is rounded
 * towards zero. The function returns only the quotient.
 *
 * \param[out] res result
 * \param[in]  op1
 *   dividend
 * \param[in]  op2
 *   divider
 */
void bn_tdiv_q_ui(bn_t res, bn_t op1, unsigned long op2);


/**
 * \brief bignum modular division
 *
 * Bignum modular division.
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   dividend
 * \param[in]  op2
 *   divider
 */
void bn_mod(bn_t res, bn_t op1, bn_t op2);


/**
 * \brief bignum modular division
 *
 * Bignum modular division.
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   dividend
 * \param[in]  op2
 *   divider
 */
void bn_mod_ui(bn_t res, bn_t op1, unsigned long op2);


/**
 * \brief bignum exponentiation
 *
 * Computes the result of op1 raised to op2.
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   base
 * \param[in]  op2
 *   exponent
 */
void bn_ui_pow_ui(bn_t res, unsigned long op1, unsigned int op2);


/**
 * \brief bignum multiplicative inverse
 *
 * Computes the multiplicative inverse of op1 modulo op2.
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   input
 * \param[in]  op2
 *   modulo
 *
 * \note
 * If the inverse does not exist, then the result value is not defined.
 */
void bn_invert(bn_t res, bn_t op1, bn_t op2);


/**
 * \brief compares a bignum to a given unsigned integer
 *
 * Compares op1 and op2, returns positive value if op1 > op2,
 * negative if op1 < op2, zero else.
 *
 * \param[in] op1
 *   first compared number
 * \param[in] op2
 *   second compared number
 * \return
 *   positive if op1 > op2, negative if op1 < op2, 0 else
 */
signed int bn_cmp_ui(bn_t op1, unsigned long op2);


#ifdef __cplusplus
}
#endif

#endif /* _BIGNUM_H_ */
