/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef _INTERFACE_H_
#define _INTERFACE_H_


#include <stdio.h>

#include "base.h"
#include "cpu_rsa.h"
#include "gpu_rsa.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * \brief write a byte-wise representation of a number
 *
 * Writes a byte-wise representation of the given bignum number.
 *
 * \param[out] fout
 *   output file
 * \param[in]  m
 *   pointer to a number
 * \param[in]  bytes
 *   number of bytes of the number
 * \param[in]  cid
 *   identifier
 */
void rad_fwrite(FILE *fout, const unsigned char *m,
                size_t bytes, const char cid);

#define mess_fwrite(fout, mess) rad_fwrite((fout), (mess), MESSBYTES, 'm')


/**
 * \brief read a byte-wise represented message
 *
 * Reads a byte-wise representation of a given number.
 *
 * \param[in]  fin
 *   input file
 * \param[out] m
 *   number with the message length
 * \param[in]  bytes
 *   number of bytes of the number to be read
 * \param[in]  cid
 *   identifier
 */
void rad_fread(FILE *fin, unsigned char *m,
               size_t bytes, const char cid);

#define mess_fread(fin, mess) rad_fread((fin), (mess), MESSBYTES, 'm')


/**
 * \brief dumps a key to a file
 *
 * Writes a key to a text-file.
 *
 * \param[out] fout
 *   output file
 * \param[in]  key
 *   RSA key
 */
void key_fwrite(FILE *fout, const rsa_key *key);


/**
 * \brief reads a key from a file
 *
 * Reads a key from a text-file.
 *
 * \param[in] fin
 *   input file
 * \param[in] key
 *   RSA key
 */
void key_fread(FILE *fin, rsa_key *key);


/**
 * \brief dumps message-package into file
 *
 * Writes all messages to a binary file.
 *
 * \param[out] fbout
 *   binary file to be written
 * \param[in]  host_mess_arr
 *   array holding messages to be written to file
 */
void dump_messages_binary(FILE *fbout,
                          const unsigned char host_mess_arr[MESSBYTES*CIPHERS]);


/**
 * \brief reads message-package from a file
 *
 * Reads message-package from a binary file.
 *
 * \param[in]  fbin
 *   binary file to be read
 * \param[out] host_mess_arr
 *   memory to be written
 *
 * \note
 * No checks for message length are performed.
 */
void read_messages_binary(FILE *fbin,
                          unsigned char host_mess_arr[MESSBYTES*CIPHERS]);


/**
 * \brief write a base64 representation of a message
 *
 * Writes a base64 representation of a given message with the byt.
 *
 * \param[out] fout  Output file
 * \param[in]  m     Message
 * \param[in]  bytes Number of bytes to be tranformed into base64
 */
void rad_fwrite_base64(FILE *fout, unsigned char *m, size_t bytes);


/**
 * \brief reads a base64 representation of a message
 *
 * Reads a base 64 representation of a given message.
 *
 * \param[in]  fin   Input file
 * \param[out] m     Mesage
 * \param[in]  bytes Maximal number of bytes of message to be written
 */
void rad_fread_base64(FILE *fin, unsigned char *m, size_t bytes);


#ifdef __cplusplus
}
#endif


#endif /* _INTREFACE_H_ */
