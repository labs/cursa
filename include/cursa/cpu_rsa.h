/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef _CPU_RSA_H_
#define _CPU_RSA_H_


#include "base.h"


#ifdef __cplusplus
extern "C" {
#endif


/**
 * \brief compute RSA in RNS on the CPU
 *
 * Performs a computation of RSA. The function uses values which are
 * pre-computed in the initialization phase. Sets m = C**exp_rad mod n.
 *
 * \param[out] m
 *   encrypted message
 * \param[in]  C
 *   message to be encrypted
 * \param[in]  exp_rad
 *   exponent in radix representation
 * \param[in]  exp_bytes
 *   number of bytes of the exponent
 *
 * \note
 * It is better to pass the minimum number of bytes which can hold the exponent
 * instead of the actual size of the exponent. This can increase performance
 * when using small exponents. (When a 1024-bit exponent stores the number 13,
 * then the byte-size of the exponent can be set to 1.)
 *
 * \note
 * The function exists for software development purposes only, it is
 * not intended to be used in real application.
 */
void cpu_rsa(unsigned char m[MESSBYTES],
             unsigned char C[MESSBYTES],
             unsigned char *exp_rad, size_t exp_bytes);


#ifdef __cplusplus
}
#endif


#endif /* _CPU_RSA_H_ */
