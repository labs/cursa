/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef _BASE_H_
#define _BASE_H_


#ifdef __cplusplus
extern "C" {
#endif


/**
 * \brief 1024-bit message bytes
 *
 * Number of bytes in a 1024-bit message.
 *
 * \note
 * The number stored in the message must me smaller than the used modulo
 * divider ;).
 */
#define MESSBYTES1024 128                  /* (1024/8) */
/**
 * \brief number of moduli in the used RNS bases
 *
 * Number of 32-bit moduli in a RNS base needed to represent a 1024-bit number
 * during the RNS modular exponentiation process.
 *
 * \note
 * The moduli are 32-bit primes with MSB set to 1. The dynamic ranges of
 * the used RNS bases A and B have to satisfy the following conditions:
 * A > B; A,B > 8N, where N is the selected 1024-bit modulo divider.
 */
#define MODULINUM1024_32 34                /* ((1024+3)/31) rounded up */
/**
 * \brief redundant modulus bits
 *
 * Redundant modulus is used mainly in the CRT based base extension algorithms.
 * It is equal to log2(MODULINUM1024_32) rounded up. 
 *
 * \note
 * The computed redundant modulus cannot be larger than the number of moduli
 * in the used RNS base. In order to optimize performance it is selected to be
 * a power of 2.
 */
#define REDUNDANTBITS1024_32 6              /* 2**6 >= MODULINUM1024_32 */
/**
 * \brief redundant modulus bit mask
 *
 * Marks bits used to express a valid redundant modulus.
 */
#define REDUNDANTMASK1024_32 63            /* 2**6 - 1 */
/**
 * \brief Kawamura's alpha coefficient
 *
 * Coefficient used for error estimation in the CRT based Kawamura's base
 * extension algorithm.
 */
#define ALPHA01024_32 0xf000               /* magic alpha :) */


#define MESSBYTES2048        256           /* (2048/8) */
#define MODULINUM2048_32     67            /* ((2048+3)/31) rounded up */
#define REDUNDANTBITS2048_32 7             /* 2**7 >= MODULINUM2048_32 */
#define REDUNDANTMASK2048_32 127           /* 2**7 - 1 */
#define ALPHA02048_32        0x1e000       /* needs testing */


#define MESSBYTES4096        512           /* (4096/8) */
#define MODULINUM4096_32     133           /* ((4096+3)/31) rounded up */
#define REDUNDANTBITS4096_32 8             /* 2**8 >= MODULINUM4096_32 */
#define REDUNDANTMASK4096_32 255           /* 2**8 - 1 */
#define ALPHA04096_32        0x3c000       /* needs testing */


/**
 * \brief 32-bit moduli type
 */
typedef unsigned int moduli32_t;


/**
 * \brief RNS base a
 *
 * RNS base a consisting 32-bit primes needed to encode a 1024-bit number
 * during the  modular exponentiation.
 */
extern
const moduli32_t rns_basea1024_32[MODULINUM1024_32],
/**
 * \brief RNS base b
 *
 * RNS base b consisting 32-bit primes needed to encode a 1024-bit number
 * during the modular exponentiation.
 */
                 rns_baseb1024_32[MODULINUM1024_32];


/**
 * \brief RNS base a
 *
 * RNS base a consisting 32-bit primes needed to encode a 2048-bit number
 * during the  modular exponentiation.
 */
extern
const moduli32_t rns_basea2048_32[MODULINUM2048_32],
/**
 * \brief RNS base b
 *
 * RNS base a consisting 32-bit primes needed to encode a 2048-bit number
 * during the  modular exponentiation.
 */
                 rns_baseb2048_32[MODULINUM2048_32];


/**
 * \brief RNS base a
 *
 * RNS base a consisting 32-bit primes needed to encode a 4096-bit number
 * during the  modular exponentiation.
 */
extern
const moduli32_t rns_basea4096_32[MODULINUM4096_32],
/**
 * \brief RNS base a
 *
 * RNS base a consisting 32-bit primes needed to encode a 4096-bit number
 * during the  modular exponentiation.
 */
                 rns_baseb4096_32[MODULINUM4096_32];

/**
 * \brief RNS base a moduli complements
 *
 * The RNS base a moduli complements are used in division-less modular
 * multiplication and addition implementation when encoding 1024-bit numbers.
 * (c[i] = 0-a[i]; 0 == 2**(moduli bit width))
 */
extern
moduli32_t rns_basea1024_32comp[MODULINUM1024_32],
/**
 * \brief RNS base b moduli complements
 *
 * RNS base a moduli complements. These are used in division-less modular
 * multiplication and addition implementation when encoding 1024-bit numbers.
 * (c[i] = 0-b[i]; 0 == 2**(moduli bit width))
 */
           rns_baseb1024_32comp[MODULINUM1024_32];


/**
 * \brief RNS base a moduli complements
 *
 * The RNS base a moduli complements are used in division-less modular
 * multiplication and addition implementation when encoding 2048-bit numbers.
 * (c[i] = 0-a[i]; 0 == 2**(moduli bit width))
 */
extern
moduli32_t rns_basea2048_32comp[MODULINUM2048_32],
/**
 * \brief RNS base b moduli complements
 *
 * The RNS base b moduli complements are used in division-less modular
 * multiplication and addition implementation when encoding 2048-bit numbers.
 * (c[i] = 0-a[i]; 0 == 2**(moduli bit width))
 */
           rns_baseb2048_32comp[MODULINUM2048_32];


/**
 * \brief RNS base a moduli complements
 *
 * The RNS base a moduli complements are used in division-less modular
 * multiplication and addition implementation when encoding 4096-bit numbers.
 * (c[i] = 0-a[i]; 0 == 2**(moduli bit width))
 */
extern
moduli32_t rns_basea4096_32comp[MODULINUM4096_32],
/**
 * \brief RNS base b moduli complements
 *
 * The RNS base b moduli complements are used in division-less modular
 * multiplication and addition implementation when encoding 4096-bit numbers.
 * (c[i] = 0-a[i]; 0 == 2**(moduli bit width))
 */
           rns_baseb4096_32comp[MODULINUM4096_32];


/**
 * \brief CPU unsigned integer type
 *
 * Unsigned integer type used in some CPU based conversion functions. These
 * functions can utilize a CPU specific integer bit-width in order to convert
 * radix represented numbers into RNS more efficiently.
 *
 * \note
 * Currently implemented for 32-bit numbers.
 */
typedef unsigned int cpu_unum_t;


/**
 * \brief GPU moduli type
 *
 * The type to represent a single moduli in the GPU.
 *
 * \note
 * Currently implemented for 32-bit numbers.
 */
typedef moduli32_t moduli_t;


#if defined(RSA1024)
  #define MESSBYTES       MESSBYTES1024
  #define MODULINUM       MODULINUM1024_32
  #define REDUNDANTBITS   REDUNDANTBITS1024_32
  #define REDUNDANTMASK   REDUNDANTMASK1024_32
  #define rns_base_a      rns_basea1024_32
  #define rns_base_b      rns_baseb1024_32
  #define rns_base_a_comp rns_basea1024_32comp
  #define rns_base_b_comp rns_baseb1024_32comp
  #define ALPHA0          ALPHA01024_32
#elif defined(RSA2048)
  #define MESSBYTES       MESSBYTES2048
  #define MODULINUM       MODULINUM2048_32
  #define REDUNDANTBITS   REDUNDANTBITS2048_32
  #define REDUNDANTMASK   REDUNDANTMASK2048_32
  #define rns_base_a      rns_basea2048_32
  #define rns_base_b      rns_baseb2048_32
  #define rns_base_a_comp rns_basea2048_32comp
  #define rns_base_b_comp rns_baseb2048_32comp
  #define ALPHA0          ALPHA02048_32
#elif defined(RSA4096)
  #define MESSBYTES       MESSBYTES4096
  #define MODULINUM       MODULINUM4096_32
  #define REDUNDANTBITS   REDUNDANTBITS4096_32
  #define REDUNDANTMASK   REDUNDANTMASK4096_32
  #define rns_base_a      rns_basea4096_32
  #define rns_base_b      rns_baseb4096_32
  #define rns_base_a_comp rns_basea4096_32comp
  #define rns_base_b_comp rns_baseb4096_32comp
  #define ALPHA0          ALPHA04096_32
#else
#error "RSA bit width not defined of unsupported"
#endif


/**
 * \brief number of bytes used in the cipher
 */
#define CIPHBYTES MESSBYTES


/**
 * \brief number of bytes to represent a RNS number
 *
 * The number of bytes needed to store any number (within the bases dynamic
 * range) represented in RNS base a or base b after the conversion to radix
 * representation.
 *
 * \note
 * The number has to be a multiple of 8 in case when 64-bit numbers are used.
 * In general its the number of bytes to store the numbers A and B
 * which represent the dynamic ranges A and B.
 */
#define BASEBYTES ((((MODULINUM*sizeof(moduli_t))+7)/8)*8)


/**
 * \brief number of words (numbers) to represent any RNS number in CPU
 *
 * The number of integers needed to store any number represented
 * in RNS bases a or base b after the conversion to radix representation.
 *
 * \note
 * The number of words depends on the type used to represent a unsigned integer
 * in the CPU. It is needed mainly in the CPU conversion functions which may be
 * different than the functions used in the GPU.
 */
#define BASE_CPU_WORDS (BASEBYTES/sizeof(cpu_unum_t))


/**
 * \brief number of words (numbers) to represent any RNS number in GPU
 *
 * The number of integers needed to store any number represented
 * in RNS bases a or b after the conversion to radix representation.
 *
 * \note
 * The number of words depends on the type used to represent a unsigned integer
 * in the GPU. It is needed mainly in the GPU conversion functions which may be
 * different than the functions used in the CPU.
 */
#define BASE_GPU_WORDS (BASEBYTES/sizeof(moduli_t))
 

/**
 * \brief CPU radix to RNS base a representation conversion array
 *
 * The array is used for word-wise CPU conversion of plain binary (radix
 * represented) numbers into RNS base a representation.
 */
extern
moduli_t cpu_ww_radtornsa[MODULINUM][BASE_CPU_WORDS],
/**
 * \brief CPU radix to RNS base b representation conversion array
 *
 * The array is used for word-wise CPU conversion of plain binary (radix
 * represented) numbers into RNS base b representation.
 */
         cpu_ww_radtornsb[MODULINUM][BASE_CPU_WORDS];


/**
 * \brief GPU radix to RNS base a conversion array
 *
 * The array is used for word-wise GPU conversion of plain binary numbers
 * into RNS base a representation.
 */
extern
moduli_t gpu_ww_radtornsa[MODULINUM][BASE_GPU_WORDS],
/**
 * \brief GPU radix to RNS base b conversion array
 *
 * The array is used for word-wise GPU conversion of plain binary numbers
 * into RNS base b representation.
 */
         gpu_ww_radtornsb[MODULINUM][BASE_GPU_WORDS];


/**
 * \brief base a dynamic range
 *
 * The plain representation of the product of all moduli in base a.
 * Base a can represent any number x within the dynamic range 0 <= x < A.
 *
 * \note
 * M = product of all moduli mi in base m; mi = m[i] i-th moduli in base m
 *
 * If x_rad is the number to be stored in base a with the dynamic range A_rad
 * then the RNS base a representation of x - x_rnsa - holds the number x mod A.
 * Therefore there exists a relation x_rad = x_rnsa + k*A for some k in the
 * set {0, 1, ...}.
 */
extern
unsigned char A_rad[BASEBYTES],
/**
 * \brief base b dynamic range
 *
 * The plain representation of the product of all moduli in base b.
 * Base b can represent any number x withing the dynamic range 0 <= x < B.
 */
              B_rad[BASEBYTES];


/**
 * \brief A_rm = A modulo redundant modulus
 *
 * The dynamic range of base a divided by redundant modulus.
 */
extern
moduli_t A_rm,
/**
 * \brief B_r = B modulo redundant modulus
 *
 * The dynamic range of base b divided by redundant modulus.
 */
         B_rm;


/**
 * \brief dynamic range of base a divided by moduli ai
 *
 * The plain representation of Ai, where Ai = A / ai.
 */
extern
unsigned char Ai_rad[MODULINUM][BASEBYTES],
/**
 * \brief dynamic range of base b divided by moduli bi
 *
 * The plain representation of Bis, where Bi = B / bi.
 */
              Bi_rad[MODULINUM][BASEBYTES];


/**
 * \brief Ai modulo redundant modulus
 */
extern
moduli_t Ai_rm[MODULINUM],
/**
 * \brief Bi modulo redundant modulus
 */
         Bi_rm[MODULINUM];


/**
 * \brief multiplicative inverse of Ai modulo ai
 *
 * \note
 * The multiplicative inverse exists if all moduli in the base are pairwise
 * co-prime.
 */
extern
moduli_t Ai_1ai[MODULINUM],
/**
 * \brief multiplicative inverse of Bi modulo bi
 */
         Bi_1bi[MODULINUM];


/**
 * \brief -A in RNS base b representation
 *
 * \note
 * In base b the numbers 0 and B are equivalent.
 * Therefore -1 == (0-1) == (B-1), ((B-1)*A = -A) modulo B.
 */
extern
moduli_t minusA_rnsb[MODULINUM],
/**
 * \brief -B in RNS base a representation
 *
 * \note
 * In base a the numbers 0 and A are equivalent.
 * Therefore -1 == (0-1) == (A-1), ((A-1)*B = -B) modulo A.
 */
         minusB_rnsa[MODULINUM];


/**
 * \brief Ai in base b representation
 */
extern
moduli_t Ai_rnsb[MODULINUM][MODULINUM],
/**
 * \brief Bi in base a representation
 */
         Bi_rnsa[MODULINUM][MODULINUM];


/**
 * \brief multiplicative inverse of A modulo redundant modulus
 *
 * \note
 * The multiplicative inverse exists if all moduli in the base are odd.
 * The redundant modulus is chosen to be a power of 2, therefore
 * the numbers are co-prime.
 */
extern
moduli_t A_1rm,
/**
 * \brief multiplicative inverse of B modulo redundant modulus
 *
 * (B * B_1rm) % redundant modulus = 1
 */
         B_1rm;


/**
 * \brief negative multiplicative inverse of B modulo n in base b representation
 *
 * \note
 * n is the used RSA modulus.
 * The number satisfies the equation B*B_1n - n*n_neg1B = 1,
 * If we lay the equation modulo n we get B*B_1n = 1.
 * B_1n is the multiplicative inverse of B modulo n.
 * If we lay the equation modulo B we get n*n_neg1B = -1 and
 * n_neg1B == -n_1B == (B-1)*n_1B.
 */
extern
moduli_t n_neg1B_rnsb[MODULINUM];


/**
 * \brief RSA modulus represented in both bases
 */
extern
moduli_t n_rnsab[MODULINUM*2];


/**
 * \brief multiplicative inverse of B modulo A in base a
 *
 * (B*B_1A) % A = 1
 */
extern
moduli_t B_1A_rnsa[MODULINUM];


/**
 * \brief B*B modulo n in both bases
 *
 * B**2 % n
 */
extern
moduli_t B2_n_rnsab[MODULINUM*2];


/**
 * \brief 1 in both bases
 *
 * \note
 * Contains ones.
 */
extern
moduli_t ones_rnsab[MODULINUM*2];


/**
 * \brief used for conversion into RNS via MRS
 *
 * mrs_Paj_1ai[0] = 1
 * mrs_Paj_1ai[i] = (product of moduli aj with index j less than i) % ai
 */
extern
moduli_t mrs_Paj_1ai[MODULINUM],
/**
 * \brief used for conversion int RNS via MRS
 *
 * mrs_Pbj_1bi[0] = 1
 * mrs_Pbj_1bi[i] = (product of moduli bj with index j less than i) % bi
 */
         mrs_Pbj_1bi[MODULINUM];


/**
 * \brief used for conversion into RNS via MRS
 *
 * mrs_Paj_ai[.][0] = 1;
 * mrs_Paj_ai[i][j] = (product of moduli aj with index j less than j) % ai
 */
extern
moduli_t mrs_Paj_ai[MODULINUM][MODULINUM],
/**
 * \brief used for conversion into RNS via MRS
 *
 * mrs_Pbj_bi[.][0] = 1;
 * mrs_Pbj_bi[i][j] = (product of moduli bj with index j less than j) % bi
 */
         mrs_Pbj_bi[MODULINUM][MODULINUM];


/**
 * \brief used for MRS based base extension
 *
 * Incremental products of moduli aj with indexes j < i modulo bi.
 */
extern
moduli_t mrs_Paji_bi[MODULINUM][MODULINUM],
/**
 * \brief used for MRS based base extension
 *
 * Incremental products of moduli bj with indexes j < i modulo ai.
 */
         mrs_Pbji_ai[MODULINUM][MODULINUM];


/**
 * \brief used for conversion from MRS to radix representation
 *
 * Radix represented
 * mrs_Paji[0] = 1
 * mrs_Paji[i] = (product of moduli aj with index j less than i)
 */
extern
unsigned char mrs_Paji[MODULINUM][BASEBYTES],
/**
 * \brief used for conversion from MRS to radix representation
 *
 * Radix represented
 * mrs_Pbji[0] = 1
 * mrs_Pbji[i] = (product of moduli bj with index j less than i)
 */
              mrs_Pbji[MODULINUM][BASEBYTES];


/**
 * \brief RSA key structure
 *
 * RSA key structure definition. The numbers are stored in little-endian
 * ordered arrays of bytes.
 */
typedef struct
{
  /** number of bits used in the cipher is currently hard-wired */
  /*unsigned int bits;*/
  /** public exponent */
  unsigned char public_exp[CIPHBYTES],
  /** modulus */
                modulus[CIPHBYTES],
  /** private exponent */
                private_exp[CIPHBYTES];
} rsa_key;


/**
 * \brief initialize RNS and dependent stuff
 *
 * Initializes all needed constants. These constants are derived from RNS bases
 * and from given RSA modulo divider n.
 *
 * \param[in] key
 *   RSA key used in the cipher
 *
 * \note
 * In order to change the key the function has to be called with the desired
 * key.
 */
void cpu_init_constants(const rsa_key *key);


#ifdef __cplusplus
}
#endif


#endif /* _BASE_H_ */
