/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <gmp.h>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>

#include "cursa/base.h"
#include "cursa/io.h"
#include "cursa/gpu_rsa.h"


/*=======================================================================*/
/* prints printable bytes of message c into ofile */
void fprint_as_string(FILE * ofile, unsigned char *c, signed int bytes)
/*=======================================================================*/
{
  bytes--;
  for (; bytes>=0; bytes--) {
    fputc((c[bytes] < 32) || (c[bytes] > 127) ? '.' : c[bytes], ofile);
  }
  fputc('\n', ofile);
}


/*=======================================================================*/
/* compares two messages for equality - 1 (equal), 0 (different) */
signed int mess_eq(unsigned char m1[MESSBYTES], unsigned char m2[MESSBYTES])
/*=======================================================================*/
{

  unsigned long i;
  signed int equal = 1;

  if (MESSBYTES % sizeof(unsigned long))
  {
    /* byte-wise */
    i = 0;
    while (equal && (i < MESSBYTES))
    {
      equal = (m1[i] == m2[i]);
      i++; 
    }
  }
  else
  {
    /* long-wise */
    i = 0;
    while (equal && (i < (MESSBYTES / sizeof(unsigned long))))
    {
      equal = ( ((unsigned long*)m1)[i] == ((unsigned long*)m2)[i]);
      i+=sizeof(unsigned long);
    }
  }

  return equal;

}


/*=======================================================================*/
/*=======================================================================*/
int main(void)
/*=======================================================================*/
/*=======================================================================*/
{

  unsigned char m[MESSBYTES] = {0,};
  unsigned char c[MESSBYTES] = {0,};
  unsigned char res2[MESSBYTES] = {0,};
  unsigned char res3[MESSBYTES] = {0,};

  rsa_key key;

  size_t bytes;
  mpz_t gn, ge, gd, gm, gc, gres;

  signed int i;

  mpz_init(gn); mpz_init(ge); mpz_init(gd);
  mpz_init(gm); mpz_init(gc); mpz_init(gres);

  gmp_sscanf("af4adcd090f39afe0a3a10f898358cea02bd136c1586df1545b8494fd07e3b675111b45e6740f06e466a0d75cc21cf0ffb707a53362fe2619621826ef1c1018fa2f2b8a1b8e9478975a591ecb69228988a2b620ff0d2eb207686c14d89060c2e6ad558362cef93e872d1d43d3016228dbacb706c4d14322693e935c51c5ab99d",
             "%Zx", gn);
  memset(key.modulus, 0, MESSBYTES);
  mpz_export(key.modulus, &bytes, -1, 1, 0, 0, gn);
  if (MESSBYTES < bytes)
  {
    fprintf(stderr, "%s: n written %lu instead of %lu bytes\n",
            __func__, (unsigned long)bytes, (unsigned long)MESSBYTES);
    exit(1);
  }

#define EXP 3 //65537
  mpz_set_ui(ge, EXP);
  memset(key.public_exp, 0, CIPHBYTES);
  *((unsigned long*)key.public_exp) = EXP;

  gmp_sscanf("74dc9335b5f7bca95c26b5fb1023b346ac7e0cf2b90494b8d925863535a97cef8b6122e99a2b4af42ef15e4e88168a0aa7a0518cceca96ebb96bac49f680abb3fcd68560f8014aea177d6fc3405bfd9282a785da14d3318cc1d30d946d108ac37bac4c80d786cb51cf180c8db357964bfab6ea92f0253f5c595f7029fdb956ab",
             "%ZX", gd);
  memset(key.private_exp, 0, MESSBYTES);
  mpz_export(key.private_exp, &bytes, -1, 1, 0, 0, gd);
  if (MESSBYTES < bytes)
  {
    fprintf(stderr, "%s: d written %lu instead of %lu bytes\n",
            __func__, (unsigned long)bytes, (unsigned long)MESSBYTES);
    exit(1);
  }

  gmp_sscanf("6fd1dff3bf55ecfd0afa235a62f4cc55b0c7e57fdd32e417e0ed8338e1f81fe67a458e3ab541e275f2d9f38cddbcf560f01ea75962b07e5e4b04fcfc103a154ec1fadac771f6b14d00c71ca2305111712a8871c7540ad4ddff20da0db10e209839dbe41b72ade528e66f93d260c392343be6d7c62bd5ef3901bd6096bee9a9de",
             "%ZX", gm);
  memset(m, 0, MESSBYTES);
  mpz_export(m, &bytes, -1, 1, 0, 0, gm);
  if (MESSBYTES < bytes)
  {
    fprintf(stderr, "%s: m written %lu instead of %lu bytes\n",
            __func__, (unsigned long)bytes, (unsigned long)MESSBYTES);
    exit(1);
  }

  /* initialization of all RNS RSA constants */
  cpu_init_constants(&key);

  fprintf(stdout, "encryption\n");
  cpu_rsa(c, m, key.private_exp, CIPHBYTES);      // large exponent
  fprintf(stdout, "decryption\n");
  cpu_rsa(res2, c, key.public_exp, CIPHBYTES);   // small exponent

  fprint_as_string(stdout, c, MESSBYTES);
  fprint_as_string(stdout, res2, MESSBYTES);

////////////////////////////////////////////////////////////////////////////
  gpu_seach_devs();
  gpu_print_dev_info();
  /* loading RNS RSA constants into GPU */
  gpu_init_constants(&key);
  gpu_structs_create();
////////////////////////////////////////////////////////////////////////////

  /* memory must be page locked in order to use async memory copies */
  unsigned char *host_out_rad_arr; //[MESSBYTES*CIPHERS]
  unsigned char *host_in_rad_arr;  //[MESSBYTES*CIPHERS]
  signed int    *host_corrupt;     //[CIPHERS]

  malloc_pagelocked((void**)&host_out_rad_arr, MESSBYTES*CIPHERS);
  malloc_pagelocked((void**)&host_in_rad_arr, MESSBYTES*CIPHERS);
  malloc_pagelocked((void**)&host_corrupt, sizeof(signed int)*CIPHERS);

  /* prepare testing data */
  for (i = 0; i < CIPHERS; i++)
  {
    memcpy(&host_in_rad_arr[MESSBYTES*i], m, MESSBYTES); // large exponent
    //memcpy(&host_in_rad_arr[MESSBYTES*i], c, MESSBYTES); // small exponent
    if (i != (CIPHERS-1))
      *(int*)(&host_in_rad_arr[MESSBYTES*i + 4]) = i;
  }

  /*
  cudaEvent_t gpu_start, gpu_stop;
  float time, gpu_seconds; 
  cudaEventCreate(&gpu_start);
  cudaEventCreate(&gpu_stop);
  cudaEventRecord(gpu_start, 0);
  */

  struct timeval gpu_start, gpu_stop;
  float gpu_seconds;
  signed int incorrect;

  gettimeofday(&gpu_start, NULL);

  incorrect = gpu_launch_rsa_sign_verify(host_out_rad_arr, host_corrupt,
                                         host_in_rad_arr);

  gettimeofday(&gpu_stop, NULL);
  gpu_seconds = (float)(gpu_stop.tv_sec - gpu_start.tv_sec) +
                ((float)(gpu_stop.tv_usec - gpu_start.tv_usec))/1000000;
  fprintf(stdout, "gpu elapsed time: %f sec\n", gpu_seconds);
  fprintf(stdout, "gpu comuted signatures: %d sig\n", CIPHERS);
  fprintf(stdout, "gpu signatures per second %f sig/sec\n",
          ((float)(CIPHERS))/gpu_seconds);

  fprintf(stdout, "total of %d messages computer incorrectly\n", incorrect);
  for (i = 0; i < CIPHERS; i++)
  {
    if (host_corrupt[i] != 0)
    {
      fprintf(stdout, "message %u computed incorrectly\n", i);
    }
  }

  /* testing encryption for errors */
  int all_equal = 1;
  mpz_t gtest;
  /* too large to be allocated on stack */
  unsigned char *test; //[MESSBYTES*CIPHERS];
  if ((test = (unsigned char*) malloc(MESSBYTES*CIPHERS)) == NULL)
  {
    fprintf(stderr, "error allocating test cipher bundle\n");
    exit(1);
  }
  mpz_init(gtest);
  clock_t cpu_start, cpu_stop;
  float cpu_seconds;
  cpu_start = clock();
  for (i = 0; i < CIPHERS; i++)
  {
    mpz_import(gtest, MESSBYTES, -1, 1, 0, 0, &host_in_rad_arr[i*MESSBYTES]);
    mpz_powm(gtest, gtest, gd, gn); // large exponent
    //mpz_powm(gtest, gtest, ge, gn); // small exponent
    mpz_export(&test[i*MESSBYTES], &bytes, -1, 1, 0, 0, gtest);
    if (bytes > MESSBYTES)
    {
      fprintf(stdout, "gmp result too large\n");
      exit(1);
    }
  }
  cpu_stop = clock();
  cpu_seconds = ((float)(cpu_stop - cpu_start))/CLOCKS_PER_SEC;
  fprintf(stdout, "cpu elapsed time: %f sec\ncpu computed signatures: %d sig\ncpu signatures per second %f sig/sec\n", cpu_seconds, CIPHERS, CIPHERS/cpu_seconds);
  for (i = 0; i < CIPHERS; i++)
  {
    //fprint_as_string(stdout, &host_out_rad_arr[MESSBYTES*i], MESSBYTES);
    all_equal = all_equal && mess_eq(&host_out_rad_arr[MESSBYTES*i],
                                     &test[i*MESSBYTES]);
  }
  mpz_clear(gtest);
  free(test);
  if (all_equal)
  {
    fprintf(stdout, "all messages computed correctly\n");
  }
  else
  {
    fprintf(stdout, "some messages differ\n");
    exit(1);
  }

  fprintf(stdout, "m:\n");
  //rns_fwriteradnum(stdout, m, MESSBYTES);
  fprintf(stdout, "rns c:\n");
  //rns_fwriteradnum(stdout, c, MESSBYTES);
  fprintf(stdout, "gmp c:\n");
  gmp_fprintf(stdout, "%Zd\n", gc);

  fprintf(stdout, "\ncpu rns res:\n");
  //rns_fwriteradnum(stdout, res2, MESSBYTES);
  fprintf(stdout, "\ngpu rns res:\n");
  //rns_fwriteradnum(stdout, res3, MESSBYTES);

  gmp_fprintf(stdout, "gmp res:\n");
  gmp_fprintf(stdout, "%Zd\n", gres);

  fprint_as_string(stdout, c, MESSBYTES);
  fprint_as_string(stdout, res2, MESSBYTES);
  
  gpu_structs_destroy();

  free_pagelocked(host_out_rad_arr);
  free_pagelocked(host_in_rad_arr);
  free_pagelocked(host_corrupt);

  mpz_clear(gn); mpz_clear(ge); mpz_clear(gd);
  mpz_clear(gm); mpz_clear(gc); mpz_clear(gres);

  return 0;

}
