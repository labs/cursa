/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <errno.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>

#include "cursa/cpu_rsa.h"
#include "cursa/gpu_rsa.h"
#include "cursa/io.h"


/*=======================================================================*/
/* returns number of messages computed incorrectly */
signed int gpu_signature(RSA *key, unsigned char invalid[CIPHERS],
                         unsigned char host_plain_arr[MESSBYTES*CIPHERS],
                         unsigned char host_encoded_arr[MESSBYTES*CIPHERS],
                         unsigned char host_decoded_arr[MESSBYTES*CIPHERS],
                         signed int    invalid_gpu_detected[CIPHERS],
                         rsa_key       *cursa_key)
/*=======================================================================*/
{

  size_t bytes;

  signed int i, j;
  signed int incorrect_separate,
             incorrect_gpu_detected;

  struct timeval gpu_start, gpu_stop;
  float gpu_seconds;

  FILE *logf;

  bytes = key->e->top * BN_BYTES;
  if (bytes > MESSBYTES)
  {
    fprintf(stderr, "public exponent too large\n");
    exit(1);
  }
  memset(cursa_key->public_exp, 0, CIPHBYTES);
  memcpy(cursa_key->public_exp, key->e->d, bytes);

  bytes = key->n->top * BN_BYTES;
  if (bytes > MESSBYTES)
  {
    fprintf(stderr, "modulus too large\n");
    exit(1);
  }
  memset(cursa_key->modulus, 0, CIPHBYTES);
  memcpy(cursa_key->modulus, key->n->d, bytes);

  bytes = key->d->top * BN_BYTES;
  if (bytes > MESSBYTES)
  {
    fprintf(stdout, "private exponent too large\n");
    exit(1);
  }
  memset(cursa_key->private_exp, 0, CIPHBYTES);
  memcpy(cursa_key->private_exp, key->d->d, bytes);

  cpu_init_constants(cursa_key);
  gpu_init_constants(cursa_key);

  gpu_structs_create();

  /* initialize messages */
  for (i = 0; i < CIPHERS; i++)
  {
    for(j = 0; j < MESSBYTES; j++)
    {
      host_plain_arr[i*MESSBYTES + j] = rand();
      if (j == (MESSBYTES-1))
      {
        host_plain_arr[i*MESSBYTES + j] = 127;
      }
    }
  }

  gettimeofday(&gpu_start, NULL);

  gpu_launch_rsa_sign(host_encoded_arr, host_plain_arr);

  gettimeofday(&gpu_stop, NULL);

  gpu_launch_rsa_verify(host_decoded_arr, host_encoded_arr);

  gpu_seconds = (float)(gpu_stop.tv_sec - gpu_start.tv_sec) +
                ((float)(gpu_stop.tv_usec - gpu_start.tv_usec))/1000000;
  fprintf(stdout,
          "gpu elapsed time: %f sec\ngpu computed signatures: %d sig\n",
          gpu_seconds, CIPHERS);
  fprintf(stdout, "gpu signatures per second %f sig/sec\n",
          ((float)(CIPHERS))/gpu_seconds);

  incorrect_separate = 0;
  for (i = 0; i < CIPHERS; i++)
  {
    invalid[i] = 0;
    for (j = 0; j < MESSBYTES; j++)
    {
      invalid[i] = invalid[i] || (host_decoded_arr[i*MESSBYTES + j] !=
                                  host_plain_arr[i*MESSBYTES + j]);
    }
    incorrect_separate += invalid[i] ? 1 : 0;
  }

  incorrect_gpu_detected = gpu_launch_rsa_sign_verify(host_encoded_arr,
                                                      invalid_gpu_detected,
                                                      host_plain_arr);

  gpu_structs_destroy();

  fprintf(stdout, "explicitly/GPU detected errors: %d/%d\n",
          incorrect_separate, incorrect_gpu_detected);

  signed int match = 1;
  for (i = 0; i < CIPHERS; i++)
  {
    match &= match && (invalid[i] == invalid_gpu_detected[i]);
  }

  fprintf(stdout, "matching incorrect marks: %s\n", match ? "true" : "false");

  if ((!match) || (incorrect_gpu_detected != incorrect_separate))
  {
    fprintf(stdout, "error while detecting error\n");

    /* dump key */
    if ((logf = fopen("invalid_key.txt", "w")) == NULL)
    {
      perror("error opening key log file");
      exit(1);
    }
    key_fwrite(logf, cursa_key);
    RSA_print_fp(logf, key, 0);
    fflush(logf);
    fclose(logf);

    /* dump messages */
    if ((logf = fopen("invalid_messages.dat", "wb")) == NULL)
    {
      perror("error opening messages log file");
      exit(1);
    }
    dump_messages_binary(logf, host_plain_arr);
    fflush(logf);
    fclose(logf);

    exit(1);
  }

  return incorrect_separate;

}


/*=======================================================================*/
signed int cpu_signature_ok(RSA *key, unsigned char plain[MESSBYTES],
                            rsa_key *cursa_key)
/*=======================================================================*/
{

  size_t bytes;

  unsigned char encoded[MESSBYTES] = {0, };
  unsigned char decoded[MESSBYTES] = {0, };

  signed int i;
  signed int ok;

  bytes = key->e->top * BN_BYTES;
  if (bytes > MESSBYTES)
  {
    fprintf(stderr, "public exponent too large\n");
    exit(1);
  }
  memset(cursa_key->public_exp, 0, CIPHBYTES);
  memcpy(cursa_key->public_exp, key->e->d, bytes);

  bytes = key->n->top * BN_BYTES;
  if (bytes > MESSBYTES)
  {
    fprintf(stderr, "modulus too large\n");
    exit(1);
  }
  memset(cursa_key->modulus, 0, CIPHBYTES);
  memcpy(cursa_key->modulus, key->n->d, bytes);

  bytes = key->d->top * BN_BYTES;
  if (bytes > MESSBYTES)
  {
    fprintf(stderr, "private exponent too large\n");
    exit(1);
  }
  memset(cursa_key->private_exp, 0, CIPHBYTES);
  memcpy(cursa_key->private_exp, key->d->d, bytes);

  cpu_init_constants(cursa_key);

  cpu_rsa(encoded, plain, cursa_key->public_exp, CIPHBYTES);
  cpu_rsa(decoded, encoded, cursa_key->private_exp, CIPHBYTES);

  ok = 1;
  for (i = 0; i < MESSBYTES; i++)
  {
    ok = ok && (decoded[i] == plain[i]);
  }

  return ok;

}


/*=======================================================================*/
/*=======================================================================*/
int main(int argc, char *argv[])
/*=======================================================================*/
/*=======================================================================*/
{

  RSA *key;
#define ERRBUFLEN 512
  char err_str[ERRBUFLEN];

  signed int i, j;
  FILE *ff;
  unsigned int seed;

  unsigned char *host_plain_arr;   //[MESSBYTES*CIPHERS]
  unsigned char *host_encoded_arr; //[MESSBYTES*CIPHERS]
  unsigned char *host_decoded_arr; //[MESSBYTES*CIPHERS]

  signed int    *invalid_gpu_detected; //[CIPHERS]

  rsa_key cursa_key;

  signed int wrong_signatures;

  unsigned char message_wrong[CIPHERS];

  struct timeval time;
#define LOGFNAMELEN 512
  char logfname[LOGFNAMELEN];
  FILE * logf;

  if (!gpu_seach_devs())
  {
    fprintf(stderr, "no GPU devices found\n");
    exit(1);
  }

  malloc_pagelocked((void**)&host_plain_arr, MESSBYTES*CIPHERS);
  malloc_pagelocked((void**)&host_encoded_arr, MESSBYTES*CIPHERS);
  malloc_pagelocked((void**)&host_decoded_arr, MESSBYTES*CIPHERS);
  malloc_pagelocked((void**)&invalid_gpu_detected, sizeof(signed int)*CIPHERS);

  if ((ff = fopen("/dev/urandom", "rb")) == NULL)
  {
    perror("opening urandom");
    exit(1);
  }

  if (fread(&seed, sizeof(unsigned int), 1, ff) != 1)
  {
    fprintf(stderr, "error reading urandom\n");
    exit(1);
  }

  fclose(ff);

  srand(seed);

#define ROUNDS 200000

  for (i = 0; i < ROUNDS; i++)
  {

    if ((key = RSA_generate_key(CIPHBYTES*8, 65537, NULL, NULL)) == NULL)
    {
      ERR_error_string_n(ERR_get_error(), err_str, ERRBUFLEN);
      fprintf(stderr, "error while generating key: %s\n", err_str);
      exit(1);
    }

    switch (RSA_check_key(key))
    {
      case 0:
        /* invalid key */
        fprintf(stderr, "generated invalid key\n");
        exit(1);
        break;
      case 1:
        /* key valid */
        break;
      case -1 :
        /* error */
        ERR_error_string_n(ERR_get_error(), err_str, ERRBUFLEN);
        fprintf(stderr, "error shile checking key: %s\n", err_str);
        exit(1);
        break;
      default :
        break;
    }

    fprintf(stdout, "%d/%d:\n", i, ROUNDS);
    if (wrong_signatures = gpu_signature(key, message_wrong, host_plain_arr,
                                         host_encoded_arr, host_decoded_arr,
                                         invalid_gpu_detected, &cursa_key))
    {

      for (j = 0; j < CIPHERS; j++)
      {
        gettimeofday(&time, NULL);

        if (message_wrong[j])
        {
          snprintf(logfname, LOGFNAMELEN, "invalid_%lu_%08d.txt", time.tv_sec, j);

          if ((logf = fopen(logfname, "w")) == NULL)
          {
            perror("error opening log file");
            exit(1);
          }

          fprintf(stderr, "message %d:\n", j);
          mess_fwrite(logf, &host_plain_arr[j*MESSBYTES]);
          key_fwrite(logf, &cursa_key);
          RSA_print_fp(logf, key, 0);

          fclose(logf);
        }
      }

      fprintf(stdout, "something somewhere is wrong\n");
    }

    fflush(stdout);
    RSA_free(key);
  }

  free_pagelocked(host_plain_arr);
  free_pagelocked(host_encoded_arr);
  free_pagelocked(host_decoded_arr);
  free_pagelocked(invalid_gpu_detected);

  return 0;

}
