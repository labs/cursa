/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "cursa/cpu_rsa.h"
#include "cursa/gpu_rsa.h"
#include "cursa/io.h"


/*=======================================================================*/
/* returns number of messages computed incorrectly */
signed int gpu_signature(unsigned char invalid_explicit_detected[CIPHERS],
                         const unsigned char host_plain_arr[MESSBYTES*CIPHERS],
                         unsigned char host_encoded_arr[MESSBYTES*CIPHERS],
                         unsigned char host_decoded_arr[MESSBYTES*CIPHERS],
                         signed int    invalid_gpu_detected[CIPHERS],
                         rsa_key       *cursa_key)
/*=======================================================================*/
{

  size_t bytes;

  signed int i, j;
  signed int incorrect_separate,
             incorrect_gpu_detected;

  struct timeval gpu_start, gpu_stop;
  float gpu_seconds;

  FILE *logf;

  cpu_init_constants(cursa_key);
  gpu_init_constants(cursa_key);

  gpu_structs_create();

  gettimeofday(&gpu_start, NULL);

  gpu_launch_rsa_sign(host_encoded_arr, host_plain_arr);

  gettimeofday(&gpu_stop, NULL);

  gpu_launch_rsa_verify(host_decoded_arr, host_encoded_arr);

  gpu_seconds = (float)(gpu_stop.tv_sec - gpu_start.tv_sec) +
                ((float)(gpu_stop.tv_usec - gpu_start.tv_usec))/1000000;
  fprintf(stdout,
          "gpu elapsed time: %f sec\ngpu computed signatures: %d sig\n",
          gpu_seconds, CIPHERS);
  fprintf(stdout, "gpu signatures per second %f sig/sec\n",
          ((float)(CIPHERS))/gpu_seconds);

  incorrect_separate = 0;
  for (i = 0; i < CIPHERS; i++)
  {
    invalid_explicit_detected[i] = 0;
    for (j = 0; j < MESSBYTES; j++)
    {
      invalid_explicit_detected[i] = invalid_explicit_detected[i] ||
                                     (host_decoded_arr[i*MESSBYTES + j] !=
                                      host_plain_arr[i*MESSBYTES + j]);
    }
    incorrect_separate += invalid_explicit_detected[i] ? 1 : 0;
  }

  incorrect_gpu_detected = gpu_launch_rsa_sign_verify(host_encoded_arr,
                                                      invalid_gpu_detected,
                                                      host_plain_arr);

  gpu_structs_destroy();

  fprintf(stdout, "explicitly/GPU detected errors: %d/%d\n",
          incorrect_separate, incorrect_gpu_detected);

  signed int match = 1;
  for (i = 0; i < CIPHERS; i++)
  {
    match &= match && (invalid_explicit_detected[i] ==
                       invalid_gpu_detected[i]);
  }

  fprintf(stdout, "matching error marks: %s\n", match ? "true" : "false");

  if ((!match) || (incorrect_gpu_detected != incorrect_separate))
  {
    fprintf(stdout, "error while detecting error\n");
    exit(1);
  }

  return incorrect_separate;

}


/*=======================================================================*/
/* print help message */
void print_help(FILE *fout)
/*=======================================================================*/
{

  fprintf(fout, "-k keyfile\n");
  fprintf(fout, "   reads the file containing the RSA keys\n");
  fprintf(fout, "-m messagefile\n");
  fprintf(fout, "   reads the file containing messages\n");
  fprintf(fout, "-h\n");
  fprintf(fout, "   prints this message\n");

}


/*=======================================================================*/
/*=======================================================================*/
int main(int argc, char *argv[])
/*=======================================================================*/
/*=======================================================================*/
{

  FILE *f;
  int ch;
#define FNAMELEN 512
  char keyfname[FNAMELEN],
       messfname[FNAMELEN];
  int keyfset,
      messfset;

  rsa_key cursa_key;

  unsigned char *host_mess_arr; //[MESSBYTES*CIPHERS]
  unsigned char *host_encoded_arr; //[MESSBYTES*CIPHERS]
  unsigned char *host_decoded_arr; //[MESSBYTES*CIPHERS]

  signed int    *invalid_gpu_detected; //[CIPHERS]

  unsigned char message_wrong[CIPHERS];

  signed int wrong_signatures;

  keyfname[0] = messfname[0] = '\0';
  keyfset = messfset = 0;

  while ((ch = getopt(argc, argv, "k:m:h")) != -1)
  {
    switch (ch)
    {

      case 'k':
        strncpy(keyfname, optarg, FNAMELEN - 1);
        keyfname[FNAMELEN-1] = '\0';
        keyfset = keyfname[0] != '\0';
        break;

      case 'm':
        strncpy(messfname, optarg, FNAMELEN - 1);
        messfname[FNAMELEN-1] = '\0';
        messfset = messfname[0] != '\0';
        break;

      case 'h':
        print_help(stdout);
        exit(0);
        break;

    }
  }

  /* open and read key file */
  if (!keyfset)
  {
    fprintf(stderr, "key file not set\n");
    exit(1);
  }
  if ((f = fopen(keyfname, "r")) == NULL)
  {
    fprintf(stderr, "error opening key-file %s\n", keyfname);
    exit(1);
  }
  key_fread(f, &cursa_key);
  fclose(f);
  

  /* open and read file containing messages */
  if (!messfset)
  {
    fprintf(stderr, "messages file not set\n");
    exit(1);
  }
  if ((f = fopen(messfname, "rb")) == NULL)
  {
    fprintf(stderr, "error opening file %s\n", messfname);
    exit(1);
  }

  malloc_pagelocked((void**)&host_mess_arr, MESSBYTES*CIPHERS);
  malloc_pagelocked((void**)&host_encoded_arr, MESSBYTES*CIPHERS);
  malloc_pagelocked((void**)&host_decoded_arr, MESSBYTES*CIPHERS);
  malloc_pagelocked((void**)&invalid_gpu_detected, sizeof(signed int)*CIPHERS);

  read_messages_binary(f, host_mess_arr);
  fclose(f);

  /* test signatures */
  wrong_signatures = gpu_signature(message_wrong, host_mess_arr,
                                   host_encoded_arr, host_decoded_arr,
                                   invalid_gpu_detected, &cursa_key);

  free_pagelocked(host_mess_arr);
  free_pagelocked(host_encoded_arr);
  free_pagelocked(host_decoded_arr);
  free_pagelocked(invalid_gpu_detected);

}
