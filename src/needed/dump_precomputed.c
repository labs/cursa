/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>

#include "cursa/base.h"
#include "cursa/gpu_rsa.h"


/*=======================================================================*/
/* prints help message */
void print_help(FILE *fout)
/*=======================================================================*/
{

  fprintf(fout, "-d file.py\n");
  fprintf(fout, "   dumps arrays to be used in python C code expander\n");
  fprintf(fout, "-h\n");
  fprintf(fout, "   prints this message\n");

}


/*=======================================================================*/
/*=======================================================================*/
int main(int argc, char *argv[])
/*=======================================================================*/
/*=======================================================================*/
{

  int ch;
  unsigned int dump_consts = 0;
  char dump_fname[512];

  rsa_key key;
  memset(key.public_exp, 0, CIPHBYTES);
  memset(key.modulus, 0, CIPHBYTES);
  memset(key.private_exp, 0, CIPHBYTES);
  key.modulus[0] = 13;

  /* parse command line */
  while ((ch = getopt(argc, argv, "d:h")) != -1)
  {

    switch (ch)
    {

      case 'd' : /* set flag to dump constant values */
        dump_consts = 1;
        if (strlen(optarg) >= 512)
        {
          fprintf(stderr, "name of dump file too long\n");
          exit(1);
        }
        strncpy(dump_fname, optarg, 512);
        break;

      case 'h' :
        print_help(stdout);
        exit(0);
        break;

      default :
        print_help(stdout);
        exit(0);
        break;

    }

  }

  /* initialization of all RNS RSA constants,
     the modulo divider does not have to be set correctly */
  cpu_init_constants(&key);

  /* dump constants into file and exit */
  if (dump_consts)
  {
    FILE * pyf;
    int to_file = 1;
    unsigned long namelen = 0;

    namelen = strlen(dump_fname);

    /* test dump file name */
    if ((namelen == 1) && (dump_fname[0] == '-'))
    {
      to_file = 0;
      pyf = stdout;
    }
    else if ((namelen <= 3) ||
             ((namelen > 3) &&
              (strncmp(&dump_fname[namelen-3], ".py", 3) != 0)))
    {
      fprintf(stdout, "output file name must contain \".py\" suffix\n");
      exit(1);
    }

    if (to_file && ((pyf = fopen(dump_fname, "w")) == NULL))
    {
      fprintf(stderr, "error opening file \"%s\" for writing\n", dump_fname);
      exit(1);
    }

    dump_modulus_independent_to_python(pyf);

    if (to_file)
    {
      fclose(pyf);
    }
  }

  return 0;

}
