#!/bin/sh

# A dirty workaround how to pass a several parameters to the nvcc compiler.

PARAMS=`echo $@ | sed -e 's/\(-fPIC\)/-Xcompiler \1/g' -e 's/-Wl,/-Xlinker /g'`
echo $PARAMS
`$PARAMS`
