/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <stdlib.h>
#include <string.h>

#include "cursa/base.h"
#include "cursa/cpu_rsa.h"
#include "cpu_asm.h"


/**
 * \brief word-wise CPU radix to RNS conversion
 *
 * Converts a number in radix (plain) representation into RNS representation.
 *
 * \param[out] rnsm
 *   RNS base_m representation of rad
 * \param[in]  rad
 *   number to be converted
 * \param[in]  bytes
 *   number of bytes of the converted number
 * \param[in]  base_m
 *   selected base
 * \param[in]  cpu_ww_radtornsm
 *   precomputed conversion values
 */
static
void cpu_rns_from_rad_ww(moduli_t rnsm[MODULINUM],
                         unsigned char *rad, size_t bytes,
                         const moduli_t base_m[MODULINUM],
                         moduli_t cpu_ww_radtornsm[MODULINUM][BASE_CPU_WORDS]);
/**
 * \brief converts a radix number into RNS base a
 *
 * Converts input radix number into RNS representation with base a.
 *
 * \param[out] rnsa
 *   RNS base a representation
 * \param[in]  rad
 *   radix representation
 * \param[in]  bytes
 *   number of bytes of the radix number
 */
#define cpu_rnsa_from_rad(rnsa, rad, bytes) cpu_rns_from_rad_ww((rnsa), \
  (rad), (bytes), rns_base_a, cpu_ww_radtornsa)
/**
 * \brief converts a radix number into RNS base b
 *
 * Converts input radix number into RNS representation with base b.
 *
 * \param[out] rnsb
 *   RNS base b representation
 * \param[in]  rad
 *   radix representation
 * \param[in]  bytes
 *   number of bytes of the radix number
 */
#define cpu_rnsb_from_rad(rnsb, rad, bytes) cpu_rns_from_rad_ww((rnsb), \
  (rad), (bytes), rns_base_b, cpu_ww_radtornsb)


/**
 * \brief converts a number from RNS into radix representation
 *
 * Converts input RNS number into radix representation using
 * MRS intermediate representation.
 *
 * \param[out] rad
 *   radix representation
 * \param[in]  maxbytes
 *   max number of bytes of the radix number
 * \param[in]  rnsm
 *   RNS representation of the number to be converted
 * \param[in]  basem
 *   RNS base
 * \param[in]  mrs_Pmj_1mi
 *   multiplicative inverse of the product of moduli mj with
 *   indexes j less than i
 * \param[in]  mrs_Pmj_mi
 *   incremental products of the moduli mj with indexes j less
 *   than i modulo mi
 * \param[in]  mrs_Pmji
 *   products of the moduli mj with indexes j less than i
 */
static
void cpu_rad_from_rns_mrs(unsigned char *rad, size_t maxbytes,
                          moduli_t rnsm[MODULINUM],
                          const moduli_t basem[MODULINUM],
                          moduli_t mrs_Pmj_1mi[MODULINUM],
                          moduli_t mrs_Pmj_mi[MODULINUM][MODULINUM],
                          unsigned char mrs_Pmji[MODULINUM][BASEBYTES]);
/**
 * \brief converts RNS number in base a into radix representation
 *
 * Converts input RNS number represented in base a into radix number.
 *
 * \param[out] rad
 *   radix representation
 * \param[in]  bytes
 *   max bytes to be written as radix number
 * \param[in]  rnsa
 *   RNS base a representation
 */
#define cpu_rad_from_rnsa(rad, bytes, rnsa) cpu_rad_from_rns_mrs((rad), \
  (bytes), (rnsa), rns_base_a, mrs_Paj_1ai, mrs_Paj_ai, mrs_Paji)
/**
 * \brief converts RNS number in base b into radix representation
 *
 * Converts input RNS number represented in base b into radix number.
 *
 * \param[out] rad
 *   radix representation
 * \param[in]  bytes
 *   max bytes to be written as radix number
 * \param[in]  rnsb
 *   RNS base b representation
 */
#define cpu_rad_from_rnsb(rad, bytes, rnsb) cpu_rad_from_rns_mrs((rad), \
  (bytes), (rnsb), rns_base_b, mrs_Pbj_1bi, mrs_Pbj_bi, mrs_Pbji)


/**
 * \brief unsigned bignum multiplication
 *
 * Multiplies a bignum number op1 with ui and stores to res.
 *
 * \param[out] res
 *   result
 * \papar[in]  op1
 *   multiplicand
 * \param[in]  ui
 *   multiplier
 * \return
 *   0 if result fits into res; non-zero else
 */
static
unsigned int rad_umul_ui(unsigned char res[BASEBYTES],
                         unsigned char op1[BASEBYTES], unsigned int ui);


/**
 * \brief add two bignum numbers
 *
 * Performs unsigned addition of two bignum numbers.
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   first operand
 * \param[in]  op2
 *   second perand
 * \return
 *   0 if result fits into res; nonzero else
 */
static
unsigned int rad_uadd(unsigned char res[BASEBYTES],
                      unsigned char op1[BASEBYTES],
                      unsigned char op2[BASEBYTES]);


/**
 * \brief addition of RNS numbers
 *
 * Performs addition of two RNS numbers according to given base.
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   first operand
 * \param[in]  op2
 *   second operand
 * \param[in]  base_m
 *   RNS base
 */
static
void rns_add(moduli_t res[MODULINUM], moduli_t op1[MODULINUM],
             moduli_t op2[MODULINUM], const moduli_t base_m[MODULINUM]);
/**
 * \brief RNS base a addition
 *
 * Adds two RNS numbers in base a.
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   first operand
 * \param[in]  op2
 *   second operand
 */
#define rnsa_add(res, op1, op2) rns_add((res), (op1), (op2), rns_base_a)
/**
 * \brief RNS base b addition
 *
 * Adds two RNS numbers in base b.
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   first operand
 * \param[in]  op2
 *   second operand
 */
#define rnsb_add(res, op1, op2) rns_add((res), (op1), (op2), rns_base_b)


/**
 * \brief multiplication of RNS numbers
 *
 * Performs multiplication of two RNS numbers according to given base.
 *
 * \param[out] res
 *   result
 * \param[in]  op1
 *   first operand
 * \param[in]  op2
 *   second operand
 * \param[in]  base_m
 *   RNS base
 */
static
void rns_mul(moduli_t res[MODULINUM], moduli_t op1[MODULINUM],
             moduli_t op2[MODULINUM], const moduli_t base_m[MODULINUM]);
/**
 * \brief RNS base a multiplication
 *
 * Multiplies two RNS numbers in base a.
 *
 * \param[out] res result
 * \param[in]  op1 first operand
 * \param[in]  op2 second operand
 */
#define rnsa_mul(res, op1, op2) rns_mul((res), (op1), (op2), rns_base_a)
/**
 * \brief RNS base b multiplication
 *
 * Multiplies two RNS numbers in base b.
 *
 * \param[out] res result
 * \param[in]  op1 first operand
 * \param[in]  op2 second operand
 */
#define rnsb_mul(res, op1, op2) rns_mul((res), (op1), (op2), rns_base_b)


/**
 * \brief base extension algorithm
 *
 * This base extension algorithm is based on the Kawamura's base extension
 * algorithm combined with redundant modulus extension.
 * 
 * \param[out] rns_t
 *   target RNS number
 * \param[in]  base_t
 *   target RNS base
 * \param[in]  rns_f
 *   source RNS number
 * \param[in]  base_f
 *   source RNS base
 * \param[in]  F_1rm
 *   multiplicative inverse of source base dynamic range
 *   modulo redundant modulus
 * \param[in]  Fi_1fi
 *   multiplicative inverse of source base moduli product except fi modulo fi
 * \param[in]  Fi_rnst
 *   source base moduli products except fi target base RNS representation
 * \param[in]  F_rm
 *   source base moduli product modulo redundant modulus
 * \param[in]  Fi_rm
 *   source base moduli products except fi modulo redundant modulus
 * \param[in]  minusF_rnst
 *   negative source base moduli product represented in target base
 *
 * \note
 * This algorithm uses error estimation, which can in some cases produce
 * incorrect results.
 */
static
void cpu_base_extension_krm(moduli_t rns_t[MODULINUM],
                            const moduli_t base_t[MODULINUM],
                            moduli_t rns_f[MODULINUM],
                            const moduli_t base_f[MODULINUM],
                            moduli_t F_1rm,
                            moduli_t Fi_1fi[MODULINUM],
                            moduli_t Fi_rnst[MODULINUM][MODULINUM],
                            moduli_t F_rm,
                            moduli_t Fi_rm[MODULINUM],
                            moduli_t minusF_rnst[MODULINUM]);


/**
 * \brief base extension algorithm
 *
 * This base extension algorithm is bases on the MRS conversion.
 *
 * \param[out] rns_t
 *   target RNS number
 * \param[in]  base_t
 *   target RNS base
 * \param[in]  rns_f
 *   source RNS number
 * \param[in]  base_f
 *   source RNS base
 * \param[in]  mrs_Pfj_1fi
 * \param[in]  mrs_Pfj_fi
 * \param[in]  mrs_Pfji_ti
 */
static
void cpu_base_extension_mrs(moduli_t rns_t[MODULINUM],
                            const moduli_t base_t[MODULINUM],
                            moduli_t rns_f[MODULINUM],
                            const moduli_t base_f[MODULINUM],
                            moduli_t mrs_Pfj_1fi[MODULINUM],
                            moduli_t mrs_Pfj_fi[MODULINUM][MODULINUM],
                            moduli_t mrs_Pfji_ti[MODULINUM][MODULINUM]);


/**
 * \brief base extension
 *
 * Computes the RNS base a representation from a number in RNS base b.
 *
 * \param[out] rnsa
 *   RNS base a number
 * \param[in]  rnsb
 *   RNS base b number
 *
 * \note
 * The algorithm uses a less precise but faster base extension algorithm.
 */
#define rnsa_from_rnsb_approx(rnsa, rnsb) cpu_base_extension_krm((rnsa), \
  rns_base_a, (rnsb), rns_base_b, B_1rm, Bi_1bi, Bi_rnsa, B_rm, Bi_rm, \
  minusB_rnsa)
/**
 * \brief base extension
 *
 * Computes the RNS base a representation from a number in RNS base b.
 *
 * \param[out] rnsa
 *   RNS base a number
 * \param[in]  rnsb
 *   RNS base b number
 *
 * \note
 * The algorithm uses the mixed radix number representation. The result
 * is precise but the computation is slow.
 */
#define rnsa_from_rnsb_exact(rnsa, rnsb) cpu_base_extension_mrs((rnsa), \
  rns_base_a, (rnsb), rns_base_b, mrs_Pbj_1bi, mrs_Pbj_bi, mrs_Pbji_ai)
/**
 * \brief base extension
 *
 * Computes the RNS base b representation from a number in RNS base a.
 *
 * \param[out] rnsb
 *   RNS base b number
 * \param[in]  rnsa
 *   RNS base a number
 *
 * \note
 * The algorithm uses a less precise but faster base extension algorithm.
 */
#define rnsb_from_rnsa_approx(rnsb, rnsa) cpu_base_extension_krm((rnsb), \
  rns_base_b, (rnsa), rns_base_a, A_1rm, Ai_1ai, Ai_rnsb, A_rm, Ai_rm, \
  minusA_rnsb)
/**
 * \brief base extension
 *
 * Computes the RNS base b representation from a number if RNS base a.
 *
 * \param[out] rnsb
 *   RNS base b number
 * \param[in]  rnsa
 *   RNS base a number
 *
 * \note
 * The algorithm uses the mixed radix number representation. The result
 * is precise but the computation is slow.
 */
#define rnsb_from_rnsa_exact(rnsb, rnsa) cpu_base_extension_mrs((rnsb), \
  rns_base_b, (rnsa), rns_base_a, mrs_Paj_1ai, mrs_Paj_ai, mrs_Paji_bi)


/**
 * \brief Montgomery multiplication algorithm
 *
 * Montgomery multiplication algorithm.
 * Computes w = x*y*B^{-1} mod N.
 *
 * \param[out] w_rnsab
 *   result represented in both RNS bases
 * \param[in]  x_rnsab
 *   first operand represented in both RNS bases
 * \param[in]  y_rnsab
 *   second operand represented in both RNS bases
 *
 * \note
 * Uses base extension with parameters approximation.
 */
static
void cpu_rnsab_mmul_approx(moduli_t w_rnsab[MODULINUM*2],
                           moduli_t x_rnsab[MODULINUM*2],
                           moduli_t y_rnsab[MODULINUM*2]);


/**
 * \brief Montgomery multiplication algorithm
 *
 * Montgomery multiplication algorithm.
 * Computes w = x*y*B^{-1} mod N.
 *
 * \param[out] w_rnsab
 *   result represented in both RNS bases
 * \param[in]  x_rnsab
 *   first operand represented in both RNS bases
 * \param[in]  y_rnsab
 *   second operand represented in both RNS bases
 *
 * \note
 * Uses exact but slow base extension algorithm.
 */
static
void cpu_rnsab_mmul_exact(moduli_t w_rnsab[MODULINUM*2],
                          moduli_t x_rnsab[MODULINUM*2],
                          moduli_t y_rnsab[MODULINUM*2]);


/**
 * \brief Montgomery modular exponentiation
 *
 * GPU implementation of the Montgomery modular exponentiation algorithm.
 * Computes y = x**(e_rad) * B**(-(e_rad-1)) mod n.
 *
 * \param[out] y_rnsab
 *   two RNS bases result
 * \param[in]  x_rnsab
 *   two RNS bases input
 */
static
void cpu_rnsab_mexp(moduli_t y_rnsab[MODULINUM*2],
                    moduli_t x_rnsab[MODULINUM*2],
                    unsigned char *e_rad, size_t ebytes);


/*=======================================================================*/
/* convert radix->RNS according to a given conversion matrix */
/* this implementation is designed to be run on CPU */
void cpu_rns_from_rad_ww(moduli_t rnsm[MODULINUM],
                         unsigned char *rad, size_t bytes,
                         const moduli_t base_m[MODULINUM],
                         moduli_t cpu_ww_radtornsm[MODULINUM][BASE_CPU_WORDS])
/*=======================================================================*/
{

  unsigned int modulii, cpu_wordii;
  unsigned int max_word;
  unsigned long modulos;

  cpu_unum_t * cpu_rad = (cpu_unum_t*)rad;
  max_word = bytes / sizeof(cpu_unum_t);

  for (modulii = 0; modulii < MODULINUM; modulii++) /* for all modulis */
  {
    modulos = 0;
    for (cpu_wordii = 0; cpu_wordii < max_word; cpu_wordii++)
    {
      /* modulos = (cpu_rad[cpu_wordii] * cpu_ww_radtornsm[modulii][cpu_wordii]
                    + modulos) % base_m[modulii]; */
      modmac(modulos, cpu_rad[cpu_wordii],
             cpu_ww_radtornsm[modulii][cpu_wordii], base_m[modulii]);
    }

    rnsm[modulii] = modulos;
  }

}


/*=======================================================================*/
/* converts RNS->radix, MRS conversion */
/* uses moduli_t numbers */
void cpu_rad_from_rns_mrs(unsigned char *rad, size_t maxbytes,
                          moduli_t rnsm[MODULINUM],
                          const moduli_t basem[MODULINUM],
                          moduli_t mrs_Pmj_1mi[MODULINUM],
                          moduli_t mrs_Pmj_mi[MODULINUM][MODULINUM],
                          unsigned char mrs_Pmji[MODULINUM][BASEBYTES])
/*=======================================================================*/
{

  signed int i,j;

  moduli_t mrsx[MODULINUM]; /* mixed radix representation of x*/
  unsigned long aux1;

  unsigned char auxrad1[BASEBYTES], auxrad2[BASEBYTES];

  unsigned int is_larger;

  mrsx[0] = rnsm[0];

  for (i = 1; i < MODULINUM; i++)
  {
    aux1 = 0;
    for (j = 0; j < i; j++)
    {
      modmac(aux1, mrsx[j], mrs_Pmj_mi[i][j], basem[i]);
    }

    aux1 = basem[i] - aux1; // -aux1
    modadd(aux1, rnsm[i], aux1, basem[i]);

    modmul(aux1, mrs_Pmj_1mi[i], aux1, basem[i]);

    mrsx[i] = aux1;
  }

  is_larger = rad_umul_ui(auxrad2, mrs_Pmji[0], mrsx[0]);
  for (i = 1; i < MODULINUM; i++)
  {
    is_larger = is_larger || rad_umul_ui(auxrad1, mrs_Pmji[i], mrsx[i]);
    is_larger = is_larger || rad_uadd(auxrad2, auxrad2, auxrad1);
  }

#if defined(DEBUG)
  if (is_larger)
  {
    fprintf(stderr, "%s: error converting from MRS to RAD\n", __func__);
    exit(1);
  }

  if (maxbytes < BASEBYTES)
  {
    is_larger = auxrad2[BASEBYTES-1];
    for (i = BASEBYTES-2; i >= (maxbytes); i--)
    {
      is_larger = is_larger || auxrad2[i];
    }
    if (is_larger)
    {
      fprintf(stderr, "%s: error converting from MRS to RAD\n", __func__);
      exit(1);
    }
  }
#endif

  memcpy(rad, auxrad2, maxbytes);

}


/*=======================================================================*/
/* mutiply radix representation by an integer
   returns 0 if the result fits in res */
unsigned int rad_umul_ui(unsigned char res[BASEBYTES],
                         unsigned char op1[BASEBYTES], unsigned int ui)
/*=======================================================================*/
{

  unsigned int i;
  unsigned int transfer, ci, co;

  transfer = 0; ci = 0;
  for (i = 0; i < (BASEBYTES/sizeof(moduli_t)); i++)
  {
    co = 0;
    asm("mov %3, %%eax; mul %4; add %2, %%eax; adc $0, %1; mov %%edx, %2; \
         add %5, %%eax; adc $0, %1; mov %%eax, %0;"
        : "=r"(((unsigned int*)res)[i]),
          "+r"(co),
          "+r"(transfer)
        : "r"(((unsigned int*)op1)[i]),
          "r"(ui),
          "r"(ci)
        : "%eax", "%edx");
    ci = co;
  }

  return (co || transfer);

}


/*=======================================================================*/
/* add two radix represention numbers
   returns 0 if the result fits in res */
unsigned int rad_uadd(unsigned char res[BASEBYTES],
                      unsigned char op1[BASEBYTES],
                      unsigned char op2[BASEBYTES])
/*=======================================================================*/
{

  unsigned int i;
  unsigned int ci, co;

  ci = 0;
  for (i = 0; i < (BASEBYTES/sizeof(moduli_t)); i++)
  {
    co = 0;

    asm("mov %2, %%eax; add %3, %%eax; adc $0, %1; add %4, %%eax; adc $0, %1; \
         mov %%eax, %0;"
        : "=r"(((unsigned int*)res)[i]),
          "+r"(co)
        : "r"(((unsigned int*)op1)[i]),
          "r"(((unsigned int*)op2)[i]),
          "r"(ci)
        : "%eax");

    ci = co;
  }

  return co;

}


/*=======================================================================*/
/* RNS addition using basem
   res = op1 + op2 */
void rns_add(moduli_t res[MODULINUM], moduli_t op1[MODULINUM],
             moduli_t op2[MODULINUM], const moduli_t base_m[MODULINUM])
/*=======================================================================*/
{

  unsigned int modulii;
  unsigned long result;

  for (modulii = 0; modulii < MODULINUM; modulii++)
  {
    modadd(result, op1[modulii], op2[modulii], base_m[modulii]);
    res[modulii] = result;
  }

}


/*=======================================================================*/
/* RNS multiplication using basem
   res = op1 * op2 */
void rns_mul(moduli_t res[MODULINUM], moduli_t op1[MODULINUM],
             moduli_t op2[MODULINUM], const moduli_t base_m[MODULINUM])
/*=======================================================================*/
{

  unsigned int modulii;
  unsigned long result;

  for (modulii = 0; modulii < MODULINUM; modulii++)
  {
    modmul(result, op1[modulii], op2[modulii], base_m[modulii]);
    res[modulii] = result;
  }

}


/*=======================================================================*/
/* base transformation algorithm
   converts x_f in base_f onto x_t in base_t*/
void cpu_base_extension_krm(moduli_t rns_t[MODULINUM],
                            const moduli_t base_t[MODULINUM],
                            moduli_t rns_f[MODULINUM],
                            const moduli_t base_f[MODULINUM],
                            moduli_t F_1rm,
                            moduli_t Fi_1fi[MODULINUM],
                            moduli_t Fi_rnst[MODULINUM][MODULINUM],
                            moduli_t F_rm,
                            moduli_t Fi_rm[MODULINUM],
                            moduli_t minusF_rnst[MODULINUM])
/*=======================================================================*/
{

  unsigned int modulii, modulij;
  unsigned long ksi[MODULINUM];
  unsigned long y, aux;
  unsigned long q;       // moduli_t
  unsigned long x_tilda; // moduli_t

  unsigned long redundant, sigma, k;

  for (modulii = 0; modulii < MODULINUM; modulii++)
  {
    modmul(ksi[modulii], rns_f[modulii], Fi_1fi[modulii], base_f[modulii]);
  }

  x_tilda = 0;
  sigma = ALPHA0 >> 1;
  redundant = 0;
  for (modulii = 0; modulii < MODULINUM; modulii++)
  {
    maskmul(aux, ksi[modulii], Fi_rm[modulii], REDUNDANTMASK);
    maskadd(x_tilda, x_tilda, aux, REDUNDANTMASK);

    /* compute reduntant moduli */
    sigma += ksi[modulii] >> 1;
    k = sigma & ((unsigned long)1 << (sizeof(moduli_t)*8 - 1));
    sigma = sigma & ~((unsigned long)1 << (sizeof(moduli_t)*8 - 1));

    /* if this generates a jump code, then it should be rewritten */
    k = k ? 1 : 0;

    maskmul(aux, ksi[modulii], Fi_rm[modulii], REDUNDANTMASK);
    maskadd(redundant, redundant, aux, REDUNDANTMASK);
    maskmul(aux, k, F_rm, REDUNDANTMASK);
    maskmul(aux, aux, REDUNDANTMASK, REDUNDANTMASK); /* -1*aux */
    maskadd(redundant, redundant, aux, REDUNDANTMASK);
  }

  /* quotient computed from redundant moduli */
  maskmul(q, F_1rm, (x_tilda - redundant), REDUNDANTMASK);

  for (modulii = 0; modulii < MODULINUM; modulii++) 
  {
    y = 0;
    for (modulij = 0; modulij < MODULINUM; modulij++)
    {
      /* y = (y + ksi[modulij]*Fi_rnst[modulij][modulii]) % base_t[modulii]; */
      modmul(aux, ksi[modulij], Fi_rnst[modulij][modulii], base_t[modulii]);
      modadd(y, y, aux, base_t[modulii]);
    }

    /* rns_t[modulii] = (y + q*minusF_rnst[modulii]) % base_t[modulii]; */
    modmul(aux, q, minusF_rnst[modulii], base_t[modulii]);
    modadd(aux, y, aux, base_t[modulii]);
    rns_t[modulii] = aux;
  }

}


/*=======================================================================*/
/* base extension algorithm */
void cpu_base_extension_mrs(moduli_t rns_t[MODULINUM],
                            const moduli_t base_t[MODULINUM],
                            moduli_t rns_f[MODULINUM],
                            const moduli_t base_f[MODULINUM],
                            moduli_t mrs_Pfj_1fi[MODULINUM],
                            moduli_t mrs_Pfj_fi[MODULINUM][MODULINUM],
                            moduli_t mrs_Pfji_ti[MODULINUM][MODULINUM])
/*=======================================================================*/
{

  signed int i,j;

  moduli_t mrs_f[MODULINUM]; /* mixed radix representation of rns_f */
  unsigned long aux;

  mrs_f[0] = rns_f[0];

  for (i = 1; i < MODULINUM; i++)
  {
    aux = 0;
    for (j = 0; j < i; j++)
    {
      modmac(aux, mrs_f[j], mrs_Pfj_fi[i][j], base_f[i]);
    }

    aux = base_f[i] - aux; // -aux
    modadd(aux, rns_f[i], aux, base_f[i]);

    modmul(aux, mrs_Pfj_1fi[i], aux, base_f[i]);

    mrs_f[i] = aux;
  }

  for (i = 0; i < MODULINUM; i++)
  {
    aux = 0;
    for(j = 0; j < MODULINUM; j++)
    {
      modmac(aux, mrs_f[j], mrs_Pfji_ti[i][j], base_t[i]);
    }
    rns_t[i] = aux;
  }
  
}


/*=======================================================================*/
/* RNS Montgomery multiplication
   w = x*y*B^{-1} mod N */
void cpu_rnsab_mmul_approx(moduli_t w_rnsab[MODULINUM*2],
                           moduli_t x_rnsab[MODULINUM*2],
                           moduli_t y_rnsab[MODULINUM*2])
/*=======================================================================*/
{

  moduli_t s_rnsab[MODULINUM*2];
  moduli_t t_rnsab[MODULINUM*2];
  moduli_t u_rnsa[MODULINUM];
  moduli_t v_rnsa[MODULINUM];

  /* 1: s = x*y */
  rnsa_mul(&s_rnsab[0], &x_rnsab[0], &y_rnsab[0]);
  rnsb_mul(&s_rnsab[MODULINUM], &x_rnsab[MODULINUM], &y_rnsab[MODULINUM]);

  /* 2: */
  rnsb_mul(&t_rnsab[MODULINUM], &s_rnsab[MODULINUM], n_neg1B_rnsb);
 
  /* 3: */
  rnsa_from_rnsb_approx(&t_rnsab[0], &t_rnsab[MODULINUM]);

  /* 4: */
  rnsa_mul(u_rnsa, &t_rnsab[0], &n_rnsab[0]);
  
  /* 5: */
  rnsa_add(v_rnsa, &s_rnsab[0], u_rnsa);
  
  /* 6: */
  rnsa_mul(&w_rnsab[0], v_rnsa, B_1A_rnsa);

  /* 7: */
  rnsb_from_rnsa_approx(&w_rnsab[MODULINUM], &w_rnsab[0]);

}


/*=======================================================================*/
/* RNS Montgomery multiplication
   w = x*y*B^{-1} mod N */
void cpu_rnsab_mmul_exact(moduli_t w_rnsab[MODULINUM*2],
                          moduli_t x_rnsab[MODULINUM*2],
                          moduli_t y_rnsab[MODULINUM*2])
/*=======================================================================*/
{

  moduli_t s_rnsab[MODULINUM*2];
  moduli_t t_rnsab[MODULINUM*2];
  moduli_t u_rnsa[MODULINUM];
  moduli_t v_rnsa[MODULINUM];

  /* 1: s = x*y */
  rnsa_mul(&s_rnsab[0], &x_rnsab[0], &y_rnsab[0]);
  rnsb_mul(&s_rnsab[MODULINUM], &x_rnsab[MODULINUM], &y_rnsab[MODULINUM]);

  /* 2: */
  rnsb_mul(&t_rnsab[MODULINUM], &s_rnsab[MODULINUM], n_neg1B_rnsb);
 
  /* 3: */
  rnsa_from_rnsb_exact(&t_rnsab[0], &t_rnsab[MODULINUM]);

  /* 4: */
  rnsa_mul(u_rnsa, &t_rnsab[0], &n_rnsab[0]);
  
  /* 5: */
  rnsa_add(v_rnsa, &s_rnsab[0], u_rnsa);
  
  /* 6: */
  rnsa_mul(&w_rnsab[0], v_rnsa, B_1A_rnsa);

  /* 7: */
  rnsb_from_rnsa_exact(&w_rnsab[MODULINUM], &w_rnsab[0]);

}


/*=======================================================================*/
/* RNS Montgomery modular exponentiation
   y = x^{d} * B^{-(d-1)} mod N */
void cpu_rnsab_mexp(moduli_t y_rnsab[MODULINUM*2],
                    moduli_t x_rnsab[MODULINUM*2],
                    unsigned char *e_rad, size_t ebytes)
/*=======================================================================*/
{

#define bit(x, idx) (((x)[(idx)>>3] >> ((idx)&0x07)) & 0x01)

  unsigned int i;

  i = (ebytes*8)-1;

  /* find first non-zero bit starting from MSB */
  while (bit(e_rad, i) == 0) i--;

  memcpy(y_rnsab, x_rnsab, sizeof(moduli_t)*MODULINUM*2);

  i--;

  for (; i+1 >= 1; i--)
  {
    cpu_rnsab_mmul_approx(y_rnsab, y_rnsab, y_rnsab);

    if (bit(e_rad, i))
    {
      cpu_rnsab_mmul_approx(y_rnsab, y_rnsab, x_rnsab);
    }
  }

}


/*=======================================================================*/
/* modular exponentiation
   m = C^d mod N */
void cpu_rsa(unsigned char m[MESSBYTES],
             unsigned char C[MESSBYTES],
             unsigned char *d, size_t dbytes)
/*=======================================================================*/
{

  /* initial precomputation of cipher constants has been done in the
     initialisation */

  moduli_t C_rnsab[MODULINUM*2];
  moduli_t Cl_rnsab[MODULINUM*2];

  moduli_t ml_rnsab[MODULINUM*2];
  moduli_t m_rnsab[MODULINUM*2];

  cpu_rnsa_from_rad(&C_rnsab[0], C, MESSBYTES);
  cpu_rnsb_from_rad(&C_rnsab[MODULINUM], C, MESSBYTES);

  /* 5: */
  cpu_rnsab_mmul_approx(Cl_rnsab, C_rnsab, B2_n_rnsab);

  /* 6 */
  cpu_rnsab_mexp(ml_rnsab, Cl_rnsab, d, dbytes);

  /* 7 */
  cpu_rnsab_mmul_exact(m_rnsab, ml_rnsab, ones_rnsab);

  cpu_rad_from_rnsa(m, MESSBYTES, &m_rnsab[0]);

}
