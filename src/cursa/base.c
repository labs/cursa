/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "cursa/base.h"
#include "cpu_asm.h"
#include "cursa/bignum.h"


/* arrays holding 32-bit RNS bases (all primes) */
const moduli32_t rns_basea1024_32[MODULINUM1024_32] =
  {4294967291U, 4294967231U, 4294967189U, 4294967143U, 4294967087U,
   4294966997U, 4294966943U, 4294966909U, 4294966829U, 4294966769U,
   4294966661U, 4294966651U, 4294966619U, 4294966583U, 4294966477U,
   4294966441U, 4294966373U, 4294966337U, 4294966243U, 4294966231U,
   4294966187U, 4294966163U, 4294966129U, 4294966099U, 4294966073U,
   4294966007U, 4294965977U, 4294965967U, 4294965937U, 4294965887U,
   4294965841U, 4294965821U, 4294965767U, 4294965737U},
                 rns_baseb1024_32[MODULINUM1024_32] =
  {4294967279U, 4294967197U, 4294967161U, 4294967111U, 4294967029U,
   4294966981U, 4294966927U, 4294966877U, 4294966813U, 4294966667U,
   4294966657U, 4294966639U, 4294966591U, 4294966553U, 4294966447U,
   4294966427U, 4294966367U, 4294966297U, 4294966237U, 4294966217U,
   4294966177U, 4294966153U, 4294966121U, 4294966087U, 4294966043U,
   4294966001U, 4294965971U, 4294965949U, 4294965911U, 4294965847U,
   4294965839U, 4294965793U, 4294965757U, 4294965733U};


const moduli32_t rns_basea2048_32[MODULINUM2048_32] =
  {4294967291U, 4294967231U, 4294967189U, 4294967143U, 4294967087U,
   4294966997U, 4294966943U, 4294966909U, 4294966829U, 4294966769U,
   4294966661U, 4294966651U, 4294966619U, 4294966583U, 4294966477U,
   4294966441U, 4294966373U, 4294966337U, 4294966243U, 4294966231U,
   4294966187U, 4294966163U, 4294966129U, 4294966099U, 4294966073U,
   4294966007U, 4294965977U, 4294965967U, 4294965937U, 4294965887U,
   4294965841U, 4294965821U, 4294965767U, 4294965737U, 4294965721U,
   4294965683U, 4294965673U, 4294965659U, 4294965617U, 4294965601U,
   4294965529U, 4294965461U, 4294965413U, 4294965361U, 4294965331U,
   4294965307U, 4294965251U, 4294965203U, 4294965161U, 4294965137U,
   4294965019U, 4294964977U, 4294964959U, 4294964929U, 4294964903U,
   4294964897U, 4294964887U, 4294964833U, 4294964809U, 4294964771U,
   4294964689U, 4294964633U, 4294964599U, 4294964561U, 4294964537U,
   4294964491U, 4294964461U},
                 rns_baseb2048_32[MODULINUM2048_32] =
  {4294967279U, 4294967197U, 4294967161U, 4294967111U, 4294967029U,
   4294966981U, 4294966927U, 4294966877U, 4294966813U, 4294966667U,
   4294966657U, 4294966639U, 4294966591U, 4294966553U, 4294966447U,
   4294966427U, 4294966367U, 4294966297U, 4294966237U, 4294966217U,
   4294966177U, 4294966153U, 4294966121U, 4294966087U, 4294966043U,
   4294966001U, 4294965971U, 4294965949U, 4294965911U, 4294965847U,
   4294965839U, 4294965793U, 4294965757U, 4294965733U, 4294965691U,
   4294965679U, 4294965671U, 4294965641U, 4294965613U, 4294965581U,
   4294965487U, 4294965457U, 4294965383U, 4294965347U, 4294965313U,
   4294965263U, 4294965229U, 4294965193U, 4294965151U, 4294965131U,
   4294964981U, 4294964969U, 4294964939U, 4294964923U, 4294964899U,
   4294964893U, 4294964879U, 4294964827U, 4294964789U, 4294964749U,
   4294964683U, 4294964621U, 4294964579U, 4294964543U, 4294964521U,
   4294964489U, 4294964441U};


const moduli32_t rns_basea4096_32[MODULINUM4096_32] =
  {4294967291U, 4294967231U, 4294967189U, 4294967143U, 4294967087U,
   4294966997U, 4294966943U, 4294966909U, 4294966829U, 4294966769U,
   4294966661U, 4294966651U, 4294966619U, 4294966583U, 4294966477U,
   4294966441U, 4294966373U, 4294966337U, 4294966243U, 4294966231U,
   4294966187U, 4294966163U, 4294966129U, 4294966099U, 4294966073U,
   4294966007U, 4294965977U, 4294965967U, 4294965937U, 4294965887U,
   4294965841U, 4294965821U, 4294965767U, 4294965737U, 4294965721U,
   4294965683U, 4294965673U, 4294965659U, 4294965617U, 4294965601U,
   4294965529U, 4294965461U, 4294965413U, 4294965361U, 4294965331U,
   4294965307U, 4294965251U, 4294965203U, 4294965161U, 4294965137U,
   4294965019U, 4294964977U, 4294964959U, 4294964929U, 4294964903U,
   4294964897U, 4294964887U, 4294964833U, 4294964809U, 4294964771U,
   4294964689U, 4294964633U, 4294964599U, 4294964561U, 4294964537U,
   4294964491U, 4294964461U, 4294964437U, 4294964381U, 4294964327U,
   4294964287U, 4294964263U, 4294964249U, 4294964221U, 4294964209U,
   4294964203U, 4294964159U, 4294964123U, 4294964039U, 4294964027U,
   4294963993U, 4294963957U, 4294963921U, 4294963891U, 4294963847U,
   4294963781U, 4294963723U, 4294963667U, 4294963639U, 4294963619U,
   4294963571U, 4294963537U, 4294963499U, 4294963429U, 4294963369U,
   4294963333U, 4294963291U, 4294963171U, 4294963111U, 4294963093U,
   4294963039U, 4294962929U, 4294962899U, 4294962853U, 4294962817U,
   4294962779U, 4294962751U, 4294962719U, 4294962691U, 4294962653U,
   4294962629U, 4294962589U, 4294962533U, 4294962473U, 4294962409U,
   4294962391U, 4294962377U, 4294962349U, 4294962313U, 4294962277U,
   4294962233U, 4294962211U, 4294962137U, 4294962047U, 4294961963U,
   4294961941U, 4294961921U, 4294961893U, 4294961863U, 4294961843U,
   4294961791U, 4294961767U, 4294961707U},
                 rns_baseb4096_32[MODULINUM4096_32] =
  {4294967279U, 4294967197U, 4294967161U, 4294967111U, 4294967029U,
   4294966981U, 4294966927U, 4294966877U, 4294966813U, 4294966667U,
   4294966657U, 4294966639U, 4294966591U, 4294966553U, 4294966447U,
   4294966427U, 4294966367U, 4294966297U, 4294966237U, 4294966217U,
   4294966177U, 4294966153U, 4294966121U, 4294966087U, 4294966043U,
   4294966001U, 4294965971U, 4294965949U, 4294965911U, 4294965847U,
   4294965839U, 4294965793U, 4294965757U, 4294965733U, 4294965691U,
   4294965679U, 4294965671U, 4294965641U, 4294965613U, 4294965581U,
   4294965487U, 4294965457U, 4294965383U, 4294965347U, 4294965313U,
   4294965263U, 4294965229U, 4294965193U, 4294965151U, 4294965131U,
   4294964981U, 4294964969U, 4294964939U, 4294964923U, 4294964899U,
   4294964893U, 4294964879U, 4294964827U, 4294964789U, 4294964749U,
   4294964683U, 4294964621U, 4294964579U, 4294964543U, 4294964521U,
   4294964489U, 4294964441U, 4294964419U, 4294964341U, 4294964309U,
   4294964281U, 4294964257U, 4294964239U, 4294964213U, 4294964207U,
   4294964173U, 4294964131U, 4294964081U, 4294964029U, 4294964017U,
   4294963987U, 4294963943U, 4294963901U, 4294963853U, 4294963787U,
   4294963747U, 4294963681U, 4294963643U, 4294963637U, 4294963583U,
   4294963553U, 4294963523U, 4294963459U, 4294963427U, 4294963349U,
   4294963313U, 4294963237U, 4294963117U, 4294963097U, 4294963051U,
   4294962953U, 4294962911U, 4294962887U, 4294962827U, 4294962809U,
   4294962757U, 4294962731U, 4294962703U, 4294962689U, 4294962641U,
   4294962619U, 4294962541U, 4294962499U, 4294962449U, 4294962401U,
   4294962389U, 4294962367U, 4294962341U, 4294962299U, 4294962271U,
   4294962223U, 4294962151U, 4294962079U, 4294962019U, 4294961959U,
   4294961927U, 4294961897U, 4294961873U, 4294961861U, 4294961809U,
   4294961777U, 4294961747U, 4294961663U};


/* The bases must be chosen so that the RNS dynamic ranges satisfy
   the following conditions:
   gcd(A,B) = 1, gcd(B, n) = 1;
   A > B;
   A,B > 8n;
   n - RSA modulo divider */


/* arrays holding the 32-bit complements of base moduli */
moduli32_t rns_basea1024_32comp[MODULINUM1024_32],
           rns_baseb1024_32comp[MODULINUM1024_32];

moduli32_t rns_basea2048_32comp[MODULINUM2048_32],
           rns_baseb2048_32comp[MODULINUM2048_32];

moduli32_t rns_basea4096_32comp[MODULINUM4096_32],
           rns_baseb4096_32comp[MODULINUM4096_32];


/* arrays used for word-wise radix->RNS conversion, computation in CPU */
moduli_t cpu_ww_radtornsa[MODULINUM][BASE_CPU_WORDS],
         cpu_ww_radtornsb[MODULINUM][BASE_CPU_WORDS];

/* radix->RNS word-wise conversion arrays, computation in GPU */
moduli_t gpu_ww_radtornsa[MODULINUM][BASE_GPU_WORDS],
         gpu_ww_radtornsb[MODULINUM][BASE_GPU_WORDS];

/* M = products of all corresponding moduli mi */
unsigned char A_rad[BASEBYTES],
              B_rad[BASEBYTES];
/* M_rm = product of all corresponding moduli mi modulo redundant modulus */
moduli_t A_rm,
         B_rm;
/* Mi = M mod mi */
unsigned char Ai_rad[MODULINUM][BASEBYTES],
              Bi_rad[MODULINUM][BASEBYTES];
/* Mi = M / mi modulo redundant modulus */
moduli_t Ai_rm[MODULINUM],
         Bi_rm[MODULINUM];
/* Mi_1mi = multiplicative inverse Mi modulo mi in base M */
moduli_t Ai_1ai[MODULINUM],
         Bi_1bi[MODULINUM];

moduli_t minusA_rnsb[MODULINUM],        /* -A in base b representation */
         minusB_rnsa[MODULINUM];        /* -B in base a representation */
moduli_t Ai_rnsb[MODULINUM][MODULINUM], /* Ais in base b representation */
         Bi_rnsa[MODULINUM][MODULINUM]; /* Bis in base a representation */

/* M_1rm = multiplicative inverse M modulo redundant modulus */
moduli_t A_1rm,
         B_1rm;

/* n - RSA modulo divider */
unsigned char n_rad[MESSBYTES];
/* B_n = B mod n */
unsigned char B_n_rad[MESSBYTES];
/* B2_n = B^2 mod n */
unsigned char B2_n_rad[MESSBYTES];
/* B_1A; B*B_1 mod A = 1 */
unsigned char B_1A_rad[BASEBYTES];
/* B_1n; B*B_1 mod n = 1 */
unsigned char B_1n_rad[MESSBYTES];
/* n_neg1B;
   B*B_1 - n*n_neg1B = 1;
   (n*n_1B = -1) mod B;
   n_neg1B = (n_1B*(B-1)) mod B */
unsigned char n_neg1B_rad[BASEBYTES];

/* n_neg1B in rns base b representation */
moduli_t n_neg1B_rnsb[MODULINUM];

/* B_1A_rnsa - base a representation of B**(-1) mod A */
moduli_t B_1A_rnsa[MODULINUM];

/* n_rnsab - two base RNS representation of n */
moduli_t n_rnsab[MODULINUM*2];
/* B_n_rnsab = two base RNS representation of B mod n */
moduli_t B_n_rnsab[MODULINUM*2];
/* B2_n_rnsab = two base RNS representation of B^2 mod n */
moduli_t B2_n_rnsab[MODULINUM*2];

/* ones_rnsab - 1 represented in two base RNS */
moduli_t ones_rnsab[MODULINUM*2];

/* multiplicative inverses of products of moduli with indexes
   less than i modulo moduli i */
moduli_t mrs_Paj_1ai[MODULINUM],
         mrs_Pbj_1bi[MODULINUM];
/* incremental products of moduli with indexes
   less than i modulo moduli i */
moduli_t mrs_Paj_ai[MODULINUM][MODULINUM],
         mrs_Pbj_bi[MODULINUM][MODULINUM];
/* incremental products of moduli with indexes less than i modulo
   moduli i in other base */
moduli_t mrs_Paji_bi[MODULINUM][MODULINUM],
         mrs_Pbji_ai[MODULINUM][MODULINUM];
/* incremental products of moduli with indexes less than i */
unsigned char mrs_Paji[MODULINUM][BASEBYTES],
              mrs_Pbji[MODULINUM][BASEBYTES];


/**
 * \brief initialize values needed for radix to RNS word-wise conversion
 *
 * Pre-computes values (depend on a single base) needed for word-wise
 * forward conversion.
 *
 * \param[out] cpu_ww_radtornsm
 *   pointer to CPU conversion array. This array is used for the CPU based
 *  conversion function.
 * \param[out] gpu_ww_radtornsm
 *   pointer to GPU conversion array. The values stored here are utilized
 *   in the GPU conversion function.
 * \param[in]  base_m
 *   RNS base
 *
 * \note
 * Depending on the configuration the GPU and CPU conversion arrays may contain
 * different values. The implementation may differ in the bit-width of
 * the intermediate values.
 */
static
void cpu_init_ww_rns_from_rad_sb(moduli_t cpu_ww_radtornsm[MODULINUM][BASE_CPU_WORDS],
                                 moduli_t gpu_ww_radtornsm[MODULINUM][BASE_GPU_WORDS],
                                 const moduli_t base_m[MODULINUM]);


/**
 * \brief initializes some values for RNS to RNS conversion
 *
 * Pre-computes single-base dependent values needed for base extension
 * (RNS to RNS conversion).
 *
 * \param[out] M_rad
 *   array holding radix (plain) representation of moduli product
 * \param[out] Mi_rad
 *   array holding radix representation of all moduli product except mi
 * \param[out] M_rm
 *   M modulo redundant modulus
 * \param[out] Mi_rm
 *   Mi values modulo redundant modulus
 * \param[out] Mi_1mi
 *   multiplicative inverse Mi modulo mi
 * \param[out] M_1rm
 *   multiplicative inverse M modulo redundant modulus
 * \param[in]  base_m
 *   RNS base
 */
static
void cpu_init_base_extension_krm_sb(unsigned char M_rad[BASEBYTES],
                                    unsigned char Mi_rad[MODULINUM][BASEBYTES],
                                    moduli_t *M_rm,
                                    moduli_t Mi_rm[MODULINUM],
                                    moduli_t Mi_1mi[MODULINUM],
                                    moduli_t *M_1rm,
                                    const moduli_t base_m[MODULINUM]);


/**
 * \brief initializes values for MRS based base extension algorithm
 *
 * Pre-computes conversion values needed for mixed radix system (MRS) based
 * base extension (RNS to RNS conversion).
 *
 * \param[out] mrs_Pfji_ti
 *   array holding values of mrs_Pfji modulo corresponding base moduli
 * \param[in]  mrs_Pfji
 * \param[in]  base_t
 *   RNS base
 */
static
void init_base_extension_mrs(moduli_t mrs_Pfji_ti[MODULINUM][MODULINUM],
                             unsigned char mrs_Pfji[MODULINUM][BASEBYTES],
                             const moduli_t base_t[MODULINUM]);


/**
 * \brief initializes RNS to radix via MRS conversion values
 *
 * Pre-computes single base dependent values needed for MRS reverse conversion.
 *
 * \param[out] mrs_Pmj_1mi
 *   multiplicative inverse of the product of moduli mj with index j less
 *   than i modulo mi
 * \param[out] mrs_Pmj_mi
 *   incremental products of moduli mj with indexes j less than i modulo mi
 * \param[out] mrs_Pmji
 *   products of moduli mj with indexes j less than i
 * \param[in]  base_m
 *   RNS base
 */
static
void cpu_init_mrs_rad_from_rns_sb(moduli_t mrs_Pmj_1mi[MODULINUM],
                                  moduli_t mrs_Pmj_mi[MODULINUM][MODULINUM],
                                  unsigned char mrs_Pmji[MODULINUM][BASEBYTES],
                                  const moduli_t base_m[MODULINUM]);


/**
 * \brief initialize key-dependent values
 *
 * Pre-computes key-dependent values needed for RNS Montgomery multiplication.
 *
 * \param[in] n
 *   RSA modulo divider
 */
static
void cpu_init_key_dependent(const unsigned char n[MESSBYTES]);


/**
 * \brief word-wise CPU radix to RNS conversion
 *
 * Converts a number in radix (plain) representation into RNS base m.
 *
 * \param[out] rnsm
 *   RNS base_m representation of rad
 * \param[in]  rad
 *   number to be converted
 * \param[in]  bytes
 *   number of bytes of the converted number
 * \param[in]  base_m
 *   selected base
 * \param[in]  cpu_ww_radtornsm
 *   precomputed conversion values
 */
static
void cpu_rns_from_rad_ww(moduli_t rnsm[MODULINUM],
                         unsigned char *rad, size_t bytes,
                         const moduli_t base_m[MODULINUM],
                         moduli_t cpu_ww_radtornsm[MODULINUM][BASE_CPU_WORDS]);
/**
 * \brief converts a radix number into RNS base a
 *
 * Converts input radix number into RNS representation with base a.
 *
 * \param[out] rnsa
 *   RNS base a representation
 * \param[in]  rad
 *   radix representation
 * \param[in]  bytes
 *   number of bytes of the radix number
 */
#define cpu_rnsa_from_rad(rnsa, rad, bytes) cpu_rns_from_rad_ww((rnsa), (rad),\
  (bytes), rns_base_a, cpu_ww_radtornsa)
/**
 * \brief converts a radix number into RNS base b
 *
 * Converts input radix number into RNS representation with base b.
 *
 * \param[out] rnsb
 *   RNS base b representation
 * \param[in]  rad
 *   radix representation
 * \param[in]  bytes
 *   number of bytes of the radix number
 */
#define cpu_rnsb_from_rad(rnsb, rad, bytes) cpu_rns_from_rad_ww((rnsb), (rad),\
  (bytes), rns_base_b, cpu_ww_radtornsb)


/**
 * \brief extended Euclidean algorithm for small numbers
 *
 * Extended Euclidean algorithm, computes the multiplicative inverse and 
 * negative multiplicative inverse of r and n.
 * r*r_1n - n*n_neg1r = gcd(r, n) = 1
 * (r*r_1n) mod n = 1
 * n*n_neg1r mod r = r-1; (r-1) mod r = (-1) mod r
 * n_neg1r = (n_1r*(r-1)) mod r
 * n_neg1r = -n_1r
 *
 * \param[in]  R
 * \param[in]  N
 * \param[out] R_1N
 *   multiplicative inverse of R modulo N
 * \param[out] N_neg1R
 *   negative multiplicative inverse of N modulo R
 * \return
 *   returns 1 if gcd(R, N) is equal to 1
 *
 * \note
 * May generate incorrect values due to register overflow.
 */
static
unsigned long egcdm_small(const unsigned long R, const unsigned long N,
                          unsigned long *R_1N, unsigned long *N_neg1R);


/**
 * \brief extended Euclidean algorithm
 *
 * Extended Euclidean algorithm, computes the multiplicative inverse and 
 * negative multiplicative inverse of r and n.
 * r*r_1n - n*n_neg1r = gcd(r, n) = 1
 * (R*R_1N) mod N = 1
 * N*N_neg1R mod R = R-1; (R-1) mod R = (-1) mod R
 * N_neg1R = (N_1R*(R-1)) mod R
 * N_neg1R = -N_1R
 *
 * \param[in]  R
 * \param[in]  N
 * \param[out] R_1N
 *   multiplicative inverse of R modulo N
 * \param[out] N_neg1R
 *   negative multiplicative inverse of N modulo R
 * \return
 *   returns 1 if gcd(R, N) is equal to 1
 *
 * \note
 * This function uses bignums to avoid integer overflow.
 */
static
unsigned long egcdm(const unsigned long R, const unsigned long N,
                    unsigned long *R_1N, unsigned long *N_neg1R);


/**
 * \brief extended Euclidean algorithm running with large numbers
 *
 * Extended Euclidean algorithm running on large numbers. Computes
 * the multiplicative inverse and negative multiplicative inverse of R and N.
 * R*R_1N - N*N_neg1R = gcd(R, N) = 1
 * (R*R_1N) mod N = 1
 * N*N_neg1R mod R = R-1; (R-1) mod R = (-1) mod R
 * N_neg1R = (N_1R*(R-1)) mod R
 * N_neg1R = -N_1R
 *
 * \param[in]  R
 * \param[in]  Rbytes
 *   number of bytes of the number R
 * \param[in]  N
 * \param[in]  Nbytes
 *   number of bytes of the number N
 * \param[out] R_1N
 *   multiplicative inverse of R modulo N
 * \param[out] N_neg1R
 *   negative multiplicative inverse of N modulo R
 * \return
 *   returns 1 if gcd(R, N) is equal to 1
 *
 * \note
 * R_1N is considered to have at least Nbytes bytes
 * N_neg1R is considered to have al least Rbytes bytes
 * The algorithm uses bignums.
 */
static
unsigned int egcdm_bignum(const unsigned char *R, const size_t Rbytes,
                          const unsigned char *N, const size_t Nbytes,
                          unsigned char *R_1N, unsigned char *N_neg1R);


/*=======================================================================*/
/* pre-computes all constants - including those key*dependent */
void cpu_init_constants(const rsa_key *key)
/*=======================================================================*/
{

  unsigned int modulii;


  /* compute moduli complements */
  for (modulii = 0; modulii < MODULINUM; modulii++)
  {
    rns_basea1024_32comp[modulii] = 0 - rns_basea1024_32[modulii];
    rns_baseb1024_32comp[modulii] = 0 - rns_baseb1024_32[modulii];
  }


  /* initialize forward conversion */
  cpu_init_ww_rns_from_rad_sb(cpu_ww_radtornsa, gpu_ww_radtornsa, rns_base_a);
  cpu_init_ww_rns_from_rad_sb(cpu_ww_radtornsb, gpu_ww_radtornsb, rns_base_b);


  /* initialization of base extension */
  cpu_init_base_extension_krm_sb(A_rad, Ai_rad, &A_rm, Ai_rm, Ai_1ai, &A_1rm,
                                 rns_base_a);
  cpu_init_base_extension_krm_sb(B_rad, Bi_rad, &B_rm, Bi_rm, Bi_1bi, &B_1rm,
                                 rns_base_b);

  for (modulii = 0; modulii < MODULINUM; modulii++)
  {
    cpu_rnsb_from_rad(Ai_rnsb[modulii], Ai_rad[modulii], BASEBYTES);
    cpu_rnsa_from_rad(Bi_rnsa[modulii], Bi_rad[modulii], BASEBYTES);
  }

  size_t bytes;
  unsigned char aux[BASEBYTES];
  bn_t gminus1, gA, gB;

  bn_init(gminus1); bn_init(gA); bn_init(gB);

  bn_import(gB, BASEBYTES, LSF, 1, NATIVE, 0, B_rad);
  bn_import(gA, BASEBYTES, LSF, 1, NATIVE, 0, A_rad);

  bn_sub_ui(gminus1, gB, 1);    /* B-1 */
  bn_mul(gminus1, gminus1, gA); /* (B-1)*A */
  bn_mod(gminus1, gminus1, gB); /* -A mod B */
  memset(aux, 0, BASEBYTES);
  bn_export(aux, &bytes, LSF, 1, NATIVE, 0, gminus1);
  if (BASEBYTES < bytes)
  {
    fprintf(stderr, "%s: -AmodB written %lu instead of %lu bytes\n",
            __func__, (unsigned long)bytes, (unsigned long)BASEBYTES);
    exit(1);
  }
  cpu_rnsb_from_rad(minusA_rnsb, aux, BASEBYTES);

  bn_sub_ui(gminus1, gA, 1);    /* A-1 */
  bn_mul(gminus1, gminus1, gB); /* (A-1)*B */
  bn_mod(gminus1, gminus1, gA); /* -B mod A */
  memset(aux, 0, BASEBYTES);
  bn_export(aux, &bytes, LSF, 1, NATIVE, 0, gminus1);
  if (BASEBYTES < bytes)
  {
    fprintf(stderr, "%s: -BmodA written %lu instead of %lu bytes\n",
            __func__, (unsigned long)bytes, (unsigned long)BASEBYTES);
    exit(1);
  }
  cpu_rnsa_from_rad(minusB_rnsa, aux, BASEBYTES);

  bn_clear(gminus1); bn_clear(gA); bn_clear(gB);


  /* initialize MRS reverse conversion */
  cpu_init_mrs_rad_from_rns_sb(mrs_Paj_1ai, mrs_Paj_ai, mrs_Paji, rns_base_a);
  cpu_init_mrs_rad_from_rns_sb(mrs_Pbj_1bi, mrs_Pbj_bi, mrs_Pbji, rns_base_b);

  init_base_extension_mrs(mrs_Paji_bi, mrs_Paji, rns_base_b);
  init_base_extension_mrs(mrs_Pbji_ai, mrs_Pbji, rns_base_a);

  /* set modulo divider and dependent values */
  cpu_init_key_dependent(key->modulus);


  /* 1 in both bases */
  for (modulii = 0; modulii < MODULINUM*2; modulii++)
  {
    ones_rnsab[modulii] = 1;
  }

}


/*=======================================================================*/
/* pre-compute values needed for word-wise forward conversion */
void cpu_init_ww_rns_from_rad_sb(moduli_t cpu_ww_radtornsm[MODULINUM][BASE_CPU_WORDS],
                                 moduli_t gpu_ww_radtornsm[MODULINUM][BASE_GPU_WORDS],
                                 const moduli_t base_m[MODULINUM])
/*=======================================================================*/
{

  unsigned int modulii, cpu_wordii, gpu_wordii;

  bn_t nominator, denominator, modulo;

  /* initialize and set to 0 */
  bn_init(nominator);
  bn_init(denominator);
  bn_init(modulo);

  /* for all moduli in a base */
  for (modulii = 0; modulii < MODULINUM; modulii++)
  {
    /* CPU conversion */
    cpu_ww_radtornsm[modulii][0] = 1;
    /* for all words */
    for (cpu_wordii = 1; cpu_wordii < BASE_CPU_WORDS; cpu_wordii++)
    {
      bn_ui_pow_ui(nominator, 2, cpu_wordii*sizeof(cpu_unum_t)*8);
      bn_mod_ui(modulo, nominator, base_m[modulii]);
      cpu_ww_radtornsm[modulii][cpu_wordii] = bn_get_ui(modulo);
    }

    /* GPU conversion */
    gpu_ww_radtornsm[modulii][0] = 1;
    /* for all words */
    for (gpu_wordii = 1; gpu_wordii < BASE_GPU_WORDS; gpu_wordii++)
    {
      bn_ui_pow_ui(nominator, 2, gpu_wordii*sizeof(moduli_t)*8);
      bn_mod_ui(modulo, nominator, base_m[modulii]);
      gpu_ww_radtornsm[modulii][gpu_wordii] = bn_get_ui(modulo);
    }

  }

  bn_clear(nominator);
  bn_clear(denominator);
  bn_clear(modulo);

}


/*=======================================================================*/
/* pre-compute values needed for base extension */
void cpu_init_base_extension_krm_sb(unsigned char M_rad[BASEBYTES],
                                    unsigned char Mi_rad[MODULINUM][BASEBYTES],
                                    moduli_t *M_rm,
                                    moduli_t Mi_rm[MODULINUM],
                                    moduli_t Mi_1mi[MODULINUM],
                                    moduli_t *M_1rm,
                                    const moduli_t base_m[MODULINUM])
/*=======================================================================*/
{

  unsigned int modulii;
  size_t bytes;
  unsigned long aux_inv, aux_cyc;

  bn_t X, Xi, Xi_mod;

  /* initialize and set to 0 */
  bn_init(X);
  bn_init(Xi);
  bn_init(Xi_mod);

  modulii = 0;
  bn_set_ui(X, base_m[modulii]);

  for (modulii = 1; modulii < MODULINUM; modulii++)
  {
    bn_mul_ui(X, X, base_m[modulii]);
  }
  memset(M_rad, 0, BASEBYTES);
  bn_export(M_rad, &bytes, LSF, 1, NATIVE, 0, X); /* M */
  if (BASEBYTES < bytes)
  {
    fprintf(stderr, "%s: M_rad written %lu instead of %lu bytes\n",
            __func__, (unsigned long)bytes, (unsigned long)BASEBYTES);
    exit(1);
  }

  for (modulii = 0; modulii < MODULINUM; modulii++)
  {
    bn_tdiv_q_ui(Xi, X, base_m[modulii]);
    memset(Mi_rad[modulii], 0, BASEBYTES);
    bn_export(Mi_rad[modulii], &bytes, LSF, 1, NATIVE, 0, Xi); /* Mi = M/mi */
    if (BASEBYTES < bytes)
    {
      fprintf(stderr, "%s: Mi_rad[%u] written %lu instead of %lu bytes\n",
              __func__, modulii, (unsigned long)bytes,
              (unsigned long)BASEBYTES);
      exit(1);
    }
    bn_mod_ui(Xi_mod, Xi, base_m[modulii]); /* Mi % mi */

    egcdm(bn_get_ui(Xi_mod), base_m[modulii], &aux_inv, &aux_cyc);
    Mi_1mi[modulii] = aux_inv; /* Mi_1mi = multiplicative inverse Mi mod mi */

  }

  bn_mod_ui(Xi_mod, X, 1<<REDUNDANTBITS); /* M % redundant modulus */
  egcdm(bn_get_ui(Xi_mod), 1<<REDUNDANTBITS, &aux_inv, NULL);
  *M_1rm = aux_inv;

  bn_clear(X);
  bn_clear(Xi);
  bn_clear(Xi_mod);

  *M_rm = ((moduli_t*)M_rad)[0] & REDUNDANTMASK;

  for (modulii = 0; modulii < MODULINUM; modulii++)
  {
    Mi_rm[modulii] = ((moduli_t*)(Mi_rad[modulii]))[0] & REDUNDANTMASK;
  }

}


/*=======================================================================*/
/* MRS based base extension algorithm */
void init_base_extension_mrs(moduli_t mrs_Pfji_ti[MODULINUM][MODULINUM],
                             unsigned char mrs_Pfji[MODULINUM][BASEBYTES],
                             const moduli_t base_t[MODULINUM])
/*=======================================================================*/
{

  unsigned int i, j;

  size_t bytes;

  bn_t gnum, gmodulo;

  bn_init(gnum);  bn_init(gmodulo);

  memset(mrs_Pfji_ti, 0, sizeof(moduli_t)*MODULINUM*MODULINUM);

  for (i = 0; i < MODULINUM; i++)
  {
    for (j = 0; j < MODULINUM; j++)
    {
      bn_import(gnum, BASEBYTES, LSF, 1, NATIVE, 0, mrs_Pfji[j]);
      bn_mod_ui(gmodulo, gnum, base_t[i]);
      bn_export(&mrs_Pfji_ti[i][j], &bytes, LSF, 1, NATIVE, 0, gmodulo);
      if (sizeof(moduli_t) < bytes)
      {
        fprintf(stderr, "%s: written %lu instead of %lu bytes\n",
                __func__, bytes, sizeof(moduli_t));
        exit(1);
      }
    }
  }

  bn_clear(gnum); bn_clear(gmodulo);

}


/*=======================================================================*/
/* pre-compute RNS -> radix constants for MRS conversion */
void cpu_init_mrs_rad_from_rns_sb(moduli_t mrs_Pmj_1mi[MODULINUM],
                                  moduli_t mrs_Pmj_mi[MODULINUM][MODULINUM],
                                  unsigned char mrs_Pmji[MODULINUM][BASEBYTES],
                                  const moduli_t base_m[MODULINUM])
/*=======================================================================*/
{

  unsigned int i, j;

  size_t bytes;
  bn_t gproduct, gmoduli, ginverse, gmodulo;

  bn_init(gproduct); bn_init(gmoduli); bn_init(ginverse); bn_init(gmodulo);

  /* Pmj_1mi */
  mrs_Pmj_1mi[0] = 1;

  for (i = 1; i < MODULINUM; i++)
  {

    bn_set_ui(gproduct, 1);

    for (j = 0; j < i; j++)
    {
      bn_mul_ui(gproduct, gproduct, base_m[j]);
    }

    bn_set_ui(gmoduli, base_m[i]);
    bn_invert(ginverse, gproduct, gmoduli);
    bn_export(&mrs_Pmj_1mi[i], &bytes, LSF, 1, NATIVE, 0, ginverse);
    if (sizeof(moduli_t) < bytes)
    {
      fprintf(stderr, "%s: mrs_Pmj_1mi[%u] written %lu instead of %lu bytes\n",
              __func__, i, (unsigned long)bytes, sizeof(moduli_t));
      exit(1);
    }

  }

  /* Pmj_mi */
  memset(mrs_Pmj_mi, 0, sizeof(moduli_t)*MODULINUM*MODULINUM);
  mrs_Pmj_mi[0][0] = 1;
  
  for (i = 1; i < MODULINUM; i++)
  {

    bn_set_ui(gproduct, 1);

    mrs_Pmj_mi[i][0] = 1;
    for (j = 0; j < i; j++)
    {
      bn_mul_ui(gproduct, gproduct, base_m[j]);
      bn_mod_ui(gmodulo, gproduct, base_m[i]);
      bn_export(&mrs_Pmj_mi[i][j+1], &bytes, LSF, 1, NATIVE, 0, gmodulo);
      if (sizeof(moduli_t) < bytes)
      {
        fprintf(stderr,
                "%s: mrs_Pmj_mi[%u][%u] written %lu instead of %lu bytes\n",
                __func__, i, j, (unsigned long)bytes, sizeof(moduli_t));
        exit(1);
      }
    }
    
  }

  /* Pmji */
  memset(mrs_Pmji, 0, MODULINUM*BASEBYTES);
  mrs_Pmji[0][0] = 1;

  for (i = 1; i < MODULINUM; i++)
  {

    bn_set_ui(gproduct, 1);

    for (j = 0; j < i; j++)
    {
      bn_mul_ui(gproduct, gproduct, base_m[j]);
    }

    bn_export(mrs_Pmji[i], &bytes, LSF, 1, NATIVE, 0, gproduct);
    if (BASEBYTES < bytes)
    {
      fprintf(stderr, "%s: mrs_Pmji[%u] written %lu instead of %lu bytes\n",
              __func__, i, (unsigned long)bytes, (unsigned long)BASEBYTES);
      exit(1);
    }

  }

  bn_clear(gproduct); bn_clear(gmoduli); bn_clear(ginverse); bn_clear(gmodulo);

}


/*=======================================================================*/
/* pre-compute key-dependent RNS RSA constants */
void cpu_init_key_dependent(const unsigned char n[MESSBYTES])
/*=======================================================================*/
{

  size_t bytes;
  bn_t nominator, denominator, modulo;

  /* set modulo divider */
  memcpy(n_rad, n, MESSBYTES);

  bn_init(nominator); bn_init(denominator); bn_init(modulo);

  bn_import(nominator, BASEBYTES, LSF, 1, NATIVE, 0, B_rad);
  bn_import(denominator, MESSBYTES, LSF, 1, NATIVE, 0, n_rad);

  bn_mod(modulo, nominator, denominator);
  memset(B_n_rad, 0, MESSBYTES);
  bn_export(B_n_rad, &bytes, LSF, 1, NATIVE, 0, modulo); /* B_n = B mod n */
  if (MESSBYTES < bytes)
  {
    fprintf(stderr, "%s: B_N_rad written %lu instead of %u bytes\n",
            __func__, (unsigned long)bytes, MESSBYTES);
    exit(1);
  }

  bn_mul(nominator, nominator, nominator); /* B**2 */
  bn_mod(modulo, nominator, denominator);
  memset(B2_n_rad, 0, MESSBYTES);
  bn_export(B2_n_rad, &bytes, LSF, 1, NATIVE, 0, modulo); /* B2_n = B**2 % n */
  if (MESSBYTES < bytes)
  {
    fprintf(stderr, "%s: B2_N_rad written %lu instead of %u bytes\n",
            __func__, (unsigned long)bytes, MESSBYTES);
    exit(1);
  }

  /* n_neg1B */
  egcdm_bignum(B_rad, BASEBYTES, n_rad, MESSBYTES, B_1n_rad, n_neg1B_rad);
  /* B_1A */
  egcdm_bignum(B_rad, BASEBYTES, A_rad, BASEBYTES, B_1A_rad, NULL);

  /* n_neg1B_rnsb = n_neg1B in base b */
  cpu_rnsb_from_rad(n_neg1B_rnsb, n_neg1B_rad, BASEBYTES);
  /* B_1A_rnsa = B_1 mod A in base a */
  cpu_rnsa_from_rad(B_1A_rnsa, B_1A_rad, BASEBYTES);

  cpu_rnsa_from_rad(&n_rnsab[0], n_rad, MESSBYTES);
  /* n_rnsab = n in base a and b */
  cpu_rnsb_from_rad(&n_rnsab[MODULINUM], n_rad, MESSBYTES);

  cpu_rnsa_from_rad(&B_n_rnsab[0], B_n_rad, MESSBYTES);
  /* B_n_rnsab = B mod n in base a and b */
  cpu_rnsb_from_rad(&B_n_rnsab[MODULINUM], B_n_rad, MESSBYTES);

  cpu_rnsa_from_rad(&B2_n_rnsab[0], B2_n_rad, MESSBYTES);
  /* B2_n_rnsab = (B**2) mod n in base a and b */
  cpu_rnsb_from_rad(&B2_n_rnsab[MODULINUM], B2_n_rad, MESSBYTES);

  bn_clear(nominator); bn_clear(denominator); bn_clear(modulo);

}


/*=======================================================================*/
/* convert radix->RNS according to a given conversion matrix */
/* this implementation is designed to be run on CPU */
void cpu_rns_from_rad_ww(moduli_t rnsm[MODULINUM],
                         unsigned char *rad, size_t bytes,
                         const moduli_t base_m[MODULINUM],
                         moduli_t cpu_ww_radtornsm[MODULINUM][BASE_CPU_WORDS])
/*=======================================================================*/
{

  unsigned int modulii, cpu_wordii;
  unsigned int max_word;
  unsigned long modulos;

  cpu_unum_t * cpu_rad = (cpu_unum_t*)rad;
  max_word = bytes / sizeof(cpu_unum_t);

  for (modulii = 0; modulii < MODULINUM; modulii++) /* for all modulis */
  {
    modulos = 0;
    for (cpu_wordii = 0; cpu_wordii < max_word; cpu_wordii++)
    {
      /* modulos = (cpu_rad[cpu_wordii] * cpu_ww_radtornsm[modulii][cpu_wordii]
                    + modulos) % base_m[modulii]; */
      modmac(modulos,
             cpu_rad[cpu_wordii], cpu_ww_radtornsm[modulii][cpu_wordii],
             base_m[modulii]);
    }

    rnsm[modulii] = modulos;
  }

}


/*=======================================================================*/
/* extended Euclidean gcd algorithm */
unsigned long egcdm_small(const unsigned long R, const unsigned long N,
                          unsigned long *R_1N, unsigned long *N_neg1R)
/*=======================================================================*/
{

  unsigned long x, y, u, v, m, n, a, b, q, r;

  /* N = R(0) + N(1) */
  x = 0; y = 1;
 
  /* R = R(1) + N(0) */
  u = 1; v = 0;
 
  for (a = R, b = N;
       0 != a;
       b = a, a = r, x = u, y = v, u = m, v = n)
  {
    /* b = a*q + r and 0 <= r < a */
    q = b / a;
    r = b % a;
 
    /* r = R*x + N*y - a*q
         = R*x + N*y - (R*u + N*v)q
         = R(x - u*q) + N(y - v*q) */
    m = x - (u * q);
    n = y - (v * q); 
  }   
 
  /* R*x + N*y = gcd(R, N) */

  x += N;
  x %= N;
  y = x + ((x * (R - N) - b) / N);

  if (R_1N != NULL) *R_1N = x;
  if (N_neg1R != NULL) *N_neg1R = y;
 
  return b;

}


/*=======================================================================*/
/* extended Euclidean algorithm, runs with integer values,
   uses multiple precision numbers to eliminate integer overflows */
unsigned long egcdm(const unsigned long R, const unsigned long N,
                    unsigned long *R_1N, unsigned long *N_neg1R)
/*=======================================================================*/
{

  size_t bytes;
  bn_t x, y, u, v, m, n, a, b, q, r;
  bn_t mR, mN;
  bn_t aux;
  unsigned int ret;

  size_t max_bytes = sizeof(unsigned long);

  bn_init(x); bn_init(y); bn_init(u); bn_init(v); bn_init(m);
  bn_init(n); bn_init(a); bn_init(b); bn_init(q); bn_init(r);
  bn_init(mR); bn_init(mN);
  bn_init(aux);

  bn_set_ui(mR, R);
  bn_set_ui(mN, N);

  /* N = R(0) + N(1) */
  bn_set_ui(x, 0); bn_set_ui(y, 1);
 
  /* R = R(1) + N(0) */
  bn_set_ui(u, 1); bn_set_ui(v, 0);


  for (bn_set(a, mR), bn_set(b, mN);
       bn_cmp_ui(a, 0) != 0;
       bn_set(b, a), bn_set(a, r), bn_set(x, u),
       bn_set(y, v), bn_set(u, m), bn_set(v, n))
  {
    /* b = aq + r and 0 <= r < a */
    bn_tdiv_q(q, b, a);
    bn_mod(r, b, a);
 
    /* r = Rx + Ny - aq = Rx + Ny - (Ru + Nv)q = R(x - uq) + N(y - vq) */
    bn_mul(aux, u, q);
    bn_sub(m, x, aux);
    bn_mul(aux, v, q);
    bn_sub(n, y, aux);
  }


  /* Rx + Ny = gcd(R, N) */

  bn_add(x, x, mN);
  bn_mod(x, x, mN);
  bn_sub(aux, mR, mN);
  bn_mul(aux, x, aux);
  bn_sub(aux, aux, b);
  bn_tdiv_q(aux, aux, mN);
  bn_add(y, x, aux);

  if (R_1N != NULL)
  {
    *R_1N = 0;
    bn_export(R_1N, &bytes, LSF, 1, NATIVE, 0, x);
    if (max_bytes < bytes)
    {
      fprintf(stderr, "%s: R_1N written %lu instead of %lu bytes\n",
              __func__, (unsigned long)bytes, (unsigned long)max_bytes);
      exit(1);
    }
  }
  if (N_neg1R != NULL)
  {
    *N_neg1R = 0;
    bn_export(N_neg1R, &bytes, LSF, 1, NATIVE, 0, y);
    if (max_bytes < bytes)
    {
      fprintf(stderr, "%s: N_neg1R written %lu instead of %lu bytes\n",
              __func__, (unsigned long)bytes, (unsigned long)max_bytes);
      exit(1);
    }
  }

  ret = bn_cmp_ui(b, 1) == 0; // return 1 if equal

  bn_clear(x); bn_clear(y); bn_clear(u); bn_clear(v); bn_clear(m);
  bn_clear(n); bn_clear(a); bn_clear(b); bn_clear(q); bn_clear(r);
  bn_clear(mR); bn_clear(mN);
  bn_clear(aux);

  return ret;

}


/*=======================================================================*/
/* extended Euclidean gcd algorithm running with large bit-width numbers
   R_inv is considered to have Nbytes bytes
   N_cyc is considered to have Rbytes bytes */
unsigned int egcdm_bignum(const unsigned char *R, const size_t Rbytes,
                          const unsigned char *N, const size_t Nbytes,
                          unsigned char *R_1N, unsigned char *N_neg1R)
/*=======================================================================*/
{

  size_t bytes;
  bn_t x, y, u, v, m, n, a, b, q, r;
  bn_t mR, mN;
  bn_t aux;
  unsigned int ret;

  bn_init(x); bn_init(y); bn_init(u); bn_init(v); bn_init(m);
  bn_init(n); bn_init(a); bn_init(b); bn_init(q); bn_init(r);
  bn_init(mR); bn_init(mN);
  bn_init(aux);

  bn_import(mR, Rbytes, -1, 1, 0, 0, R);
  bn_import(mN, Nbytes, -1, 1, 0, 0, N);

  /* N = R(0) + N(1) */
  bn_set_ui(x, 0); bn_set_ui(y, 1);
 
  /* R = R(1) + N(0) */
  bn_set_ui(u, 1); bn_set_ui(v, 0);


  for (bn_set(a, mR), bn_set(b, mN);
       bn_cmp_ui(a, 0) != 0;
       bn_set(b, a), bn_set(a, r), bn_set(x, u),
       bn_set(y, v), bn_set(u, m), bn_set(v, n))
  {
    /* b = aq + r and 0 <= r < a */
    bn_tdiv_q(q, b, a);
    bn_mod(r, b, a);
 
    /* r = Ax + By - aq = Ax + By - (Au + Bv)q = A(x - uq) + B(y - vq) */
    bn_mul(aux, u, q);
    bn_sub(m, x, aux);
    bn_mul(aux, v, q);
    bn_sub(n, y, aux);
  }


  /* Rx + Ny = gcd(R, N) */

  bn_add(x, x, mN);
  bn_mod(x, x, mN);
  bn_sub(aux, mR, mN);
  bn_mul(aux, x, aux);
  bn_sub(aux, aux, b);
  bn_tdiv_q(aux, aux, mN);
  bn_add(y, x, aux);

  if (R_1N != NULL)
  {
    memset(R_1N, 0, Nbytes);
    bn_export(R_1N, &bytes, LSF, 1, NATIVE, 0, x);
    if (Nbytes < bytes)
    {
      fprintf(stderr, "%s: R_1N written %lu instead of %lu bytes\n",
              __func__, (unsigned long)bytes, (unsigned long)Nbytes);
      exit(1);
    }
  }
  if (N_neg1R != NULL)
  {
    memset(N_neg1R, 0, Rbytes);
    bn_export(N_neg1R, &bytes, LSF, 1, NATIVE, 0, y);
    if (Rbytes < bytes)
    {
      fprintf(stderr, "%s: N_neg1R written %lu instead of %lu bytes\n",
              __func__, (unsigned long)bytes, (unsigned long)Rbytes);
      exit(1);
    }
  }

  ret = bn_cmp_ui(b, 1) == 0; // return 1 if equal

  bn_clear(x); bn_clear(y); bn_clear(u); bn_clear(v); bn_clear(m);
  bn_clear(n); bn_clear(a); bn_clear(b); bn_clear(q); bn_clear(r);
  bn_clear(mR); bn_clear(mN);
  bn_clear(aux);

  return ret;

}
