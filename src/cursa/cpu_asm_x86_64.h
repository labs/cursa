/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef _CPU_ASM_X86_64_H_
#define _CPU_ASM_X86_64_H_


/* a = (a + b*c) % m */
#define cpu_modmac(a, b, c, m) asm("mov %1, %%rax; mul %2; add %0, %%rax; \
                                    adc $0, %%rdx; div %3; mov %%rdx, %0;" \
                                   : "+r"(a) \
                                   : "r"((unsigned long)(b)), \
                                     "r"((unsigned long)(c)), \
                                     "r"((unsigned long)(m)) \
                                   : "%rax", "%rdx")


/* r = (a+b) % m */
#define cpu_modadd(r, a, b, m) asm("mov $0, %%rdx; mov %1, %%rax; \
                                    add %2, %%rax; adc $0, %%rdx; \
                                    div %3; mov %%rdx, %0;" \
                                   : "=r"(r) \
                                   : "r"((unsigned long)(a)), \
                                     "r"((unsigned long)(b)), \
                                     "r"((unsigned long)(m)) \
                                   : "%rax", "%rdx")


/* r = (a*b) % m */
#define cpu_modmul(r, a, b, m) asm("mov %1, %%rax; mul %2; div %3; \
                                    mov %%rdx, %0;" \
                                    : "=r"(r) \
                                    : "r"((unsigned long)(a)), \
                                      "r"((unsigned long)(b)), \
                                      "r"((unsigned long)(m)) \
                                    : "%rax", "%rdx")


/* r = (a+b) & mask */
#define cpu_maskadd(r, a, b, mask) asm("mov %1, %%rax; add %2, %%rax; \
                                        and %3, %%rax; mov %%rax, %0;" \
                                       : "=r"(r) \
                                       : "r"((unsigned long)(a)), \
                                         "r"((unsigned long)(b)), \
                                         "r"((unsigned long)(mask)) \
                                       : "%rax")


/* r = (a*b) & mask */
#define cpu_maskmul(r, a, b, mask) asm("mov %1, %%rax; mul %2; and %3, %%rax; \
                                        mov %%rax, %0;" \
                                       : "=r"(r) \
                                       : "r"((unsigned long)(a)), \
                                         "r"((unsigned long)(b)), \
                                         "r"((unsigned long)(mask)) \
                                       : "%rax", "%rdx")


#endif /* _CPU_ASM_X85_64_H_ */
