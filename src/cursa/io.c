/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <stdio.h>
#include <stdlib.h>


#include "cursa/base.h"
#include "cursa/io.h"


/**
 * \brief write a byte-wise representation of a number
 *
 * Writes a byte-wise representation of the given bignum number.
 *
 * \param[out] fout
 *   output file
 * \param[in]  m
 *   pointer to a number
 * \param[in]  bytes
 *   number of bytes of the number
 * \param[in]  cid
 *   identifier
 */
void rad_fwrite(FILE *fout, const unsigned char *m,
                size_t bytes, const char cid);


/**
 * \brief read a byte-wise represented message
 *
 * Reads a byte-wise representation of a given number.
 *
 * \param[in]  fin
 *   input file
 * \param[out] m
 *   number with the message length
 * \param[in]  bytes
 *   number of bytes of the number to be read
 * \param[in]  cid
 *   identifier
 */
void rad_fread(FILE *fin, unsigned char *m,
               size_t bytes, const char cid);


/*=======================================================================*/
/* writes a byte-wise representation of the given number */
void rad_fwrite(FILE *fout, const unsigned char *m,
                size_t bytes, const char cid)
/*=======================================================================*/
{

  signed int i;

  fprintf(fout, "(%lu bit %c) %02x", bytes*8, cid, m[bytes-1]);

  for (i = bytes-2; i >= 0; i--)
  {
    fprintf(fout, ":%02x", m[i]);
  }
  fprintf(fout, "\n");

}


/*=======================================================================*/
/* reads a byte-wise representation of a given number */
void rad_fread(FILE *fin, unsigned char *m,
               size_t bytes, const char cid)
/*=======================================================================*/
{

  signed int i;

  int num;
  int ret;
  char id;

  ret = fscanf(fin, "(%d bit %c)", &num, &id);
  if ((ret == EOF) || (ret != 2))
  {
    fprintf(stderr, "%s: error reading header\n", __func__);
    exit(1);
  }
  if (num != (bytes*8))
  {
    fprintf(stderr, "%s: unexpected bit length\n", __func__);
    exit(1);
  }
  if (id != cid)
  {
    fprintf(stderr, "%s: unexpected identifier \'%c\', expected \'%c\'",
            __func__, id, cid);
    exit(1);
  }

  ret = fscanf(fin, " %02x", &num);
  if ((ret == EOF) || (ret != 1))
  {
    fprintf(stderr, "%s: error reading highest byte\n", __func__);
    exit(1);
  }
  m[bytes-1] = num & 0xff;

  for (i = bytes-2; i >= 1; i--)
  {
    ret = fscanf(fin, ":%02x", &num);
    if ((ret == EOF) || (ret != 1))
    {
      fprintf(stderr, "%s: error reading highest byte %d\n", __func__, i);
      exit(1);
    }
    m[i] = num & 0xff;
  }

  ret = fscanf(fin, ":%02x\n", &num);
  if ((ret == EOF) || (ret != 1))
  {
    fprintf(stderr, "%s: error reading lowest byte\n", __func__);
    exit(1);
  }
  m[0] = num & 0xff;

}


/*=======================================================================*/
/* writes a key to a file */
void key_fwrite(FILE *fout, const rsa_key *key)
/*=======================================================================*/
{

  rad_fwrite(fout, key->public_exp, CIPHBYTES, 'e');
  rad_fwrite(fout, key->modulus, CIPHBYTES, 'n');
  rad_fwrite(fout, key->private_exp, CIPHBYTES, 'd');

}


/*=======================================================================*/
/* reads a key from a file */
void key_fread(FILE *fin, rsa_key *key)
/*=======================================================================*/
{

  rad_fread(fin, key->public_exp, CIPHBYTES, 'e');
  rad_fread(fin, key->modulus, CIPHBYTES, 'n');
  rad_fread(fin, key->private_exp, CIPHBYTES, 'd');

}


/*=======================================================================*/
/* writes messages to file */
void dump_messages_binary(FILE *fbout,
                          const unsigned char host_mess_arr[MESSBYTES*CIPHERS])
/*=======================================================================*/
{

  fwrite(host_mess_arr, sizeof(unsigned char), MESSBYTES*CIPHERS, fbout);

}


/*=======================================================================*/
/* reads messages from file */
void read_messages_binary(FILE *fbin,
                          unsigned char host_mess_arr[MESSBYTES*CIPHERS])
/*=======================================================================*/
{

  if (fread(host_mess_arr, sizeof(unsigned char), MESSBYTES*CIPHERS, fbin) <
      MESSBYTES*CIPHERS)
  {

    fprintf(stderr, "%s: error reading file containing messages\n", __func__);
    exit(1);

  }

}


/*=======================================================================*/
/* write a message of bytes bytes as a number in base64 */
void rad_fwrite_base64(FILE *fout, unsigned char *m, size_t bytes);
/*=======================================================================*/


/*=======================================================================*/
/* read a message of bytes bytes from a base64 re[presentation */
void rad_fread_base64(FILE *fin, unsigned char *m, size_t bytes);
/*=======================================================================*/
