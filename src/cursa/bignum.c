/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "cursa/bignum.h"
#include "cpu_asm.h"

#include <gmp.h>


/*=======================================================================*/
/* initializes a bignum number */
void bn_init(bn_t x)
/*=======================================================================*/
{

  mpz_init(x);

}


/*=======================================================================*/
/* de-initializes the bignum number */
void bn_clear(bn_t x)
/*=======================================================================*/
{

  mpz_clear(x);

}


/*=======================================================================*/
/* assigns a bignum to a bignum */
void bn_set(bn_t rx, bn_t x)
/*=======================================================================*/
{

  mpz_set(rx, x);

}


/*=======================================================================*/
/* assigns a integer value to a bignum */
void bn_set_ui(bn_t rx, unsigned long x)
/*=======================================================================*/
{

  mpz_set_ui(rx, x);

}


/*=======================================================================*/
/* converts to integer */
unsigned long bn_get_ui(bn_t x)
/*=======================================================================*/
{

  return mpz_get_ui(x);

}


/*=======================================================================*/
/* import a bignum number */
void bn_import(bn_t rn, size_t words, order_t order, size_t size,
               endian_t endian, size_t nails, const void *n)
/*=======================================================================*/
{

  mpz_import(rn, words, order, size, endian, nails, n);

}


/*=======================================================================*/
/* exports a bignum number */
void bn_export(void *rn, size_t *words, order_t order, size_t size,
               endian_t endian, size_t nails, bn_t n)
/*=======================================================================*/
{

  mpz_export(rn, words, order, size, endian, nails, n);

}


/*=======================================================================*/
/* bignum addition */
void bn_add(bn_t res, bn_t op1, bn_t op2)
/*=======================================================================*/
{

  mpz_add(res, op1, op2);

}


/*=======================================================================*/
/* bignum subtraction */
void bn_sub(bn_t res, bn_t op1, bn_t op2)
/*=======================================================================*/
{

  mpz_sub(res, op1, op2);

}


/*=======================================================================*/
/* bignum subtraction */
void bn_sub_ui(bn_t res, bn_t op1, unsigned long op2)
/*=======================================================================*/
{

  mpz_sub_ui(res, op1, op2);

}


/*=======================================================================*/
/* bignum multiplication */
void bn_mul(bn_t res, bn_t op1, bn_t op2)
/*=======================================================================*/
{

  mpz_mul(res, op1, op2);

}


/*=======================================================================*/
/* bignum multiplication */
void bn_mul_ui(bn_t res, bn_t op1, unsigned long op2)
/*=======================================================================*/
{

  mpz_mul_ui(res, op1, op2);

}


/*=======================================================================*/
void bn_tdiv_q(bn_t res, bn_t op1, bn_t op2)
/*=======================================================================*/
{

  mpz_tdiv_q(res, op1, op2);

}


/*=======================================================================*/
/* bignum division */
void bn_tdiv_q_ui(bn_t res, bn_t op1, unsigned long op2)
/*=======================================================================*/
{

  mpz_tdiv_q_ui(res, op1, op2);

}


/*=======================================================================*/
/* bignum modulo division */
void bn_mod(bn_t res, bn_t op1, bn_t op2)
/*=======================================================================*/
{

  mpz_mod(res, op1, op2);

}

/*=======================================================================*/
/* bignum modulo division */
void bn_mod_ui(bn_t res, bn_t op1, unsigned long op2)
/*=======================================================================*/
{

  mpz_mod_ui(res, op1, op2);

}


/*=======================================================================*/
/* bignum exponentiation */
void bn_ui_pow_ui(bn_t res, unsigned long op1, unsigned int op2)
/*=======================================================================*/
{

  mpz_ui_pow_ui(res, op1, op2);

}


/*=======================================================================*/
/* bignum multiplicative inverse */
void bn_invert(bn_t res, bn_t op1, bn_t op2)
/*=======================================================================*/
{

  mpz_invert(res, op1, op2);

}


/*=======================================================================*/
/* compares a bignum with an unsigned integer */
signed int bn_cmp_ui(bn_t op1, unsigned long op2)
/*=======================================================================*/
{

  return mpz_cmp_ui(op1, op2);

}
