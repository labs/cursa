/**
 * \file
 *
 * \author Karel Slany <karel.slany@nic.cz>
 *
 * \version xxx
 *
 * \section LICENSE
 *
 * Copyright (c) 2010-2011, CZ.NIC, z.s.p.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <stdio.h>
#include <stdlib.h>

#include "cursa/gpu_rsa.h"
#include "cursa/cpu_rsa.h"


/* Comments starting immediately with a dollar sign are replaced by
   the constant expander python script either with the 'const' keyword or
   by array initialization assignment. */


/* moduli complements in GPU constant memory
   used: MRS arithmetics, base extension */
__constant__
moduli_t const_base_a_comp[MODULINUM] __align__(8),
         const_base_b_comp[MODULINUM] __align__(8);

/* M_1rm = multiplicative inverse M modulo redundant modulus
   used: KRM base extension */
__constant__
moduli_t const_A_1rm[1] __align__(8) /*$ const_A_1rm */,
         const_B_1rm[1] __align__(8) /*$ const_B_1rm */;

/* Mi_1mi = multiplicative inverse Mi modulo mi
   used: KRM base extension */
__constant__
moduli_t const_Ai_1ai[MODULINUM] __align__(8) /*$ const_Ai_1ai */,
         const_Bi_1bi[MODULINUM] __align__(8) /*$ const_Bi_1bi */;

/* Ais in base b representation and Bis in base a representation
   used: KRM base extension */
__constant__
moduli_t const_Ai_rnsb[MODULINUM][MODULINUM] __align__(8) /*$ const_Ai_rnsb */,
         const_Bi_rnsa[MODULINUM][MODULINUM] __align__(8) /*$ const_Bi_rnsa */;

/* M_rm = dynamic range of base m modulo redundant modulus
   used: KRM base extension */
__constant__
moduli_t const_A_rm[1] __align__(8) /*$ const_A_rm */,
         const_B_rm[1] __align__(8) /*$ const_B_rm */;

/* Mi_rm = M/mi modulo redundant modulus
   used: KRM base extension */
__constant__
moduli_t const_Ai_rm[MODULINUM] __align__(8) /*$ const_Ai_rm */,
         const_Bi_rm[MODULINUM] __align__(8) /*$ const_Bi_rm */;

/* -A in base b representation and -B in base a representation
   used: KRM base extension */
__constant__
moduli_t const_minusA_rnsb[MODULINUM] __align__(8) /*$ const_minusA_rnsb */,
         const_minusB_rnsa[MODULINUM] __align__(8) /*$ const_minusB_rnsa */;

/* n_neg1B in RNS base b representation
   used: RNS Montgomery multiplication */
__constant__
moduli_t const_n_neg1B_rnsb[MODULINUM] __align__(8);

/* n_rnsab = two base RNS representation of RSA modulus n
   used: RNS Montgomery multiplication */
__constant__
moduli_t const_n_rnsab[MODULINUM*2] __align__(8);

/* B_1A_rnsa = RNS base a representation of B**(-1) modulo A
   used: RNS Montgomery multiplication */
__constant__
moduli_t const_B_1A_rnsa[MODULINUM] __align__(8);

/* B2_n_rnsab = two base RNS representation of B**2 modulo n
   used: RNS Montgomery modular exponentiation */
__constant__
moduli_t const_B2_n_rnsab[MODULINUM*2] __align__(8);

/* ones_rnsab - 1 represented in two base RNS
   used: RNS Montgomery modular exponentiation */
__constant__
moduli_t const_ones_rnsab[MODULINUM*2] __align__(8);

/* radix->RNS word-wise conversion arrays, computation in GPU */
__device__ /*$ const */
moduli_t const_ww_radtornsa[MODULINUM][BASE_GPU_WORDS] __align__(8)
                           /*$ const_ww_radtornsa */,
         const_ww_radtornsb[MODULINUM][BASE_GPU_WORDS] __align__(8)
                           /*$ const_ww_radtornsb */;

/* RNS bases
   used: MRS reverse conversion, MRS base extension */
__device__ /*$ const */ /* MRS reverse conversion, MRS base extension */
moduli_t const_base_a[MODULINUM] __align__(8) /*$ const_base_a */,
         const_base_b[MODULINUM] __align__(8) /*$ const_base_b */;

/* multiplicative inverses of products of modulis with indexes
   less than i modulo moduli i
   used: MRS reverse conversion, MRS base extension */
__device__ /*$ const */
moduli_t const_mrs_Paj_1ai[MODULINUM] __align__(8) /*$ const_mrs_Paj_1ai */,
         const_mrs_Pbj_1bi[MODULINUM] __align__(8) /*$ const_mrs_Pbj_1bi */;

/* incremental products of modulis with indexes
   less than i modulo moduli i
   used: MRS reverse conversion, MRS base extension */
__device__ /*$ const */
moduli_t const_mrs_Paj_ai[MODULINUM][MODULINUM] __align__(8)
                         /*$ const_mrs_Paj_ai */,
         const_mrs_Pbj_bi[MODULINUM][MODULINUM] __align__(8)
                         /*$ const_mrs_Pbj_bi */;

/* incremental products of modulus with indexes
   less than i modulo moduli i of the second base
   used: MRS base extension */
__device__ /*$ const */
moduli_t const_mrs_Paji_bi[MODULINUM][MODULINUM] __align__(8)
                          /*$ const_mrs_Paji_bi */,
         const_mrs_Pbji_ai[MODULINUM][MODULINUM] __align__(8)
                          /*$ const_mrs_Pbji_ai */;

/* incremental products of modulis with indexes less than i
   used: MRS reverse conversion */
__device__ /*$ const */
unsigned char const_mrs_Paji[MODULINUM][BASEBYTES] __align__(8)
                            /*$ const_mrs_Paji */,
              const_mrs_Pbji[MODULINUM][BASEBYTES] __align__(8)
                            /*$ const_mrs_Pbji */;

/* public exponent - number of bytes */
__constant__
size_t const_pub_bytes[1] __align__(8);
/* public exponent - radix representation */
__constant__
unsigned char const_pub[MESSBYTES] __align__(8);
/* private exponent - number of bytes */
__constant__
size_t const_priv_bytes[1] __align__(8);
/* private exponent - radix representation */
__constant__
unsigned char const_priv[MESSBYTES] __align__(8);


/**
 * \brief stream structure
 *
 * Stream data structure holding stream related context. The computation can
 * be divided into several streams. (At least one.) These streams may
 * on several devices be executed in a manner which allows overlapping
 * of the executed code.
 */
typedef struct
{
  /** CUDA stream identifier */
  cudaStream_t  stream;
  /** device memory associated with a stream for signed messages */
  unsigned char *dev_out_rad,
  /** device memory associated with a stream for messages to be signed */
                *dev_in_rad;
  /** indicates a corrupt signature in the stream */
  signed int    *dev_stream_corrupt,
  /** sums of corrupt messages in each block */
                *dev_block_corrupt,
  /** sum of corrupt signatures in the stream */
                *dev_num_corrupt;
} stream_data;


/**
 * \brief 64-bit GPU data type
 *
 * Data type needed in several GPU in-line assembly code to access
 * 64-bit integers.
 */
typedef union
{
  /** 64-bit number */
  unsigned long long w64;
  /** allows a simple access to the high and low part number */
  unsigned int u32[2];
} wide_t;


/**
 * \brief word-wise GPU conversion from radix representation into RNS
 *
 * Forward conversion from radix representation into a two base RNS
 * representation in GPU.
 *
 * \param[out] dev_rnsab
 *   both bases RNS representation
 * \param[in]  dev_rad
 *   radix representation of the converted number
 */
/*__noinline__*/ __device__
void gpu_rnsab_from_rad_ww(moduli_t dev_rnsab[MODULINUM*2],
                           const unsigned char dev_rad[MESSBYTES]);
/**
 * \brief forward GPU radix to two base RNS conversion
 *
 * Forward conversion macro, it hides the used method in the source code.
 *
 * \param[out] dev_rnsab
 *   both bases RNS representation
 * \param[in]  dev_rad
 *   radix representation of the converted number
 */
#define gpu_rnsab_from_rad(dev_rnsab, dev_rad) gpu_rnsab_from_rad_ww(\
  (dev_rnsab), (dev_rad))


/**
 * \brief GPU conversion from a RNS base a representation into radix via MRS
 *
 * Reverse conversion from RNS base a representation into radix representation
 * via a MRS number.
 *
 * \param[out] dev_rad
 *   radix representation
 * \param[in]  dev_rnsa
 *   base a RNS representation of the converted number
 * \return
 *   0 if result fits into given space, non 0 else
 */
/*__noinline__*/ __device__
unsigned int gpu_rad_from_rnsa_mrs(unsigned char dev_rad[MESSBYTES],
                                   moduli_t dev_rnsa[MODULINUM]);
/**
 * \brief reverse GPU RNS base a to radix conversion
 *
 * Reverse conversion macro, it hides the used method in the source code.
 *
 * \param[out] dev_rad
 *   radix representation
 * \param[in]  dev_rnsa
 *   base a representation of the converted number
 * \return
 *   0 if result fits into given space, non 0 else
 */
#define gpu_rad_from_rnsa(dev_rad, dev_rnsa) gpu_rad_from_rnsa_mrs((dev_rad),\
                                                                   (dev_rnsa))


/**
 * \brief GPU conversion from a RNS base b representation into radix via MRS
 *
 * Reverse conversion from RNS base b representation into radix representation
 * via a MRS number.
 *
 * \param[out] dev_rad
 *   radix representation of
 * \param[in]  dev_rnsb
 *   base b RNS representation of the converted number
 * \return
 *   0 if result fits into given space, non 0 else
 */
/*__noinline__*/ __device__
unsigned int gpu_rad_from_rnsb_mrs(unsigned char dev_rad[MESSBYTES],
                                   moduli_t dev_rnsb[MODULINUM]);
/**
 * \brief reverse GPU RNS base b to radix conversion
 *
 * Reverse conversion macro, it hides the used method in the source code.
 *
 * \param[out] dev_rad
 *   radix representation
 * \param[in]  dev_rnsb
 *   base b representation of the converted number
 * \return
 *   0 if result fits into given space, non 0 else
 */
#define gpu_rad_from_rnsb(dev_rad, dev_rnsb) gpu_rad_from_rnsb_mrs((dev_rad),\
                                                                   (dev_rnsb))


/**
 * \brief division-less modular addition
 *
 * Computes (a + b) % moduli without division.
 *
 * \param[in] a
 *   first operand
 * \param[in] b
 *   second operand
 * \param[in] moduli_comp
 *   modulus complement
 * \return
 *   (a + b) % moduli
 *
 * \note
 * moduli_comp = 2^w - moduli; w - register bit-width
 * 2^w - moduli -> 0 - moduli (w = 32 for int)
 */
__forceinline__ __device__
moduli_t gpu_commod_add_nodiv(moduli_t a, moduli_t b, moduli_t moduli_comp);
/**
 * \brief modular addition with moduli complement
 *
 * This macro hides the used method in the source code.
 *
 * \param[in] a
 *   first operand
 * \param[in] b
 *   second operand
 * \param[in] mc
 *   modulus complement
 * \return
 *   (a + b) % moduli
 */
#define gpu_commod_add(a, b, mc) gpu_commod_add_nodiv((a), (b), (mc))


/**
 * \brief division-less modular multiplication
 *
 * Computes (a * b) % moduli without division.
 *
 * \param[in] a
 *   multiplicand
 * \param[in] b
 *   multiplier
 * \param[in] moduli_comp
 *   modulus complement
 * \return
 *   (a * b) % moduli
 *
 * \note
 * a, b < moduli; moduli_comp = 2^w - moduli; w - register bit-width
 * moduli_comp < 2^t; 0 < t < (w-1)/2 (w = 32 for int)
 */
__forceinline__ __device__
moduli_t gpu_commod_mul_nodiv(moduli_t a, moduli_t b, moduli_t moduli_comp);
/**
 * \brief modular multiplication with moduli complement
 *
 * This macro hides the used method in the source code.
 *
 * \param[in] a
 *   multiplicand
 * \param[in] b
 *   multiplier
 * \param[in] mc
 *   modulus complement
 * \return
 *   (a * b) % moduli
 */
#define gpu_commod_mul(a, b, mc) gpu_commod_mul_nodiv((a), (b), (mc))


/**
 * \brief masked addition
 *
 * Computes addition for the use of redundant modulus which is a power
 * of two. Therefore the modular division can be replaced with bit masking.
 *
 * \param[in] a
 *   first operand
 * \param[in] b
 *   second operand
 * \return
 *   (a + b) & REDUNDANTMASK
 *
 * \note
 * The constant REDUNDANTMASK masks the bits used in the redundant moduli.
 */
__forceinline__ __device__
moduli_t gpu_mask_add(moduli_t a, moduli_t b);


/**
 * \brief masked multiplication
 *
 * Performs multiplication for the use of redundant modulus which is a power
 * of two. Therefore the modular division can be replaced with bit masking.
 *
 * \param[in] a
 *   multiplicand
 * \param[in] b
 *   multiplier
 * \return
 *   (a * b) & REDUNDANTMASK
 *
 * \note
 * The constant REDUNDANTMASK masks the bits used in the redundant moduli.
 */
__forceinline__ __device__
moduli_t gpu_mask_mul(moduli_t a, moduli_t b);


/**
 * \brief RNS base a addition
 *
 * Adds two numbers in RNS base a.
 *
 * \param[out] r
 *   RNS base a result = s + t
 * \param[in]  s
 *   RNS base a operand
 * \param[in]  t
 *   RNS base a operand
 */
__forceinline__ __device__
void gpu_rnsa_add(moduli_t r[MODULINUM],
                  moduli_t s[MODULINUM], moduli_t t[MODULINUM]);
/**
 * \brief RNS base b addition
 *
 * Adds two numbers in RNS base b.
 *
 * \param[out] r
 *   RNS base b result = s + t
 * \param[in]  s
 *   RNS base b operand
 * \param[in]  t
 *   RNS base b operand
 */
__forceinline__ __device__
void gpu_rnsb_add(moduli_t r[MODULINUM],
                  moduli_t s[MODULINUM], moduli_t t[MODULINUM]);


/**
 * \brief RNS base a multiplication
 *
 * Multiplies two numbers in RNS base a.
 *
 * \param[out] r
 *   RNS base a result = s * t
 * \param[in]  s
 *   RNS base a operand
 * \param[in]  t
 *   RNS base a operand
 */
__forceinline__ __device__
void gpu_rnsa_mul(moduli_t r[MODULINUM],
                  moduli_t s[MODULINUM], moduli_t t[MODULINUM]);
/**
 * \brief RNS base b multiplication
 *
 * Multiplies two numbers in RNS base b.
 *
 * \param[out] r
 *   RNS base b result = s * t
 * \param[in]  s
 *   RNS base b operand
 * \param[in]  t
 *   RNS base b operand
 */
__forceinline__ __device__
void gpu_rnsb_mul(moduli_t r[MODULINUM],
                  moduli_t s[MODULINUM], moduli_t t[MODULINUM]);


/**
 * \brief GPU radix number addition
 *
 * Adds two radix (bignum) numbers.
 *
 * \param[out] r
 *   radix result = s + t
 * \param[in]  s
 *   radix operand
 * \param[in]  t
 *   radix operand
 * \return
 *   0 if result fits into given space, non 0 else
 */
__forceinline__ __device__
unsigned int gpu_rad_add(unsigned char r[BASEBYTES],
                         unsigned char s[BASEBYTES],
                         unsigned char t[BASEBYTES]);


/**
 * \brief radix number multiplication
 *
 * Multiplies a radix (bignum) number with an integer.
 *
 * \param[out] r
 *   radix result = s * t
 * \param[in]  s
 *   radix multiplicand
 * \param[in]  t
 *   integer multiplier
 * \return
 *   0 if result fits into given space, non 0 else
 */
__forceinline__ __device__
unsigned int gpu_rad_mul_ui(unsigned char r[BASEBYTES],
                            const unsigned char s[BASEBYTES], moduli_t t);


/**
 * \brief base extension algorithm
 *
 * Base extension algorithm which computes the RNS base a representation
 * from a number represented in RNS base b. This implementation uses
 * the Kawamura's algorithm with a redundant modulus.
 *
 * \param[out] rns_a
 *   base a RNS result
 * \param[in]  rns_b
 *   base b RNS input
 */
__device__
void gpu_rnsa_from_rnsb_krm(moduli_t rns_a[MODULINUM],
                            moduli_t rns_b[MODULINUM]);
/**
 * \brief base extension algorithm
 *
 * Base extension algorithm which computes the RNS base b representation
 * from a number represented in RNS base a. This implementation uses
 * the Kawamura's algorithm with a redundant modulus.
 *
 * \param[out] rns_b
 *   base b RNS result
 * \param[in]  rns_a
 *   base a RNS input
 */
__device__
void gpu_rnsb_from_rnsa_krm(moduli_t rns_b[MODULINUM],
                            moduli_t rns_a[MODULINUM]);


/**
 * \brief base extension algorithm
 *
 * Base extension algorithm which computes the RNS base a representation
 * from a number represented in RNS base b. This implementation uses
 * the MRS (mixed radix system) conversion algorithm.
 *
 * \param[out] rns_a
 *   base a RNS result
 * \param[in]  rns_b
 *   base b RNS input
 */
__device__
void gpu_rnsa_from_rnsb_mrs(moduli_t rns_a[MODULINUM],
                            moduli_t rns_b[MODULINUM]);
/**
 * \brief base extension algorithm
 *
 * Base extension algorithm which computes the RNS base b representation
 * from a number represented in RNS base a. This implementation uses
 * the MRS (mixed radix system) conversion algorithm.
 *
 * \param[out] rns_b
 *   base b RNS result
 * \param[in]  rns_a
 *   base a RNS input
 */
__device__
void gpu_rnsb_from_rnsa_mrs(moduli_t rns_b[MODULINUM],
                            moduli_t rns_a[MODULINUM]);


/**
 * \brief base extension
 *
 * The macros hide the implementation in the source code.
 *
 * \param[out] rns_a
 *   base a RNS result
 * \param[in]  rns_b
 *   base b RNS input
 */
#define gpu_rnsa_from_rnsb_approx(rns_a, rns_b) gpu_rnsa_from_rnsb_krm(\
  (rns_a), (rns_b))
#define gpu_rnsa_from_rnsb_exact(rns_a, rns_b) gpu_rnsa_from_rnsb_mrs(\
  (rns_a), (rns_b))
/**
 * \brief base extension
 *
 * The macros hide the implementation in the source code.
 *
 * \param[out] rns_b
 *   base b RNS result
 * \param[in]  rns_a
 *   base a RNS input
 */
#define gpu_rnsb_from_rnsa_approx(rns_b, rns_a) gpu_rnsb_from_rnsa_krm(\
  (rns_b), (rns_a))
#define gpu_rnsb_from_rnsa_exact(rns_b, rns_a) gpu_rnsb_from_rnsa_mrs(\
  (rns_b), (rns_a))


/**
 * \brief Montgomery modular multiplication
 *
 * GPU implementation of the Montgomery multiplication algorithm.
 * Computes w = x * y * B**(-1) mod n.
 *
 * \param[out] w_rnsab
 *   two base result
 * \param[in]  x_rnsab
 *   two base multiplicand
 * \param[in]  y_rnsab
 *   two base multiplier
 *
 * \note
 * The algorithm uses a faster base extension algorithm which approximates
 * the conversion error.
 */
__device__
void gpu_rnsab_mmul_approx(moduli_t w_rnsab[MODULINUM*2],
                           moduli_t x_rnsab[MODULINUM*2],
                           moduli_t y_rnsab[MODULINUM*2]);


/**
 * \brief Montgomery modular multiplication
 *
 * GPU implementation of the Montgomery multiplication algorithm.
 * Computes w = x * y * B**(-1) mod n.
 *
 * \param[out] w_rnsab
 *   two base result
 * \param[in]  x_rnsab
 *   two base multiplicand
 * \param[in]  y_rnsab
 *   two base multiplier
 *
 * \note
 * The algorithm uses a slower base extension algorithm which produces exact
 * results.
 */
__device__
void gpu_rnsab_mmul_exact(moduli_t w_rnsab[MODULINUM*2],
                          moduli_t x_rnsab[MODULINUM*2],
                          moduli_t y_rnsab[MODULINUM*2]);


/**
 * \brief Montgomery modular exponentiation
 *
 * GPU implementation of the Montgomery modular exponentiation algorithm.
 * Computes y = x**d * B**(-(d-1)) mod n.
 *
 * \param[out] y_rnsab
 *   two base result
 * \param[in]  x_rnsab
 *   two base input
 * \param[in]  dev_exp
 *   radix representation of the exponent stored in device memory
 * \param[in]  exp_bytes
 *   number of bytes of the used exponent
 */
__device__
void gpu_rnsab_mexp(moduli_t y_rnsab[MODULINUM*2],
                    moduli_t x_rnsab[MODULINUM*2],
                    const unsigned char dev_exp[MESSBYTES],
                    const size_t exp_bytes);


/**
 * \brief RNS RSA algorithm
 *
 * GPU modular exponentiation algorithm. Computes out = in**exp mod n.
 *
 * \param[out] dev_out_rnsab
 *   two base representation of the encrypted message
 * \param[in]  dev_in_rnsab
 *   two base representation of the message to be encrypted
 * \param[in]  dev_exp
 *   exponent
 * \param[in]  exp_bytes
 *   exponent bytes
 */
/*__noinline__*/ __device__
void gpu_rnsab_rsa(moduli_t dev_out_rnsab[MODULINUM*2],
                   moduli_t dev_in_rnsab[MODULINUM*2],
                   const unsigned char dev_exp[MESSBYTES],
                   const size_t exp_bytes);

/**
 * \brief compares two RNS numbers
 *
 * Compares two RNS numbers and compares stored coefficients.
 *
 * \param[in] dev_fst_rnsab
 *   first two base RNS number
 * \param[in] dev_snd_rnsab
 *   second two base RNS number
 * \return
 *   returns 0 if equal, 1 if not equal
 */
__device__
signed int gpu_rnsab_neq(const moduli_t dev_fst_rnsab[MODULINUM*2],
                         const moduli_t dev_snd_rnsab[MODULINUM*2]);


/**
 * \bief compares two radix numbers
 *
 * Performs a comparison of two radix numbers.
 *
 * \param[in] dev_fst_rad
 *   first radix number
 * \param[in] dev_snd_rad
 *   second radix number
 * \return
 *   returns 0 if equal, 1 if not equal
 */
__device__
signed int gpu_rad_neq(const unsigned char dev_fst_rad[MESSBYTES],
                       const unsigned char dev_snd_rad[MESSBYTES]);


/**
 * \brief computes the sum of passed array
 *
 * Each block computes the sum of the passed array.
 *
 * \param[out] dev_result
 *   sum computed by the single block
 * \param[in]  dev_block_data
 *   array to be summed by a block
 *
 * \note
 * The function must be executed by at least (THREADS+1)>>1 threads
 * in each block.
 */
__device__
void gpu_reduce_threads(signed int *dev_result,
                        const signed int dev_block_data[THREADS]);

/**
 * \brief computes the sum of passed array
 *
 * A single block computes the sum of passes array.
 *
 * \param[out] dev_result
 *   sum computed by a single block
 * \param[in]  dev_grid_sums
 *   array to be summed by a single block
 *
 * \note
 * The function is intended to be run by a single block of at least
 * (BLOCKS+1)>>1 threads.
 */
__device__
void gpu_reduce_blocks(signed int *dev_result,
                       const signed int dev_grid_sums[BLOCKS]);


/**
 * \brief computes the sums of threads in blocks
 *
 * Each block in a single stream computes a sum of the given array.
 *
 * \param[out] dev_block_sums
 *   array holding sums from the thread blocks
 * \param[in]  dev_block_data
 *   array holding the data to be summed
 *
 * \note
 * The kernel should be executed with (THREADS+1)>>1 threads per block and
 * BLOCKS blocks per stream;
 */
__global__
void gpu_kernel_reduce_threads(signed int dev_block_sums[BLOCKS],
                               const signed int dev_block_data[STREAMSIZE]);


/**
 * \brief computes the sum of blocks
 *
 * One block computes the sum of all blocks.
 *
 * \param[out] dev_sum
 *   the computed sum
 * \param[in]  dev_grid_sums
 *   the sums of blocks to be summed
 *
 * \note
 * The kernel should be executed in a single block of (BLOCKS+1)>>1 threads.
 */
__global__
void gpu_kernel_reduce_blocks(signed int *dev_sum,
                              const signed int dev_grid_sums[BLOCKS]);


/**
 * \brief complete modular exponentiation kernel with private exponent
 *
 * Signs the messages with the private exponent.
 *
 * \param[out] dev_out_rad
 *   radix represented signed messages in device memory
 * \param[in]  dev_in_rad
 *   radix represented messages to be signed in device memory
 */
__global__
void gpu_kernel_rsa_private(unsigned char dev_out_rad[MESSBYTES*STREAMSIZE],
                            unsigned char dev_in_rad[MESSBYTES*STREAMSIZE]);


/**
 * \brief complete modular exponentiation kernel with public exponent
 *
 * Performs all conversion and modular exponentiation.
 *
 * \param[out] dev_out_rad
 *   radix represented verified messages in device memory
 * \param[in]  dev_in_rad
 *   radix represented messages to be verified in device memory
 */
__global__
void gpu_kernel_rsa_public(unsigned char dev_out_rad[MESSBYTES*STREAMSIZE],
                           unsigned char dev_in_rad[MESSBYTES*STREAMSIZE]);


/**
 * \brief complete modular exponentiation kernel with private exponent
 *   and verification
 *
 * Performs all conversion and modular exponentiation. It preforms a check
 * whether the signature is correct by reconstructing the original message
 * with the public key.
 *
 * \param[out] dev_out_rad
 *   radix represented signed messages in device memory
 * \param[out] dev_corrupt
 *   indicates whether the signature was computed correctly
 * \param[in]  dev_in_rad
 *   radix represented messages to be signed in device memory
 */
__global__
void gpu_kernel_rsa_private_verif(unsigned char dev_out_rad[MESSBYTES*STREAMSIZE],
                                  signed int dev_corrupt[CIPHERS],
                                  unsigned char dev_in_rad[MESSBYTES*STREAMSIZE]);


/**
 * \brief conversion testing kernel
 *
 * Runs conversions and base extensions. It is intended to run from/to RNS
 * conversion tests.
 *
 * \param[out] dev_out_rad
 *   radix represented output messages
 * \param[in]  dev_in_rad
 *   radix represented input messages
 *
 * \note
 * The output messages should be the same as input messages, otherwise an
 * error occured.
 */
__global__
void gpu_kernel_test_conversion(unsigned char dev_out_rad[MESSBYTES*STREAMSIZE],
                                unsigned char dev_in_rad[MESSBYTES*STREAMSIZE]);


/**
 * \brief check CUDA error return value
 *
 * Checks the received error value. If an error is detected then the custom
 * error string is printed following with the error description and the program
 * execution is aborted.
 *
 * \param[in] err
 *   CUDA error value
 * \param[in] fname
 *   function name string
 * \param[in] estr
 *   custom error description string
 */
void check_ret_val(cudaError_t err, const char *fname, const char *estr);


/**
 * \brief check for last CUDA error value
 *
 * Checks for last error value set by previous CUDA events. If an error is
 * detected then the custom error string is printed following with the error
 * description and the program execution is aborted.
 *
 * \param[in] fname
 *   function name string
 * \param[in] estr
 *   cusom  error description string
 */
void check_last_err(const char *fname, const char *estr);


/**
 * \brief dump 1D array to python file
 *
 * Prints an array definition in python notation into file.
 *
 * \param[out] fout
 *   output text file
 * \param[in]  mods
 *   array to be dumped
 * \param[in]  elements
 *   number of elements of the array
 * \param[in]  name
 *   array name
 */
void dump_hex_1D_moduli_py(FILE *fout,
                           const moduli_t *mods, unsigned int elements,
                           const char *name);


/**
 * \brief dump 2D array to python file
 *
 * Prints an array definition in python notation into file.
 *
 * \param[out] fout
 *   output text file
 * \param[in]  mods
 *   array
 * \param[in]  rows
 *   Y dimension
 * \param[in]  cols
 *   X dimension
 * \param[in]  name
 *   array name
 */
void dump_hex_2D_moduli_py(FILE *fout,
                           moduli_t *mods,
                           unsigned int rows, unsigned int cols,
                           const char *name);


/**
 * \brief dump 1D array of bignums
 *
 * Prints an array of bignums definition in python notation to a file. Each
 * bignum is defined by an array of char (numbers).
 *
 * \param[out] fout
 *   output file
 * \param[in]  bigs
 *   array of bignums
 * \param[in]  bignums
 *   number of elements in the array
 * \param[in]  bigbytes
 *   byte size of the bignum numbers
 * \param[in]  name
 *   name of the array
 */
void dump_hex_1D_bignum_py(FILE* fout,
                           unsigned char* bigs, unsigned int bignums,
                           size_t bigbytes, const char* name);


/**
 * \brief sets the exponent
 *
 * Stores the private and public exponent into constant memory on the GPU.
 *
 * \param[in] pub
 *   pointer to public exponent
 * \param[in] pub_bytes
 *   number of bytes of the public exponent
 * \param[in] priv
 *   pointer to private exponent
 * \param[in] priv_bytes
 *   number of bytes of the private exponent
 */
void gpu_set_exponents(const unsigned char pub[CIPHBYTES], size_t pub_bytes,
                       const unsigned char priv[CIPHBYTES], size_t priv_bytes);


/**
 * \brief global stream data structure
 *
 * This structure holds necessary information needed for the execution of
 * the streams on a CUDA capable device.
 */
static stream_data sd[STREAMS];


/*=======================================================================*/
/* convert radix->RNS according to a given conversion matrix */
/* this implementation is designed to be run on GPU */
/*__noinline__*/ __device__
void gpu_rnsab_from_rad_ww(moduli_t dev_rnsab[MODULINUM*2],
                           const unsigned char dev_rad[MESSBYTES])
/*=======================================================================*/
{

  unsigned int modulii, gpu_wordii;
  moduli_t modulos, aux;

  for (modulii = 0; modulii < MODULINUM; modulii++) /* for all modulis */
  {
    modulos = 0;
    for (gpu_wordii = 0; gpu_wordii < MESS_GPU_WORDS; gpu_wordii++)
    {
      /* modulos = (((gpu_unum_t*)rad)[gpu_wordii] *
                    const_ww_radtornsa[modulii][gpu_wordii] + modulos) %
                   base_a[modulii]; */
      aux = gpu_commod_mul(((moduli_t*)dev_rad)[gpu_wordii],
                           const_ww_radtornsa[modulii][gpu_wordii],
                           const_base_a_comp[modulii]);
      modulos = gpu_commod_add(aux, modulos, const_base_a_comp[modulii]);
    }
    dev_rnsab[modulii] = modulos;
  }

  for (modulii = 0; modulii < MODULINUM; modulii++) /* for all modulis */
  {
    modulos = 0;
    for (gpu_wordii = 0; gpu_wordii < MESS_GPU_WORDS; gpu_wordii++)
    {
      /* modulos = (((gpu_unum_t*)rad)[gpu_wordii] *
                    const_ww_radtornsb[modulii][gpu_wordii] + modulos) %
                   base_b[modulii]; */
      aux = gpu_commod_mul(((moduli_t*)dev_rad)[gpu_wordii],
                           const_ww_radtornsb[modulii][gpu_wordii],
                           const_base_b_comp[modulii]);
      modulos = gpu_commod_add(aux, modulos, const_base_b_comp[modulii]);
    }
    dev_rnsab[MODULINUM + modulii] = modulos;
  }

}


/*=======================================================================*/
/* converts RNS base A -> radix, MRS conversion */
/* this implementation is designed to be run of GPU */
/*__noinline__*/ __device__
unsigned int gpu_rad_from_rnsa_mrs(unsigned char dev_rad[MESSBYTES],
                                   moduli_t dev_rnsa[MODULINUM])
/*=======================================================================*/
{

  signed int i,j;

  moduli_t mrsx[MODULINUM]; /* mixed radix representation of x*/
  unsigned long aux1, aux2;

  unsigned char auxrad1[BASEBYTES] __align__(8),
                auxrad2[BASEBYTES] __align__(8);

  unsigned int is_larger;

  mrsx[0] = dev_rnsa[0];

  for (i = 1; i < MODULINUM; i++)
  {
    aux1 = 0;
    for (j = 0; j < i; j++)
    {
      aux2 = gpu_commod_mul(mrsx[j], const_mrs_Paj_ai[i][j],
                            const_base_a_comp[i]);
      aux1 = gpu_commod_add(aux1, aux2, const_base_a_comp[i]);
    }

    aux1 = const_base_a[i] - aux1; /* aux1 = -aux1 */
    aux1 = gpu_commod_add(dev_rnsa[i], aux1, const_base_a_comp[i]);

    aux1 = gpu_commod_mul(const_mrs_Paj_1ai[i], aux1, const_base_a_comp[i]);

    mrsx[i] = aux1;
  }

  is_larger = gpu_rad_mul_ui(auxrad2, const_mrs_Paji[0], mrsx[0]);
  for (i = 1; i < MODULINUM; i++)
  {
    is_larger |= gpu_rad_mul_ui(auxrad1, const_mrs_Paji[i], mrsx[i]);
    is_larger |= gpu_rad_add(auxrad2, auxrad2, auxrad1);
  }

  /* memcpy(dev_rad, auxrad2, MESSBYTES); */
  for (i = 0; i < (MESSBYTES/sizeof(unsigned int)); i++)
  {
    ((unsigned int*)dev_rad)[i] = ((unsigned int*)auxrad2)[i];
  }

  for (i = BASEBYTES-1; i >= MESSBYTES; i--)
  {
    is_larger |= auxrad2[i];
  }

  return is_larger;

}


/*=======================================================================*/
/* converts RNS base B -> radix, MRS conversion */
/* this implementation is designed to be run of GPU */
/*__noinline__*/ __device__
unsigned int gpu_rad_from_rnsb_mrs(unsigned char dev_rad[MESSBYTES],
                                   moduli_t dev_rnsb[MODULINUM])
/*=======================================================================*/
{

  signed int i,j;

  moduli_t mrsx[MODULINUM]; /* mixed radix representation of x*/
  unsigned long aux1, aux2;

  unsigned char auxrad1[BASEBYTES] __align__(8),
                auxrad2[BASEBYTES] __align__(8);

  unsigned int is_larger;

  mrsx[0] = dev_rnsb[0];

  for (i = 1; i < MODULINUM; i++)
  {
    aux1 = 0;
    for (j = 0; j < i; j++)
    {
      aux2 = gpu_commod_mul(mrsx[j], const_mrs_Pbj_bi[i][j],
                            const_base_b_comp[i]);
      aux1 = gpu_commod_add(aux1, aux2, const_base_b_comp[i]);
    }

    aux1 = const_base_b[i] - aux1; /* aux1 = -aux1 */
    aux1 = gpu_commod_add(dev_rnsb[i], aux1, const_base_b_comp[i]);

    aux1 = gpu_commod_mul(const_mrs_Pbj_1bi[i], aux1, const_base_b_comp[i]);

    mrsx[i] = aux1;
  }

  is_larger = gpu_rad_mul_ui(auxrad2, const_mrs_Pbji[0], mrsx[0]);
  for (i = 1; i < MODULINUM; i++)
  {
    is_larger |= gpu_rad_mul_ui(auxrad1, const_mrs_Pbji[i], mrsx[i]);
    is_larger |= gpu_rad_add(auxrad2, auxrad2, auxrad1);
  }

  /* memcpy(de_rad, auxrad2, MESSBYTES); */
  for (i = 0; i < (MESSBYTES/sizeof(unsigned int)); i++)
  {
    ((unsigned int*)dev_rad)[i] = ((unsigned int*)auxrad2)[i];
  }

  for (i = BASEBYTES-1; i >= MESSBYTES; i--)
  {
    is_larger |= auxrad2[i];
  }

  return is_larger;

}


/*=======================================================================*/
/* returns the value of (a + b) % moduli
   a, b < moduli; moduli_comp = 2^32 - moduli = - moduli : on 32 bits */
__forceinline__ __device__
moduli_t gpu_commod_add_nodiv(moduli_t a, moduli_t b,
                              moduli_t moduli_comp)
/*=======================================================================*/
{

  unsigned int r0;
  unsigned int carry0 = 0,
               carry1 = 0;

  asm("add.cc.u32 %0, %1, %2;" : "=r"(r0) : "r"(a), "r"(b));
  asm("addc.u32 %0, %1, %1;" : "=r"(carry0) : "n"(0));

  asm("add.cc.u32 %0, %1, %2;" : "=r"(r0) : "r"(r0), "r"(moduli_comp));
  asm("addc.u32 %0, %1, %1;" : "=r"(carry1) : "n"(0));

  asm("or.b32 %0, %0, %1;" : "+r"(carry0) : "r"(carry1));

  /*
  if (carry0) return r1;
  else return r0;
  */
  asm("sub.u32 %0, %0, %1;" : "+r"(carry0) : "n"(1));
  asm("and.b32 %0, %0, %1;" : "+r"(carry0) : "r"(moduli_comp));
  asm("sub.u32 %0, %0, %1;" : "+r"(r0) : "r"(carry0));

  return r0;

}


/*=======================================================================*/
/* returns the value of (a + b) % moduli
   a, b < moduli; moduli_comp = 2^32 - moduli = - moduli : on 32 bits;
   moduli_comp < 2^t; 0 < t < (k-1)/2; k = 32 */
__forceinline__ __device__
moduli_t gpu_commod_mul_nodiv(moduli_t a, moduli_t b,
                              moduli_t moduli_comp)
/*=======================================================================*/
{

  wide_t p, r;

  asm("mul.wide.u32 %0, %1, %2;" : "=l"(p.w64) : "r"(a), "r"(b));

  asm("mad.wide.u32 %0, %1, %2, %3;" : "=l" (p.w64)
                                     : "r"(moduli_comp),
                                       "r"(p.u32[1]),
                                       "l"((unsigned long long)p.u32[0]));

  asm("mad.wide.u32 %0, %1, %2, %3;" : "=l"(p.w64)
                                     : "r"(moduli_comp),
                                       "r"(p.u32[1]),
                                       "l"((unsigned long long)p.u32[0]));

  /* in this case the mad instruction slows the code execution */
  asm("add.cc.u32 %0, %1, %2;" : "=r"(r.u32[0])
                               : "r"(p.u32[0]), "r"(moduli_comp));
  asm("addc.u32 %0, %1, %2;" : "=r"(r.u32[1]) : "r"(p.u32[1]), "n"(0));

  /*
  if (r.u32[1]) return r.u32[0];
  else return p__.u32[0];
  */
  /* sets borrow/carry */
  asm("sub.cc.u32 %0, %0, %1;" : "+r"(r.u32[1]) : "n"(1));
  asm("subc.u32 %0, %1, %1;" : "=r"(r.u32[1]) : "n"(0));
  asm("and.b32 %0, %0, %1;" : "+r"(r.u32[1]) : "r"(moduli_comp));
  asm("sub.u32 %0, %0, %1;" : "+r"(r.u32[0]) : "r"(r.u32[1]));

  return r.u32[0];

}


/*=======================================================================*/
/* res = (op1 + op2) & REDUNDANTMASK */
__forceinline__ __device__
moduli_t gpu_mask_add(moduli_t a, moduli_t b)
/*=======================================================================*/
{

  unsigned int r;

  asm("add.u32 %0, %1, %2;" : "=r"(r) : "r"(a), "r"(b));
  asm("and.b32 %0, %0, %1;" : "+r"(r) : "n"(REDUNDANTMASK));

  return r;

}


/*=======================================================================*/
/* res = (op1 * op2) & REDUNDANTMASK */
__forceinline__ __device__
moduli_t gpu_mask_mul(moduli_t a, moduli_t b)
/*=======================================================================*/
{

  unsigned int r;

  asm("mul.lo.u32 %0, %1, %2;" : "=r"(r) : "r"(a), "r"(b));
  asm("and.b32 %0, %0, %1;" : "+r"(r) : "n"(REDUNDANTMASK));

  return r;

}


/*=======================================================================*/
/* r = s + t in base A */
__forceinline__ __device__
void gpu_rnsa_add(moduli_t r[MODULINUM],
                  moduli_t s[MODULINUM], moduli_t t[MODULINUM])
/*=======================================================================*/
{

  unsigned int r0;
  unsigned int carry0 = 0,
               carry1 = 0;

  /* expanding here causes code to be slower */
  ///*@ i, 0, MODULINUM, 1; const_base_a_comp */
  for (unsigned int i = 0; i < MODULINUM; i++)
  {

    asm("add.cc.u32 %0, %1, %2;" : "=r"(r0) : "r"(s[i]), "r"(t[i]));
    asm("addc.u32 %0, %1, %1;" : "=r"(carry0) : "n"(0));

    asm("add.cc.u32 %0, %0, %1;" : "+r"(r0) : "r"(const_base_a_comp[i]));
    asm("addc.u32 %0, %1, %1;" : "=r"(carry1) : "n"(0));

    asm("or.b32 %0, %0, %1;" : "+r"(carry0) : "r"(carry1));

    asm("sub.u32 %0, %0, %1;" : "+r"(carry0) : "n"(1)); /* sets borrow/carry */
    asm("and.b32 %0, %0, %1;" : "+r"(carry0) : "r"(const_base_a_comp[i]));
    asm("sub.u32 %0, %1, %2;" : "=r"(r[i]) : "r"(r0), "r"(carry0));

  }

}


/*=======================================================================*/
/* r = s + t in base B */
__forceinline__ __device__
void gpu_rnsb_add(moduli_t r[MODULINUM],
                  moduli_t s[MODULINUM], moduli_t t[MODULINUM])
/*=======================================================================*/
{

  /* this function is not used in the code */

  for (signed int i = 0; i < MODULINUM; i++)
  {
    r[i] = gpu_commod_add(s[i], t[i], const_base_b_comp[i]);
  }

}


/*=======================================================================*/
/* r = s * t in base A */
__forceinline__ __device__
void gpu_rnsa_mul(moduli_t r[MODULINUM],
                  moduli_t s[MODULINUM], moduli_t t[MODULINUM])
/*=======================================================================*/
{

  wide_t p, w;

  /*@ i, 0, MODULINUM, 1; const_base_a_comp */
  for (unsigned int i = 0; i < MODULINUM; i++)
  {

    asm("mul.wide.u32 %0, %1, %2;" : "=l"(p.w64) : "r"(s[i]), "r"(t[i]));

    asm("mad.wide.u32 %0, %1, %2, %3;" : "=l" (p.w64)
                                       : "r"(const_base_a_comp[i]),
                                         "r"(p.u32[1]),
                                         "l"((unsigned long long)p.u32[0]));
    asm("mad.wide.u32 %0, %1, %2, %3;" : "=l"(p.w64)
                                       : "r"(const_base_a_comp[i]),
                                         "r"(p.u32[1]),
                                         "l"((unsigned long long)p.u32[0]));

    asm("add.cc.u32 %0, %1, %2;" : "=r"(w.u32[0])
                                 : "r"(p.u32[0]), "r"(const_base_a_comp[i]));
    asm("addc.u32 %0, %1, %2;" : "=r"(w.u32[1]) : "r"(p.u32[1]), "n"(0));

    /* sets borrow/carry if 0 */
    asm("sub.cc.u32 %0, %0, %1;" : "+r"(w.u32[1]) : "n"(1));
    asm("subc.u32 %0, %1, %1;" : "=r"(w.u32[1]) : "n"(0));
    asm("and.b32 %0, %0, %1;" : "+r"(w.u32[1]) : "r"(const_base_a_comp[i]));
    asm("sub.u32 %0, %1, %2;" : "=r"(r[i]) : "r"(w.u32[0]), "r"(w.u32[1]));

    /*
    asm("add.cc.u32 %0, %0, %1;" : "+r"(p.u32[0]) : "r"(const_base_a_comp[i]));
    asm("addc.u32 %0, %0, %1;" : "+r"(p.u32[1]) : "n"(0));

    asm("sub.cc.u32 %0, %0, %1;" : "+r"(p.u32[1]) : "n"(1));
    asm("subc.u32 %0, %1, %1;" : "=r"(p.u32[1]) : "n"(0));
    asm("and.b32 %0, %0, %1;" : "+r"(p.u32[1]) : "r"(const_base_a_comp[i]));
    asm("sub.u32 %0, %1, %2;" : "=r"(r[i]) : "r"(p.u32[0]), "r"(p.u32[1]));
    */

  }

}


/*=======================================================================*/
/* r = s * t in base B */
__forceinline__ __device__
void gpu_rnsb_mul(moduli_t r[MODULINUM],
                  moduli_t s[MODULINUM], moduli_t t[MODULINUM])
/*=======================================================================*/
{

  wide_t p, w;

  /*@ i, 0, MODULINUM, 1; const_base_b_comp */
  for (unsigned int i = 0; i < MODULINUM; i++)
  {

    asm("mul.wide.u32 %0, %1, %2;" : "=l"(p.w64) : "r"(s[i]), "r"(t[i]));

    asm("mad.wide.u32 %0, %1, %2, %3;" : "=l" (p.w64)
                                       : "r"(const_base_b_comp[i]),
                                         "r"(p.u32[1]),
                                         "l"((unsigned long long)p.u32[0]));
    asm("mad.wide.u32 %0, %1, %2, %3;" : "=l"(p.w64)
                                       : "r"(const_base_b_comp[i]),
                                         "r"(p.u32[1]),
                                         "l"((unsigned long long)p.u32[0]));

    asm("add.cc.u32 %0, %1, %2;" : "=r"(w.u32[0])
                                 : "r"(p.u32[0]), "r"(const_base_b_comp[i]));
    asm("addc.u32 %0, %1, %2;" : "=r"(w.u32[1]) : "r"(p.u32[1]), "n"(0));

    /* sets borrow/carry if 0 */
    asm("sub.cc.u32 %0, %0, %1;" : "+r"(w.u32[1]) : "n"(1));
    asm("subc.u32 %0, %1, %1;" : "=r"(w.u32[1]) : "n"(0));
    asm("and.b32 %0, %0, %1;" : "+r"(w.u32[1]) : "r"(const_base_b_comp[i]));
    asm("sub.u32 %0, %1, %2;" : "=r"(r[i]) : "r"(w.u32[0]), "r"(w.u32[1]));

  }

}


/*=======================================================================*/
/* r = s + t in radix representation
   returns 0 if the result fits in r */
__forceinline__ __device__
unsigned int gpu_rad_add(unsigned char r[BASEBYTES],
                         unsigned char s[BASEBYTES],
                         unsigned char t[BASEBYTES])
/*=======================================================================*/
{

  unsigned int ci, co;

  ci = 0;

  for (signed int i = 0; i < (BASEBYTES/sizeof(unsigned int)); i++)
  {

    asm("add.cc.u32 %0, %1, %2;" : "=r"(((unsigned int*)r)[i])
                                 : "r"(((unsigned int*)s)[i]),
                                   "r"(((unsigned int*)t)[i]));
    asm("addc.u32 %0, %1, %1;" : "=r"(co)
                               : "n"(0));
    asm("add.cc.u32 %0, %0, %1;" : "+r"(((unsigned int*)r)[i])
                                 : "r"(ci));
    asm("addc.u32 %0, %0, %1;" : "+r"(co)
                               : "n"(0));

    ci = co;

  }

  return co;

}


/*=======================================================================*/
/* r = s * t in radix representation
   returns 0 if the result fits in r */
__forceinline__ __device__
unsigned int gpu_rad_mul_ui(unsigned char r[BASEBYTES],
                            const unsigned char s[BASEBYTES], moduli_t t)
/*=======================================================================*/
{

  wide_t aux;

  aux.u32[1] = 0;

  for (signed int i = 0; i < (BASEBYTES/sizeof(unsigned int)); i++)
  {

    aux.u32[0] = aux.u32[1];
    aux.u32[1] = 0;
    asm("mad.wide.u32 %0, %1, %2, %0;" : "+l"(aux.w64)
                                       : "r"(((unsigned int*)s)[i]),
                                         "r"(t));
    ((unsigned int*)r)[i] = aux.u32[0];

  }

  return (aux.u32[0]);

}


/*=======================================================================*/
/* base transformation algorithm
   converts rns_b in base B onto rns_a in base A */
__device__
void gpu_rnsa_from_rnsb_krm(moduli_t rns_a[MODULINUM],
                            moduli_t rns_b[MODULINUM])
/*=======================================================================*/
{

  unsigned int modulii, modulij;
  moduli_t ksi[MODULINUM];
  moduli_t y, aux;
  moduli_t q;
  moduli_t x_tilda;

  moduli_t redundant, sigma, k;

  gpu_rnsb_mul(ksi, rns_b, const_Bi_1bi);

  x_tilda = 0;
  sigma = ALPHA0 >> 1;
  redundant = 0;
  for (modulii = 0; modulii < MODULINUM; modulii++)
  {
    aux = gpu_mask_mul(ksi[modulii], const_Bi_rm[modulii]);
    x_tilda = gpu_mask_add(x_tilda, aux);

    /* compute reduntant moduli */
    sigma += ksi[modulii] >> 1;
    k = sigma & ((unsigned long)1<<(sizeof(moduli_t)*8 - 1));
    sigma = sigma & ~((unsigned long)1<<(sizeof(moduli_t)*8 - 1));
    
    /*k = k ? 1 : 0;*/
    asm("sub.cc.u32 %0, %0, %1;" : "+r"(k) : "n"(1));
    asm("subc.u32 %0, %1, %2;" : "=r"(k) : "n"(1), "n"(0));
    /* the subtraction code is faster */
    /*
    asm("cnot.b32 %0, %0;" : "+r"(k));
    asm("cnot.b32 %0, %0;" : "+r"(k));
    */

    aux = gpu_mask_mul(ksi[modulii], const_Bi_rm[modulii]);
    redundant = gpu_mask_add(redundant, aux);
    aux = gpu_mask_mul(k, const_B_rm[0]);
    aux = gpu_mask_mul(aux, REDUNDANTMASK); /* -1*aux */
    redundant = gpu_mask_add(redundant, aux);
  }

  /* quotient computed from redundant moduli */
  q = gpu_mask_mul(*(moduli_t*)const_B_1rm, (x_tilda - redundant));

  for (modulii = 0; modulii < MODULINUM; modulii++) 
  {
    y = 0;
    for (modulij = 0; modulij < MODULINUM; modulij++)
    {
      aux = gpu_commod_mul(ksi[modulij], const_Bi_rnsa[modulij][modulii],
                           const_base_a_comp[modulii]);
      y = gpu_commod_add(y, aux, const_base_a_comp[modulii]);
    }

    aux = gpu_commod_mul(q, const_minusB_rnsa[modulii],
                         const_base_a_comp[modulii]);
    rns_a[modulii] = gpu_commod_add(y, aux, const_base_a_comp[modulii]);
  }

}


/*=======================================================================*/
/* base transformation algorithm
   converts rns_a in base A onto rns_b in base B */
__device__
void gpu_rnsb_from_rnsa_krm(moduli_t rns_b[MODULINUM], 
                            moduli_t rns_a[MODULINUM])
/*=======================================================================*/
{

  unsigned int modulii, modulij;
  moduli_t ksi[MODULINUM];
  moduli_t y, aux;
  moduli_t q;
  moduli_t x_tilda;

  moduli_t redundant, sigma, k;

  gpu_rnsa_mul(ksi, rns_a, const_Ai_1ai);

  x_tilda = 0;
  sigma = ALPHA0 >> 1;
  redundant = 0;
  for (modulii = 0; modulii < MODULINUM; modulii++)
  {
    aux = gpu_mask_mul(ksi[modulii], const_Ai_rm[modulii]);
    x_tilda = gpu_mask_add(x_tilda, aux);

    /* compute reduntant moduli */
    sigma += ksi[modulii] >> 1;
    k = sigma & ((unsigned long)1<<(sizeof(moduli_t)*8 - 1));
    sigma = sigma & ~((unsigned long)1<<(sizeof(moduli_t)*8 - 1));
    
    /*k = k ? 1 : 0;*/
    asm("sub.cc.u32 %0, %0, %1;" : "+r"(k) : "n"(1));
    asm("subc.u32 %0, %1, %2;" : "=r"(k) : "n"(1), "n"(0));
    /* the subtraction code is faster */
    /*
    asm("cnot.b32 %0, %0;" : "+r"(k));
    asm("cnot.b32 %0, %0;" : "+r"(k));
    */

    aux = gpu_mask_mul(ksi[modulii], const_Ai_rm[modulii]);
    redundant = gpu_mask_add(redundant, aux);
    aux = gpu_mask_mul(k, const_A_rm[0]);
    aux = gpu_mask_mul(aux, REDUNDANTMASK); /* -1*aux */
    redundant = gpu_mask_add(redundant, aux);
  }

  /* quotient computed from redundant moduli */
  q = gpu_mask_mul(*(moduli_t*)const_A_1rm, (x_tilda - redundant));

  for (modulii = 0; modulii < MODULINUM; modulii++) 
  {
    y = 0;
    for (modulij = 0; modulij < MODULINUM; modulij++)
    {
      aux = gpu_commod_mul(ksi[modulij], const_Ai_rnsb[modulij][modulii],
                           const_base_b_comp[modulii]);
      y = gpu_commod_add(y, aux, const_base_b_comp[modulii]);
    }

    aux = gpu_commod_mul(q, const_minusA_rnsb[modulii],
                         const_base_b_comp[modulii]);
    rns_b[modulii] = gpu_commod_add(y, aux, const_base_b_comp[modulii]);
  }

}


/*=======================================================================*/
/* base extension algorithm
   converts rns_b in base A onto rns_a in base B */
__device__
void gpu_rnsa_from_rnsb_mrs(moduli_t rns_a[MODULINUM],
                            moduli_t rns_b[MODULINUM])
/*=======================================================================*/
{

  signed int i,j;

  moduli_t mrs_b[MODULINUM]; /* mixed radix representation of rns_b */
  unsigned long aux1, aux2;

  mrs_b[0] = rns_b[0];

  for (i = 1; i < MODULINUM; i++)
  {
    aux1 = 0;
    for (j = 0; j < i; j++)
    {
      aux2 = gpu_commod_mul(mrs_b[j], const_mrs_Pbj_bi[i][j],
                            const_base_b_comp[i]);
      aux1 = gpu_commod_add(aux1, aux2, const_base_b_comp[i]);
    }

    aux1 = const_base_b[i] - aux1; /* aux1 = -aux1 */
    aux1 = gpu_commod_add(rns_b[i], aux1, const_base_b_comp[i]);

    aux1 = gpu_commod_mul(const_mrs_Pbj_1bi[i], aux1, const_base_b_comp[i]);

    mrs_b[i] = aux1;
  }

  for (i = 0; i < MODULINUM; i++)
  {
    aux1 = 0;
    for(j = 0; j < MODULINUM; j++)
    {
      aux2 = gpu_commod_mul(mrs_b[j], const_mrs_Pbji_ai[i][j],
                            const_base_a_comp[i]);
      aux1 = gpu_commod_add(aux1, aux2, const_base_a_comp[i]);
    }
    rns_a[i] = aux1;
  }

}


/*=======================================================================*/
/* base extension algorithm
   converts rns_b in base A onto rns_a in base B */
__device__
void gpu_rnsb_from_rnsa_mrs(moduli_t rns_b[MODULINUM],
                            moduli_t rns_a[MODULINUM])
/*=======================================================================*/
{

  signed int i,j;

  moduli_t mrs_a[MODULINUM]; /* mixed radix representation of rns_a */
  unsigned long aux1, aux2;

  mrs_a[0] = rns_a[0];

  for (i = 1; i < MODULINUM; i++)
  {
    aux1 = 0;
    for (j = 0; j < i; j++)
    {
      aux2 = gpu_commod_mul(mrs_a[j], const_mrs_Paj_ai[i][j],
                            const_base_a_comp[i]);
      aux1 = gpu_commod_add(aux1, aux2, const_base_a_comp[i]);
    }

    aux1 = const_base_a[i] - aux1; /* aux1 = -aux1 */
    aux1 = gpu_commod_add(rns_a[i], aux1, const_base_a_comp[i]);

    aux1 = gpu_commod_mul(const_mrs_Paj_1ai[i], aux1, const_base_a_comp[i]);

    mrs_a[i] = aux1;
  }

  for (i = 0; i < MODULINUM; i++)
  {
    aux1 = 0;
    for(j = 0; j < MODULINUM; j++)
    {
      aux2 = gpu_commod_mul(mrs_a[j], const_mrs_Paji_bi[i][j],
                            const_base_b_comp[i]);
      aux1 = gpu_commod_add(aux1, aux2, const_base_b_comp[i]);
    }
    rns_b[i] = aux1;
  }

}


/*=======================================================================*/
/* RNS Montgomery multiplication - faster
   w = x * y * B**(-1)mod n */
__device__
void gpu_rnsab_mmul_approx(moduli_t w_rnsab[MODULINUM*2],
                           moduli_t x_rnsab[MODULINUM*2],
                           moduli_t y_rnsab[MODULINUM*2])
/*=======================================================================*/
{

  moduli_t n1[MODULINUM],
           n2[MODULINUM];

  /* 1: */
  gpu_rnsa_mul(n1, &x_rnsab[0], &y_rnsab[0]);
  gpu_rnsb_mul(n2, &x_rnsab[MODULINUM], &y_rnsab[MODULINUM]);

  /* 2: */
  gpu_rnsb_mul(n2, n2, const_n_neg1B_rnsb);
 
  /* 3: */
  gpu_rnsa_from_rnsb_approx(n2, n2);

  /* 4: */
  gpu_rnsa_mul(n2, n2, &const_n_rnsab[0]);
  
  /* 5: */
  gpu_rnsa_add(n2, n1, n2);
  
  /* 6: */
  gpu_rnsa_mul(&w_rnsab[0], n2, const_B_1A_rnsa);

  /* 7: */
  gpu_rnsb_from_rnsa_approx(&w_rnsab[MODULINUM], &w_rnsab[0]);

}


/*=======================================================================*/
/* RNS Montgomery multiplication - slower
   w = x * y * B**(-1) mod n */
__device__
void gpu_rnsab_mmul_exact(moduli_t w_rnsab[MODULINUM*2],
                          moduli_t x_rnsab[MODULINUM*2],
                          moduli_t y_rnsab[MODULINUM*2])
/*=======================================================================*/
{

  moduli_t n1[MODULINUM],
           n2[MODULINUM];

  /* 1: */
  gpu_rnsa_mul(n1, &x_rnsab[0], &y_rnsab[0]);
  gpu_rnsb_mul(n2, &x_rnsab[MODULINUM], &y_rnsab[MODULINUM]); /* s = xy */

  /* 2: */
  gpu_rnsb_mul(n2, n2, const_n_neg1B_rnsb);
 
  /* 3: */
  gpu_rnsa_from_rnsb_exact(n2, n2);

  /* 4: */
  gpu_rnsa_mul(n2, n2, &const_n_rnsab[0]);
  
  /* 5: */
  gpu_rnsa_add(n2, n1, n2);
  
  /* 6: */
  gpu_rnsa_mul(&w_rnsab[0], n2, const_B_1A_rnsa);

  /* 7: */
  gpu_rnsb_from_rnsa_exact(&w_rnsab[MODULINUM], &w_rnsab[0]);

}


/*=======================================================================*/
/* RNS Montgomery modular exponentiation
   y = x**d * B**(-(d-1)) mod n */
__device__
void gpu_rnsab_mexp(moduli_t y_rnsab[MODULINUM*2],
                    moduli_t x_rnsab[MODULINUM*2],
                    const unsigned char dev_exp[MESSBYTES],
                    const size_t *exp_bytes)
/*=======================================================================*/
{

#define get_bit(x, idx) (((x)[(idx)>>3] >> ((idx)&0x07)) & 0x01)

  unsigned int i;

  /*memcpy(y_rnsab, x_rnsab, sizeof(moduli_t)*MODULINUM*2);*/
  for (i = 0; i < MODULINUM*2; i++) y_rnsab[i] = x_rnsab[i];

  i = ((*exp_bytes)*8)-1;

  /* find first non-zero bit starting from MSB */
  while (get_bit(dev_exp, i) == 0) i--;

  i--;

  for (; i+1 >= 1; i--)
  {
    gpu_rnsab_mmul_approx(y_rnsab, y_rnsab, y_rnsab);

    if (get_bit(dev_exp, i))
    {
      gpu_rnsab_mmul_approx(y_rnsab, y_rnsab, x_rnsab);
    }
  }

}


/*=======================================================================*/
/* RNS RSA modular exponentiation
   m = C^d mod N */
/*__noinline__*/ __device__
void gpu_rnsab_rsa(moduli_t dev_out_rnsab[MODULINUM*2],
                   moduli_t dev_in_rnsab[MODULINUM*2],
                   const unsigned char dev_exp[MESSBYTES],
                   const size_t *exp_bytes)
/*=======================================================================*/
{

  moduli_t inl_rnsab[MODULINUM*2];
  moduli_t outl_rnsab[MODULINUM*2];

  /* 5: */
  gpu_rnsab_mmul_approx(inl_rnsab, dev_in_rnsab, const_B2_n_rnsab);

  /* 6 */
  gpu_rnsab_mexp(outl_rnsab, inl_rnsab, dev_exp, exp_bytes);

  /* 7 */
  /*
  gpu_rnsab_mmul_approx(dev_out_rnsab, outl_rnsab, const_ones_rnsab);
  */
  gpu_rnsab_mmul_exact(dev_out_rnsab, outl_rnsab, const_ones_rnsab);
  

}


/*=======================================================================*/
/* compare two rnsab numbers */
__device__
signed int gpu_rnsab_neq(moduli_t dev_fst_rnsab[MODULINUM*2],
                         moduli_t dev_snd_rnsab[MODULINUM*2])
/*=======================================================================*/
{

  signed int neq;

  neq = 0;
  for (signed int i = 0; i < MODULINUM*2; i++)
  {
    neq |= (dev_fst_rnsab[i] != dev_snd_rnsab[i]);
  }

  return neq;

}


/*=======================================================================*/
__device__
signed int gpu_rad_neq(const unsigned char dev_fst_rad[MESSBYTES],
                       const unsigned char dev_snd_rad[MESSBYTES])
/*=======================================================================*/
{

  signed int neq;

  neq = 0;
  for (signed int i = 0; i < MESSBYTES; i++)
  {
    neq |= (dev_fst_rad[i] != dev_snd_rad[i]);
  }

  return neq;

}


/*=======================================================================*/
/* sums the arrays in multiple blocks */
__device__
void gpu_reduce_threads(signed int *dev_result,
                        const signed int dev_block_data[THREADS])
/*=======================================================================*/
{

  __shared__ signed int shared[(THREADS+1)>>1];
  signed int sum;
  
  unsigned int threadidx = threadIdx.x;
  signed int condition;

  unsigned int summed,
               active;

  summed = THREADS;
  active = (summed+1)>>1;

  if (threadidx < active)
  {
    sum = dev_block_data[threadidx<<1];
    condition = ((threadidx<<1)+1) < summed;
    sum += condition * dev_block_data[(threadidx<<1)+1];
    shared[threadidx] = sum;
  }
  __syncthreads();

  summed = active;
  active = (summed+1)>>1;
  for ( ; summed > 1; summed = active, active = (active+1)>>1)
  {

    if (threadidx < active)
    { sum = shared[threadidx<<1]; }
    __syncthreads();
    if (threadidx < active)
    {
      condition = ((threadidx<<1)+1) < summed;
      sum += condition * shared[(threadidx<<1)+1];
    }
    __syncthreads();
    if (threadidx < active)
    {
      shared[threadidx] = sum;
    }
    __syncthreads();

  }

  if (threadidx == 0)
  {
    *dev_result = shared[0];
  }
  __syncthreads();

}


/*=======================================================================*/
/* sums the array in a single block */
__device__
void gpu_reduce_blocks(signed int *dev_result,
                       const signed int dev_grid_sums[BLOCKS])
/*=======================================================================*/
{

  __shared__ signed int shared[(BLOCKS+1)>>1];
  signed int sum;

  unsigned int threadidx = threadIdx.x;

  unsigned int summed,
               active;

  summed = BLOCKS;
  active = (summed+1)>>1;

  if (threadidx < active)
  {
    sum = dev_grid_sums[threadidx<<1];
    if (((threadidx<<1)+1) < summed)
    {
      sum += dev_grid_sums[(threadidx<<1)+1];
    }
    shared[threadidx] = sum;
  }
  __syncthreads();

  summed = active;
  active = (summed+1)>>1;
  for ( ; summed > 1; summed = active, active = (active+1)>>1)
  {

    if (threadidx < active)
    {
      sum = shared[threadidx<<1];
    }
    __syncthreads();
    if (threadidx < active)
    {
      if (((threadidx<<1)+1) < summed)
      {
        sum += shared[(threadidx<<1)+1];
      }
    }
    __syncthreads();
    if (threadidx < active)
    {
      shared[threadidx] = sum;
    }
    __syncthreads();

  }

  if (threadidx == 0)
  {
    *dev_result = shared[0];
  }
  __syncthreads();

}


/*=======================================================================*/
__global__
void gpu_kernel_reduce_threads(signed int dev_block_sums[BLOCKS],
                               const signed int dev_block_data[STREAMSIZE])
/*=======================================================================*/
{

  unsigned int blockidx = blockIdx.x;

  gpu_reduce_threads(&dev_block_sums[blockidx],
                     &dev_block_data[THREADS*blockidx]);

}


/*=======================================================================*/
__global__
void gpu_kernel_reduce_blocks(signed int *dev_sum,
                              const signed int dev_grid_sums[BLOCKS])
/*=======================================================================*/
{

  gpu_reduce_blocks(dev_sum, dev_grid_sums);

}


/*=======================================================================*/
/* modular exponentiation kernel - private exponent
   out = in**e mod n */
__global__
void gpu_kernel_rsa_private(unsigned char dev_out_rad[MESSBYTES*STREAMSIZE],
                            unsigned char dev_in_rad[MESSBYTES*STREAMSIZE])
/*=======================================================================*/
{

  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
  moduli_t tmp_rnsab[MODULINUM*2];

  gpu_rnsab_from_rad(tmp_rnsab, &dev_in_rad[MESSBYTES*i]);

  gpu_rnsab_rsa(tmp_rnsab, tmp_rnsab, const_priv, const_priv_bytes);

  gpu_rad_from_rnsa(&dev_out_rad[MESSBYTES*i], tmp_rnsab);

}


/*=======================================================================*/
/* modular exponentiation kernel - public exponent
   out = in ** pub_exp mod n */
__global__
void gpu_kernel_rsa_public(unsigned char dev_out_rad[MESSBYTES*STREAMSIZE],
                           unsigned char dev_in_rad[MESSBYTES*STREAMSIZE])
/*=======================================================================*/
{

  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
  moduli_t tmp_rnsab[MODULINUM*2];

  gpu_rnsab_from_rad(tmp_rnsab, &dev_in_rad[MESSBYTES*i]);

  gpu_rnsab_rsa(tmp_rnsab, tmp_rnsab, const_pub, const_pub_bytes);

  gpu_rad_from_rnsa(&dev_out_rad[MESSBYTES*i], tmp_rnsab);

}


/*=======================================================================*/
/* modular exponentiation kernel private exponent and verification
   out = in**priv_exp mod n */
__global__
void gpu_kernel_rsa_private_verif(unsigned char dev_out_rad[MESSBYTES*STREAMSIZE],
                                  signed int dev_corrupt[STREAMSIZE],
                                  unsigned char dev_in_rad[MESSBYTES*STREAMSIZE])
/*=======================================================================*/
{

  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
  moduli_t tmp_rnsab[MODULINUM*2];
  unsigned char verif_rad[MESSBYTES] __align__(8);

  gpu_rnsab_from_rad(tmp_rnsab, &dev_in_rad[MESSBYTES*i]);

  /* signature */
  gpu_rnsab_rsa(tmp_rnsab, tmp_rnsab, const_priv, const_priv_bytes);

  /* convert signed */
  gpu_rad_from_rnsa(&dev_out_rad[MESSBYTES*i], tmp_rnsab);

  /* verification - must not be computed via RNS only */
  gpu_rnsab_from_rad(tmp_rnsab, &dev_out_rad[MESSBYTES*i]);
  gpu_rnsab_rsa(tmp_rnsab, tmp_rnsab, const_pub, const_pub_bytes);
  gpu_rad_from_rnsa(verif_rad, tmp_rnsab);

  /* marks different messages */
  dev_corrupt[i] = gpu_rad_neq(verif_rad, &dev_in_rad[MESSBYTES*i]);

}


/*=======================================================================*/
/* conversion to RNS kernel */
__global__
void gpu_kernel_test_conversion(unsigned char dev_out_rad[MESSBYTES*STREAMSIZE],
                                unsigned char dev_in_rad[MESSBYTES*STREAMSIZE])
/*=======================================================================*/
{

  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
  moduli_t rnsab[MODULINUM*2];

  /* to RNS base a */
  gpu_rnsab_from_rad(rnsab, &dev_in_rad[MESSBYTES*i]);
  /* to RNS base b */
  gpu_rnsb_from_rnsa_exact(&rnsab[MODULINUM], &rnsab[0]);
  /* to RNS base a */
  gpu_rnsa_from_rnsb_exact(&rnsab[0], &rnsab[MODULINUM]);
  /* back to radix */
  gpu_rad_from_rnsa(&dev_out_rad[MESSBYTES*i], &rnsab[0]);

}


/*=======================================================================*/
/* initializes GPU constant memory */
void gpu_init_constants(const rsa_key *key)
/*=======================================================================*/
{

  gpu_set_exponents(key->public_exp, CIPHBYTES,
                    key->private_exp, CIPHBYTES);

  check_ret_val(cudaMemcpyToSymbol(const_base_a_comp,
                                   CONST_BASE_A_COMP_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__,
                "error copying into constant memory const_base_a_comp");
  check_ret_val(cudaMemcpyToSymbol(const_base_b_comp,
                                   CONST_BASE_B_COMP_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__,
                "error copying into constant memory const_base_b_comp");

  check_ret_val(cudaMemcpyToSymbol(const_A_1rm, &CONST_A_1RM_MUSTER,
                                  sizeof(moduli_t)),
                __func__, "error copying into constant memory const_A_1rm");
  check_ret_val(cudaMemcpyToSymbol(const_B_1rm, &CONST_B_1RM_MUSTER,
                                   sizeof(moduli_t)),
                __func__, "error copying into constant memory const_B_1rm");

  check_ret_val(cudaMemcpyToSymbol(const_Ai_1ai, CONST_AI_1AI_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__, "error copying into constant memory const_Ai_1ai");
  check_ret_val(cudaMemcpyToSymbol(const_Bi_1bi, CONST_BI_1BI_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__, "error copying into constant memory const_Bi_1bi");

  check_ret_val(cudaMemcpyToSymbol(const_Ai_rnsb, CONST_AI_RNSB,
                                   sizeof(moduli_t) * MODULINUM * MODULINUM),
                __func__, "error copying into constant memory const_Ai_rnsb");
  check_ret_val(cudaMemcpyToSymbol(const_Bi_rnsa, CONST_BI_RNSA,
                                   sizeof(moduli_t) * MODULINUM * MODULINUM),
                __func__, "error copying into constant memory const_Bi_rnsa");

  check_ret_val(cudaMemcpyToSymbol(const_A_rm, &CONST_A_RM_MUSTER,
                                   sizeof(moduli_t)),
                __func__, "error copying into constant memory const_A_rm");
  check_ret_val(cudaMemcpyToSymbol(const_B_rm, &CONST_B_RM_MUSTER,
                                   sizeof(moduli_t)),
                __func__, "error copying into constant memory const_B_rm");

  check_ret_val(cudaMemcpyToSymbol(const_Ai_rm, CONST_AI_RM_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__, "error copying into constant memory const_Ai_rm");
  check_ret_val(cudaMemcpyToSymbol(const_Bi_rm, CONST_BI_RM_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__, "error copying into constant memory const_Bi_rm");

  check_ret_val(cudaMemcpyToSymbol(const_minusA_rnsb, CONST_MINUSA_RNSB_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__,
                "error copying into constant memory const_minusA_rnsb");
  check_ret_val(cudaMemcpyToSymbol(const_minusB_rnsa, CONST_MINUSB_RNSA_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__,
                "error copying into constant memory const_minusB_rnsa");

  check_ret_val(cudaMemcpyToSymbol(const_n_neg1B_rnsb,
                                   CONST_N_NEG1B_RNSB_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__,
                "error copying into constant memory const_n_neg1B_rnsb");

  check_ret_val(cudaMemcpyToSymbol(const_n_rnsab, CONST_N_RNSAB_MUSTER,
                                   sizeof(moduli_t) * MODULINUM * 2),
                __func__, "error copying into constant memory const_n_rnsab");

  check_ret_val(cudaMemcpyToSymbol(const_B_1A_rnsa, CONST_B_1A_RNSA_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__,
                "error copying into constant memory const_B_1A_rnsa");

  check_ret_val(cudaMemcpyToSymbol(const_B2_n_rnsab, CONST_B2_N_RNSAB_MUSTER,
                                   sizeof(moduli_t) * MODULINUM * 2),
                __func__,
                "error copying into constant memory const_B2_n_rnsab");

  check_ret_val(cudaMemcpyToSymbol(const_ones_rnsab, CONST_ONES_RNSAB_MUSTER,
                                   sizeof(moduli_t) * MODULINUM * 2),
                __func__,
                "error copying into constant memory const_ones_rnsab");

  check_ret_val(cudaMemcpyToSymbol(const_ww_radtornsa,
                                   CONST_WW_RADTORNSA_MUSTER,
                                   sizeof(moduli_t) * MODULINUM * BASE_GPU_WORDS),
                __func__,
                "error copying into constant memory const_ww_radtornsa");
  check_ret_val(cudaMemcpyToSymbol(const_ww_radtornsb,
                                   CONST_WW_RADTORNSB_MUSTER,
                                   sizeof(moduli_t) * MODULINUM * BASE_GPU_WORDS),
                __func__,
                "error copying into constant memory const_ww_radtornsb");

  check_ret_val(cudaMemcpyToSymbol(const_base_a, CONST_BASE_A_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__, "error copying into constant memory const_base_a");
  check_ret_val(cudaMemcpyToSymbol(const_base_b, CONST_BASE_B_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__, "error copying into constant memory const_base_b");

  check_ret_val(cudaMemcpyToSymbol(const_mrs_Paj_1ai, CONST_MRS_PAJ_1AI_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__,
                "error copying into constant memory const_mrs_Paj_1ai");
  check_ret_val(cudaMemcpyToSymbol(const_mrs_Pbj_1bi, CONST_MRS_PBJ_1BI_MUSTER,
                                   sizeof(moduli_t) * MODULINUM),
                __func__,
                "error copying into constant memory const_mrs_Pbj_1bi");

  check_ret_val(cudaMemcpyToSymbol(const_mrs_Paj_ai, CONST_MRS_PAJ_AI_MUSTER,
                                   sizeof(moduli_t) * MODULINUM * MODULINUM),
                __func__,
                "error copying into constant memory const_mrs_Paj_ai");
  check_ret_val(cudaMemcpyToSymbol(const_mrs_Pbj_bi, CONST_MRS_PBJ_BI_MUSTER,
                                   sizeof(moduli_t) * MODULINUM * MODULINUM),
                __func__,
                "error copying into constant memory const_mrs_Pbj_bi");

  check_ret_val(cudaMemcpyToSymbol(const_mrs_Paji_bi, CONST_MRS_PAJI_BI_MUSTER,
                                   sizeof(moduli_t) * MODULINUM * MODULINUM),
                __func__,
                "error copying into constant memory const_mrs_Paji_bi");
  check_ret_val(cudaMemcpyToSymbol(const_mrs_Pbji_ai, CONST_MRS_PBJI_AI_MUSTER,
                                   sizeof(moduli_t) * MODULINUM * MODULINUM),
                __func__,
                "error copying into constant memory const_mrs_Pbji_ai");

  check_ret_val(cudaMemcpyToSymbol(const_mrs_Paji, CONST_MRS_PAJI_MUSTER,
                                   MODULINUM * BASEBYTES),
                __func__, "error copying into constant memory const_mrs_Paji");
  check_ret_val(cudaMemcpyToSymbol(const_mrs_Pbji, CONST_MRS_PBJI_MUSTER,
                                   MODULINUM * BASEBYTES),
                __func__, "error copying into constant memory const_mrs_Pbji");

}


/*=======================================================================*/
/* get number of messages to be signed at once */
unsigned long gpu_messages_at_once(void)
/*=======================================================================*/
{

  /* Currently returns the harwired value. This may chage in future when
     dynamic updates may be available. */

  return CIPHERS;

}


/*=======================================================================*/
/* copy exponent into constant memory */
void gpu_set_exponents(const unsigned char pub[MESSBYTES], size_t pub_bytes,
                       const unsigned char priv[MESSBYTES], size_t priv_bytes)
/*=======================================================================*/
{

  size_t exp_byte;

  check_ret_val(cudaMemcpyToSymbol(const_pub, pub, MESSBYTES), __func__,
                "error copying into constant memory const_pub");

  /* compute the real number of non-zero bytes */
  exp_byte = pub_bytes;
  do {
    exp_byte--;
  } while ((exp_byte != 0) && (pub[exp_byte] == 0));
  exp_byte++;

  check_ret_val(cudaMemcpyToSymbol(const_pub_bytes, &exp_byte, sizeof(size_t)),
                __func__,
                "error copying into constant memory const_pub_bytes");



  check_ret_val(cudaMemcpyToSymbol(const_priv, priv, MESSBYTES), __func__,
                "error copying into constant memory const_priv");

  /* compute the number of non-zero bytes */
  exp_byte = priv_bytes;
  do {
    exp_byte--;
  } while ((exp_byte != 0) && (priv[exp_byte] == 0));
  exp_byte++;

  check_ret_val(cudaMemcpyToSymbol(const_priv_bytes, &exp_byte,
                                   sizeof(size_t)),
                __func__,
                "error copying into constant memory const_priv_bytes");

}


/*=======================================================================*/
/* allocate memory space for intermediate values for all streams
   and create streams */
void stream_data_create(stream_data *sd)
/*=======================================================================*/
{

  check_ret_val(cudaStreamCreate(&(sd->stream)), __func__,
                "error creating stream");

  check_ret_val(cudaMalloc(&(sd->dev_out_rad), MESSBYTES * STREAMSIZE),
                __func__, "error allocating dev_out_rad");

  check_ret_val(cudaMalloc(&(sd->dev_in_rad), MESSBYTES * STREAMSIZE),
                __func__, "error allocating dev_in_rad");

  check_ret_val(cudaMalloc(&(sd->dev_stream_corrupt),
                           sizeof(signed int)*STREAMSIZE),
                __func__, "error allocating dev_stream_corrupt");

  check_ret_val(cudaMalloc(&(sd->dev_block_corrupt),
                           sizeof(signed int)*BLOCKS),
                __func__, "error allocating dev_block_corrupt");

  check_ret_val(cudaMalloc(&(sd->dev_num_corrupt), sizeof(signed int)),
                __func__, "error allocating dev_num_corrupt");

}


/*=======================================================================*/
/* free memory space for intermediate values for all streams */
void stream_data_destroy(stream_data *sd)
/*=======================================================================*/
{

  check_ret_val(cudaStreamDestroy(sd->stream), __func__,
                "error destroying stream");

  check_ret_val(cudaFree(sd->dev_out_rad), __func__,
                "error deallocating dev_out_rad");

  check_ret_val(cudaFree(sd->dev_in_rad), __func__,
                "error deallocating dev_in_rad");

  check_ret_val(cudaFree(sd->dev_stream_corrupt), __func__,
                "error deallocating dev_stream_corrupt");

  check_ret_val(cudaFree(sd->dev_block_corrupt), __func__,
                "error deallocating dev_block_corrupt");

  check_ret_val(cudaFree(sd->dev_num_corrupt), __func__,
                "error deallocating dev_num_corrupt");

}


/*=======================================================================*/
/* creates communication context */
void gpu_structs_create(void)
/*=======================================================================*/
{

  signed int i;

  for (i = 0; i < STREAMS; i++)
  {
    stream_data_create(&sd[i]);
  }

}


/*=======================================================================*/
/* destroys the communication context */
void gpu_structs_destroy(void)
/*=======================================================================*/
{

  signed int i;

  for (i = 0; i < STREAMS; i++)
  {
    stream_data_destroy(&sd[i]);
  }

}


/*=======================================================================*/
/* launch the modular exponentiation kernel with parameters - private */
void gpu_launch_rsa_sign(unsigned char host_out_rad[MESSBYTES*CIPHERS],
                         const unsigned char host_in_rad[MESSBYTES*CIPHERS])
/*=======================================================================*/
{

  signed int i;

  /* copy input data into device */ 
  for (i = 0; i < STREAMS; i++)
  {
    check_ret_val(cudaMemcpyAsync(sd[i].dev_in_rad,
                                  &host_in_rad[MESSBYTES*STREAMSIZE*i],
                                  MESSBYTES * STREAMSIZE,
                                  cudaMemcpyHostToDevice,
                                  sd[i].stream),
                  __func__, "error copying data into device");
  }

  /* launch computation */
  for (i = 0; i < STREAMS; i++)
  {
    gpu_kernel_rsa_private<<<BLOCKS, THREADS, 0, sd[i].stream>>>
                          (sd[i].dev_out_rad, sd[i].dev_in_rad);
    check_last_err(__func__, "error launching kernel");
  }

  /* get results from the device */
  for (i = 0; i < STREAMS; i++)
  {
    check_ret_val(cudaMemcpyAsync(&host_out_rad[MESSBYTES*STREAMSIZE*i],
                                  sd[i].dev_out_rad,
                                  MESSBYTES * STREAMSIZE,
                                  cudaMemcpyDeviceToHost,
                                  sd[i].stream),
                  __func__, "error copying data from device");
  }

  /* wait for all streams to finish */
  for (i = STREAMS-1; i >= 0; i--)
  {
    check_ret_val(cudaStreamSynchronize(sd[i].stream),
                  __func__, "error synchronizing streams");
  }

}


/*=======================================================================*/
/* launch the modular exponentiation kernel with parameters - public */
void gpu_launch_rsa_verify(unsigned char host_out_rad[MESSBYTES*CIPHERS],
                           const unsigned char host_in_rad[MESSBYTES*CIPHERS])
/*=======================================================================*/
{

  signed int i;

  /* copy input data into device */ 
  for (i = 0; i < STREAMS; i++)
  {
    check_ret_val(cudaMemcpyAsync(sd[i].dev_in_rad,
                                  &host_in_rad[MESSBYTES*STREAMSIZE*i],
                                  MESSBYTES * STREAMSIZE,
                                  cudaMemcpyHostToDevice,
                                  sd[i].stream),
                  __func__, "error copying data into device");
  }

  /* launch computation */
  for (i = 0; i < STREAMS; i++)
  {
    gpu_kernel_rsa_public<<<BLOCKS, THREADS, 0, sd[i].stream>>>
                         (sd[i].dev_out_rad, sd[i].dev_in_rad);
    check_last_err(__func__, "error launching kernel");
  }

  /* get results from the device */
  for (i = 0; i < STREAMS; i++)
  {
    check_ret_val(cudaMemcpyAsync(&host_out_rad[MESSBYTES*STREAMSIZE*i],
                                  sd[i].dev_out_rad,
                                  MESSBYTES * STREAMSIZE,
                                  cudaMemcpyDeviceToHost,
                                  sd[i].stream),
                  __func__, "error copying data from device");
  }

  /* wait for all streams to finish */
  for (i = STREAMS-1; i >= 0; i--)
  {
    check_ret_val(cudaStreamSynchronize(sd[i].stream),
                  __func__, "error synchronizing streams");
  }

}


/*=======================================================================*/
/* launch the modular exponentiation kernel, tests signature */
signed int gpu_launch_rsa_sign_verify(unsigned char host_out_rad[MESSBYTES*CIPHERS],
                                      signed int host_corrupt[CIPHERS],
                                      const unsigned char host_in_rad[MESSBYTES*CIPHERS])
/*=======================================================================*/
{

  signed int i;

  signed int host_num_corrupt;
  signed int host_stream_sums[STREAMS];

  /* copy data into the device */
  for (i = 0; i < STREAMS; i++)
  {
    check_ret_val(cudaMemcpyAsync(sd[i].dev_in_rad,
                                  &host_in_rad[MESSBYTES*STREAMSIZE*i],
                                  MESSBYTES * STREAMSIZE,
                                  cudaMemcpyHostToDevice,
                                  sd[i].stream),
                  __func__, "error copying input data to device");
  }

  /* launch the computation */
  for (i = 0; i < STREAMS; i++)
  {
    gpu_kernel_rsa_private_verif<<<BLOCKS, THREADS, 0, sd[i].stream>>>
      (sd[i].dev_out_rad, sd[i].dev_stream_corrupt, sd[i].dev_in_rad);
    check_last_err(__func__, "error launching kernel");
  }

  /* copy the results */
  for (i = 0; i < STREAMS; i++)
  {
    check_ret_val(cudaMemcpyAsync(&host_out_rad[MESSBYTES*STREAMSIZE*i],
                                  sd[i].dev_out_rad,
                                  MESSBYTES * STREAMSIZE,
                                  cudaMemcpyDeviceToHost,
                                  sd[i].stream),
                  __func__, "error copying result data from device");
  }

  /* copy information about the results */
  if (host_corrupt != NULL)
  {
    for (i = 0; i < STREAMS; i++)
    {
      check_ret_val(cudaMemcpyAsync(&host_corrupt[STREAMSIZE*i],
                                    sd[i].dev_stream_corrupt,
                                    sizeof(signed int) * STREAMSIZE,
                                    cudaMemcpyDeviceToHost,
                                    sd[i].stream),
                    __func__, "error copying information data from device");
    }
  }

  /* compute the number of corrupt messages */
  for (i = 0; i < STREAMS; i++)
  {
    gpu_kernel_reduce_threads<<<BLOCKS, (THREADS+1)>>1, 0, sd[i].stream>>>
                             (sd[i].dev_block_corrupt,
                              sd[i].dev_stream_corrupt);
    gpu_kernel_reduce_blocks<<<1, (BLOCKS+1)>>1, 0, sd[i].stream>>>
                            (sd[i].dev_num_corrupt, sd[i].dev_block_corrupt);
  }

  /* wait for all streams to finish */
  for (i = STREAMS-1; i >= 0; i--)
  {
    check_ret_val(cudaStreamSynchronize(sd[i].stream),
                  __func__, "error synchronizing streams");
  }

  /* collect the number of corrupt messages from the streams */
  for (i = 0; i < STREAMS; i++)
  {
    check_ret_val(cudaMemcpy(&host_stream_sums[i], sd[i].dev_num_corrupt,
                             sizeof(signed int), cudaMemcpyDeviceToHost),
                  __func__, "error collecting number of corrupt signatures");
  }

  /* collect the sum of the streams and sum them */
  host_num_corrupt = 0;
  for (i = 0; i < STREAMS; i++)
  {
    host_num_corrupt += host_stream_sums[i];
  }

  return host_num_corrupt;

}


/*=======================================================================*/
/* check return value */
void check_ret_val(cudaError_t err, const char *fname, const char * estr)
/*=======================================================================*/
{

  if (err != cudaSuccess)
  {
    fprintf(stderr, "(%s) %s: %s\n", fname, estr, cudaGetErrorString(err));
    exit(1);
  }

}


/*=======================================================================*/
/* check last err */
void check_last_err(const char *fname, const char *estr)
/*=======================================================================*/
{

  cudaError_t err;

  err = cudaGetLastError();

  if (err != cudaSuccess)
  {
    fprintf(stderr, "(%s) %s: %s\n", fname, estr, cudaGetErrorString(err));
    exit(1);
  }

}


/*=======================================================================*/
/* test if some GPU devices are present */
int gpu_seach_devs(void)
/*=======================================================================*/
{

  int dev_count = 0;

  cudaGetDeviceCount(&dev_count);

  return dev_count;

}


/*=======================================================================*/
/* print device info */
void gpu_print_dev_info(void)
/*=======================================================================*/
{

  signed int i;

  int dev_count = 0,
      driver_ver = 0,
      runtime_ver = 0;
  cudaDeviceProp dev_prop;
  int nGpuArchCoresPerSM[] = { -1, 8, 32 };


  cudaGetDeviceCount(&dev_count);
  if (dev_count == 0)
  {
    fprintf(stdout, "No device supporting CUDA found.\n");
    exit(1);
  }
  cudaDriverGetVersion(&driver_ver);
  fprintf(stdout, "CUDA driver version: %d.%d\n",
          driver_ver/1000, driver_ver%100);
  cudaRuntimeGetVersion(&runtime_ver);
  fprintf(stdout, "CUDA runtime version: %d.%d\n",
          runtime_ver/1000, runtime_ver%100);

  for (i = 0; i < dev_count; i++)
  {
    cudaGetDeviceProperties(&dev_prop, i);
    fprintf(stdout, "dev %d: compute capability: %d.%d\n",
            i, dev_prop.major, dev_prop.minor);
    fprintf(stdout, "dev %d: number of multiprocessors: %d\n",
            i, dev_prop.multiProcessorCount);
    fprintf(stdout, "dev %d: cores per multiprocessor: %d\n",
            i, nGpuArchCoresPerSM[dev_prop.major]);
    fprintf(stdout, "dev %d: global mem: %lu bytes\n",
            i, dev_prop.totalGlobalMem);
    fprintf(stdout, "dev %d: constant mem: %lu bytes\n",
            i, dev_prop.totalConstMem);
    fprintf(stdout, "dev %d: shared mem per block: %lu bytes\n",
            i, dev_prop.sharedMemPerBlock);
    fprintf(stdout, "dev %d: register available per block: %d\n",
            i, dev_prop.regsPerBlock);
    fprintf(stdout, "dev %d: warp size: %d\n",
            i, dev_prop.warpSize);
    fprintf(stdout, "dev %d: max number of threads per block: %d\n",
            i, dev_prop.maxThreadsPerBlock);
  }

}


/*=======================================================================*/
/* allocate page-locked memory */
void malloc_pagelocked(void **ptr, size_t size)
/*=======================================================================*/
{

  check_ret_val(cudaMallocHost(ptr, size, cudaHostAllocDefault),
                __func__, "error allocating memory");

}


/*=======================================================================*/
/* free page-locked memory */
void free_pagelocked(void *ptr)
/*=======================================================================*/
{

  check_ret_val(cudaFreeHost(ptr), __func__, "error freeing memory");

}


/*=======================================================================*/
/* dump 1D moduli_t array as hexadecimal numbers */
void dump_hex_1D_moduli_py(FILE *fout,
                           const moduli_t *mods, unsigned int elements,
                           const char *name)
/*=======================================================================*/
{

  unsigned int i;

  fprintf(fout, "%s = [ %#010x", name, mods[0]);
  for (i = 1; i < elements; i++)
  {
    fprintf(fout, ", %#010x", mods[i]);
  }
  fprintf(fout, " ]\n");

}


/*=======================================================================*/
/* dump 2D moduli_t array as hexadecimal numbers */
void dump_hex_2D_moduli_py(FILE* fout,
                           moduli_t* mods,
                           unsigned int rows, unsigned int cols,
                           const char* name)
/*=======================================================================*/
{

  unsigned int i, j;

  fprintf(fout, "%s = [ [ %#010x", name, mods[0]);
  for (j = 1; j < cols; j++)
  {
    fprintf(fout, ", %#010x", mods[j]);
  }
  for (i = 1; i < rows; i++)
  {
    fprintf(fout, " ],\n");
    for (j = 0; j < (strlen(name) + 5); j++)
    {
      fputc(' ', fout);
    }
    fprintf(fout, "[ %#010x", mods[i*cols]);
    for (j = 1; j < cols; j++)
    {
      fprintf(fout, ", %#010x", mods[i*cols + j]);
    }
  }
  fprintf(fout, " ] ]\n");

}


/*=======================================================================*/
/* dump 1D array of bignums as hexadeximal numbers */
void dump_hex_1D_bignum_py(FILE* fout,
                           unsigned char* bigs,
                           unsigned int bignums, size_t bigbytes,
                           const char* name)
/*=======================================================================*/
{

  unsigned long i, j;

  fprintf(fout, "%s = [ [ %#04x", name, bigs[0]);
  for (j = 1; j < bigbytes; j++)
  {
    fprintf(fout, ", %#04x", bigs[j]);
  }
  for (i = 1; i < bignums; i++)
  {
    fprintf(fout, " ],\n");
    for (j = 0; j < (strlen(name) + 5); j++)
    {
      fputc(' ', fout);
    }
    fprintf(fout, "[ %#04x", bigs[i*bigbytes]);
    for (j = 1; j < bigbytes; j++)
    {
      fprintf(fout, ", %#04x", bigs[i*bigbytes + j]);
    }
  }
  fprintf(fout, " ] ]\n");

}


/*=======================================================================*/
/* dump GPU constants into file containing python arrays,
   must be run after constants initiation */
void dump_modulus_independent_to_python(FILE * fout)
/*=======================================================================*/
{

  fprintf(fout, "MESS_GPU_WORDS = %lu\n", MESS_GPU_WORDS);
  fprintf(fout, "MODULINUM = %i\n", MODULINUM);

  fprintf(fout, "arrdim = {} # \"name\" : dimesnion\n");

  dump_hex_1D_moduli_py(fout, CONST_BASE_A_COMP_MUSTER, MODULINUM,
                        "const_base_a_comp");
  fprintf(fout, "arrdim[\"const_base_a_comp\"] = 1\n");
  dump_hex_1D_moduli_py(fout, CONST_BASE_B_COMP_MUSTER, MODULINUM,
                        "const_base_b_comp");
  fprintf(fout, "arrdim[\"const_base_b_comp\"] = 1\n");

  dump_hex_1D_moduli_py(fout, &CONST_A_1RM_MUSTER, 1, "const_A_1rm");
  fprintf(fout, "arrdim[\"const_A_1rm\"] = 1\n");
  dump_hex_1D_moduli_py(fout, &CONST_B_1RM_MUSTER, 1, "const_B_1rm");
  fprintf(fout, "arrdim[\"const_B_1rm\"] = 1\n");

  dump_hex_1D_moduli_py(fout, CONST_AI_1AI_MUSTER, MODULINUM, "const_Ai_1ai");
  fprintf(fout, "arrdim[\"const_Ai_1ai\"] = 1\n");
  dump_hex_1D_moduli_py(fout, CONST_BI_1BI_MUSTER, MODULINUM, "const_Bi_1bi");
  fprintf(fout, "arrdim[\"const_Bi_1bi\"] = 1\n");

  dump_hex_2D_moduli_py(fout, (moduli_t*)CONST_AI_RNSB, MODULINUM, MODULINUM,
                        "const_Ai_rnsb");
  fprintf(fout, "arrdim[\"const_Ai_rnsb\"] = 2\n");
  dump_hex_2D_moduli_py(fout, (moduli_t*)CONST_BI_RNSA, MODULINUM, MODULINUM,
                        "const_Bi_rnsa");
  fprintf(fout, "arrdim[\"const_Bi_rnsa\"] = 2\n");

  dump_hex_1D_moduli_py(fout, &CONST_A_RM_MUSTER, 1, "const_A_rm");
  fprintf(fout, "arrdim[\"const_A_rm\"] = 1\n");
  dump_hex_1D_moduli_py(fout, &CONST_B_RM_MUSTER, 1, "const_B_rm");
  fprintf(fout, "arrdim[\"const_B_rm\"] = 1\n");

  dump_hex_1D_moduli_py(fout, CONST_AI_RM_MUSTER, MODULINUM, "const_Ai_rm");
  fprintf(fout, "arrdim[\"const_Ai_rm\"] = 1\n");
  dump_hex_1D_moduli_py(fout, CONST_BI_RM_MUSTER, MODULINUM, "const_Bi_rm");
  fprintf(fout, "arrdim[\"const_Bi_rm\"] = 1\n");

  dump_hex_1D_moduli_py(fout, CONST_MINUSA_RNSB_MUSTER, MODULINUM,
                        "const_minusA_rnsb");
  fprintf(fout, "arrdim[\"const_minusA_rnsb\"] = 1\n");
  dump_hex_1D_moduli_py(fout, CONST_MINUSB_RNSA_MUSTER, MODULINUM,
                        "const_minusB_rnsa");
  fprintf(fout, "arrdim[\"const_minusB_rnsa\"] = 1\n");

  dump_hex_1D_moduli_py(fout, CONST_B_1A_RNSA_MUSTER, MODULINUM,
                        "const_B_1A_rnsa");
  fprintf(fout, "arrdim[\"const_B_1A_rnsa\"] = 1\n");

  dump_hex_1D_moduli_py(fout, CONST_ONES_RNSAB_MUSTER, MODULINUM*2,
                        "const_ones_rnsab");
  fprintf(fout, "arrdim[\"const_ones_rnsab\"] = 1\n");

  dump_hex_2D_moduli_py(fout, (moduli_t*)CONST_WW_RADTORNSA_MUSTER,
                        MODULINUM, BASE_GPU_WORDS, "const_ww_radtornsa");
  fprintf(fout, "arrdim[\"const_ww_radtornsa\"] = 2\n");
  dump_hex_2D_moduli_py(fout, (moduli_t*)CONST_WW_RADTORNSB_MUSTER,
                        MODULINUM, BASE_GPU_WORDS, "const_ww_radtornsb");
  fprintf(fout, "arrdim[\"const_ww_radtornsb\"] = 2\n");

  dump_hex_1D_moduli_py(fout, CONST_BASE_A_MUSTER, MODULINUM, "const_base_a");
  fprintf(fout, "arrdim[\"const_base_a\"] = 1\n");
  dump_hex_1D_moduli_py(fout, CONST_BASE_B_MUSTER, MODULINUM, "const_base_b");
  fprintf(fout, "arrdim[\"const_base_b\"] = 1\n");

  dump_hex_1D_moduli_py(fout, CONST_MRS_PAJ_1AI_MUSTER, MODULINUM,
                        "const_mrs_Paj_1ai");
  fprintf(fout, "arrdim[\"const_mrs_Paj_1ai\"] = 1\n");
  dump_hex_1D_moduli_py(fout, CONST_MRS_PBJ_1BI_MUSTER, MODULINUM,
                        "const_mrs_Pbj_1bi");
  fprintf(fout, "arrdim[\"const_mrs_Pbj_1bi\"] = 1\n");

  dump_hex_2D_moduli_py(fout, (moduli_t*)CONST_MRS_PAJ_AI_MUSTER,
                        MODULINUM, MODULINUM, "const_mrs_Paj_ai");
  fprintf(fout, "arrdim[\"const_mrs_Paj_ai\"] = 2\n");
  dump_hex_2D_moduli_py(fout, (moduli_t*)CONST_MRS_PBJ_BI_MUSTER,
                        MODULINUM, MODULINUM, "const_mrs_Pbj_bi");
  fprintf(fout, "arrdim[\"const_mrs_Pbj_bi\"] = 2\n");

  dump_hex_2D_moduli_py(fout, (moduli_t*)CONST_MRS_PAJI_BI_MUSTER,
                        MODULINUM, MODULINUM, "const_mrs_Paji_bi");
  fprintf(fout, "arrdim[\"const_mrs_Paji_bi\"] = 2\n");
  dump_hex_2D_moduli_py(fout, (moduli_t*)CONST_MRS_PBJI_AI_MUSTER,
                        MODULINUM, MODULINUM, "const_mrs_Pbji_ai");
  fprintf(fout, "arrdim[\"const_mrs_Pbji_ai\"] = 2\n");

  dump_hex_1D_bignum_py(fout, (unsigned char*)CONST_MRS_PAJI_MUSTER,
                        MODULINUM, BASEBYTES, "const_mrs_Paji");
  fprintf(fout, "arrdim[\"const_mrs_Paji\"] = 2\n");
  dump_hex_1D_bignum_py(fout, (unsigned char*)CONST_MRS_PBJI_MUSTER,
                        MODULINUM, BASEBYTES, "const_mrs_Pbji");
  fprintf(fout, "arrdim[\"const_mrs_Pbji\"] = 2\n");

}
