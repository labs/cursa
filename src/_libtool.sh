#!/bin/sh

# A dirty workaround how to force the nvcc compiler to be used for building the shared library.

LIBTOOL=`echo $* | sed -e 's/^.*[ ]\([^ ]*libtool[^ ]*\)[ ].*$/\1/g'`
MODIF_LIBTOOL=__libtool

cat $LIBTOOL | sed -e 's/\(archive_cmds.*\)\\\$CC/\1..\/_nvcc.sh nvcc/g' > $MODIF_LIBTOOL
chmod +x $MODIF_LIBTOOL

MODIF_COMMAND=`echo $* | sed -e 's/\(^.*[ ]\)[^ ]*libtool[^ ]*\([ ].*\)/\1'${MODIF_LIBTOOL}'\2/g'`
$MODIF_COMMAND

rm -f $MODIF_LIBTOOL
